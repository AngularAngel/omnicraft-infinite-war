extends Resource

class_name VoxelStructure


var _offset := Vector3i()
var _voxels := VoxelBuffer.new()


func get_dimensions():
	return voxels.get_size();
