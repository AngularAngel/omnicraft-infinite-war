extends Node

class_name EndlessHorde


const enemy_ai = preload("res://Scenes/Entities/Controllers/Enemy/BaseEnemyController.tscn")
const VALUE_SETTING = preload("res://Scenes/GUI/Common/Settings/Value/ValueSetting.tscn");
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")


@onready var _random = get_parent().get_parent()._random as RandomNumberGenerator;
@onready var _terrain = get_parent().get_parent()._terrain as VoxelTerrain;
@onready var _voxel_tool = _terrain.get_voxel_tool();


var _player;
var _faction: Faction;
var _spawn_time := 10.0;
var _start_time := 10.0;
var _spawn_counter := -_start_time;


func _physics_process(delta):
	if not is_instance_valid(_player):
		return;
	_spawn_counter += delta;
	if _spawn_counter > 0 and _random.randf() > 0.99:
		if spawn_enemy(_player):
			_spawn_counter -= _spawn_time;


func get_spawn_position(player):
	var vector = Vector2((_random.randf() - 0.5) * 2.0, (_random.randf() - 0.5) * 2.0).normalized() * 45.0;
	return Vector3(vector.x, 0, vector.y) + player.global_transform.origin;


func spawn_enemy(player):
	var spawn_position;
	var i = 0;
	while i < 5:
		spawn_position = get_spawn_position(player);
		if _voxel_tool.get_voxel(Vector3i(spawn_position)) == 0:
			break;
		i += 1;
	
	if i == 5:
		return false;
	
	var enemy = _faction._units["Enemy"].instantiate();
	enemy._faction = _faction;
	var unit_positioner = UnitPositioner.new();
	unit_positioner._terrain_tool = _voxel_tool;
	unit_positioner._target_position = spawn_position;
	enemy.add_child(unit_positioner)
	enemy.transform.origin = spawn_position;
	
	var controller = enemy_ai.instantiate();
	controller.name = "Controller";
	controller._target = player;
	enemy.add_child(controller);
	
	get_parent().get_parent().get_node("Units").add_child(enemy);
	
	return true;


func get_save_data():
	var save_dict = {
		"Config Name" : "Endless Horde",
		"Spawn Time": _spawn_time,
		"Spawn Counter": _spawn_counter,
	}
	return save_dict;


static func get_component_config():
	var horde_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	horde_config.set_component_name("Endless Horde");
	var spawn_time = VALUE_SETTING.instantiate();
	spawn_time._setting_name = "Spawn Time";
	spawn_time._minimum = 0.1;
	spawn_time._maximum = 10.0;
	spawn_time._default_value = 10.0;
	horde_config.add_setting(spawn_time);
	var start_time = VALUE_SETTING.instantiate();
	start_time._setting_name = "Start Time";
	start_time._minimum = 0.1;
	start_time._maximum = 10.0;
	start_time._default_value = 10.0;
	horde_config.add_setting(start_time);
	return horde_config;


static func get_component_from_config(component_config):
	var endless_horde = EndlessHorde.new();
	if component_config.has("Start Time"):
		endless_horde._start_time = component_config["Start Time"];
	endless_horde._spawn_time = component_config["Spawn Time"];
	if component_config.has("Spawn Counter"):
		endless_horde._spawn_counter = component_config["Spawn Counter"];
	return endless_horde;
