extends Node

class_name Faction


static var _component_scenes = {
	"ConstructionSite": preload("res://Scenes/Entities/Buildings/ConstructionSite/ConstructionSite.tscn"),
	"BaseBuildingUnit": preload("res://Scenes/Entities/Buildings/Base/BaseBuildingUnit.tscn"),
	"BaseMobileUnit": load("res://Scenes/Entities/Units/Base/BaseMobileUnit.tscn"),
	"Chassis": preload("res://Scenes/Entities/Components/External/Chassis/BaseChassisComponent.tscn"),
	"Storage": preload("res://Scenes/Entities/Components/Internal/Storage/BaseStorageComponent.tscn"),
	"Generator": preload("res://Scenes/Entities/Components/Internal/Generator/BaseGeneratorComponent.tscn"),
	"Conduit": preload("res://Scenes/Entities/Components/External/Misc/Conduit/BaseConduitComponent.tscn"),
	"Motivator": preload("res://Scenes/Entities/Components/External/Motivators/BaseMotivatorComponent.tscn"),
	"BaseWeapon": preload("res://Scenes/Entities/Components/External/Weapons/BaseWeaponComponent.tscn"),
	"MassCollector": preload("res://Scenes/Entities/Components/External/Tools/MassCollector/MassCollector.tscn"),
	"ConstructionBeam": preload("res://Scenes/Entities/Components/External/Tools/ConstructionBeam/ConstructionBeam.tscn"),
	"Fabricator": preload("res://Scenes/Entities/Components/Internal/Fabricator/BaseFabricatorComponent.tscn"),
	"ReloadingProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/Reloading/ReloadingComponentProperty.tscn"),
	"AmmoProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/Ammo/AmmoComponentProperty.tscn"),
	"RecoilProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/Recoil/RecoilComponentProperty.tscn"),
	"EnergyCostProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/EnergyCost/EnergyCostComponentProperty.tscn"),
	"FiringSoundProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/FiringSound/FiringSoundComponentProperty.tscn"),
	"EnergyBeamProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/EnergyBeam/EnergyBeamComponentProperty.tscn"),
	"DamageDeltaProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/DamageDelta/DamageDeltaComponentProperty.tscn"),
	"BulletHoleProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/BulletHole/BulletHoleComponentProperty.tscn"),
	"ChargedShotProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/ChargedShot/ChargedShotComponentProperty.tscn"),
	"ChargeGlowProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/ChargeGlow/ChargeGlowComponentProperty.tscn"),
	"ChargeBarProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/ChargeBar/ChargeBarComponentProperty.tscn"),
	"BulletProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/Bullet/BulletComponentProperty.tscn"),
	"ProjectileDetonationProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/ProjectileDetonation/ProjectileDetonationComponentProperty.tscn"),
	"AreaDamageProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/AreaDamage/AreaDamageComponentProperty.tscn"),
	"ReadyCallbackProperty": preload("res://Scenes/Entities/Components/Properties/ReadyCallback/ReadyCallbackComponentProperty.tscn"),
	"DefensePenetrationProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/DefensePenetration/DefensePenetrationComponentProperty.tscn"),
	"ExtraShotsProperty": preload("res://Scenes/Entities/Components/Properties/WeaponProperties/ExtraShots/ExtraShotsComponentProperty.tscn"),
	"AreaEffect": preload("res://Scenes/Entities/Ephemeral/AreaEffect/AreaEffect.tscn"),
	"ExplosionEffect": preload("res://Scenes/Detritus/Effects/Explosion/ExplosionEffect.tscn"),
	"EnergyGlowEffect": preload("res://Scenes/Detritus/Effects/EnergyGlow/EnergyGlow.tscn"),
	"Bullet": preload("res://Scenes/Entities/Ephemeral/Bullet/Bullet.tscn"),
	"BaseController": load("res://Scenes/Entities/Controllers/Base/BaseController.tscn"),
	"BaseUnitController": load("res://Scenes/Entities/Controllers/BaseUnit/BaseUnitController.tscn"),
	"TargetAcquisitionBehavior": load("res://Scenes/Entities/Components/Properties/Behaviors/TargetAcquisition/TargetAcquisitionBehavior.tscn"),
	"BaseAttackBehavior": load("res://Scenes/Entities/Components/Properties/Behaviors/Attack/BaseAttackBehavior.tscn"),
	"BaseEngineeringBehavior": load("res://Scenes/Entities/Components/Properties/Behaviors/Engineering/BaseEngineeringBehavior.tscn"),
	"BaseMiningBehavior": load("res://Scenes/Entities/Components/Properties/Behaviors/Mining/BaseMiningBehavior.tscn"),
};

const BULLET_HOLE = preload("res://Scenes/Detritus/BulletHole.tscn")
const RANGE_INDICATOR = preload("res://Scenes/GUI/RangeIndicator/RangeIndicator.tscn")
const COMMODITY_DISPLAY_SCENE = preload("res://Scenes/GUI/CommodityDisplay/CommodityDisplay.tscn");


@export var _base_color := Vector3(0.05, 0.05, 0.05);
@export var _edge_color := Vector3(0.95, 0.05, 0.05);
@export var _shield_color := Color(0.95, 0.05, 0.75);
@export var _energy_color := Color(0.95, 0.05, 0.75, 0.5);
@export var _edge_thickness := 1;
@export var _icon_base_color: Color;
@export var _icon_edge_color: Color;


var _materials := {};
var _meshes := {};
var _component_descriptions := {};
var _unit_descriptions := {};
var _units := {};
var _building_options := [];


func _ready():
	initialize_materials();
	initialize_meshes();
	initialize_component_descriptions();
	initialize_unit_descriptions();
	initialize_units();
	initialize_building_options();
	
	_icon_edge_color = Color(_base_color.x,
						   _base_color.y,
						   _base_color.z);
	
	_icon_base_color = Color(_edge_color.x,
						   _edge_color.y,
						   _edge_color.z);


func initialize_materials():
	BoxMeshMaterialDescription.add_component(self);
	CylinderMeshMaterialDescription.add_component(self);
	CapsuleMeshMaterialDescription.add_component(self);
	ShieldMaterialDescription.add_component(self);


func initialize_meshes():
	BoxMeshDescription.add_component(self);
	CylinderMeshDescription.add_component(self);
	CapsuleMeshDescription.add_component(self);


func initialize_component_descriptions():
	add_component_description(StorageComponentDescription.new(), "Storage");
	add_component_description(GeneratorComponentDescription.new(), "Generator");
	add_component_description(ConduitComponentDescription.new(), "Conduit");
	add_component_description(MotivatorComponentDescription.new(), "Motivator");
	add_component_description(MassCollectorComponentDescription.new(), "Mass Collector");
	add_component_description(ConstructionBeamComponentDescription.new(), "Construction Beam");
	add_component_description(RifleComponentDescription.new(), "Rifle");
	add_component_description(LaserComponentDescription.new(), "Laser");
	add_component_description(ShieldDisruptorComponentDescription.new(), "Shield Disruptor");
	add_component_description(GrenadeLauncherComponentDescription.new(), "Grenade Launcher");
	add_component_description(ShotgunComponentDescription.new(), "Shotgun");


func initialize_unit_descriptions():
	add_unit_description(ConstructionSiteComponentDescription.new(), "Construction Site");
	add_unit_description(PowerGeneratorComponentDescription.new(), "Power Generator");
	add_unit_description(EnergyStorageComponentDescription.new(), "Energy Storage");
	add_unit_description(MassStorageComponentDescription.new(), "Mass Storage");
	add_unit_description(TransmissionTowerComponentDescription.new(), "Transmission Tower");
	add_unit_description(LaserEmplacementComponentDescription.new(), "Laser Emplacement");
	add_unit_description(EngineeringTowerComponentDescription.new(), "Engineering Tower");
	add_unit_description(MiningTowerComponentDescription.new(), "Mining Tower");
	
	add_unit_description(EnemyComponentDescription.new(), "Enemy");
	add_unit_description(PlayerComponentDescription.new(), "Player");


func initialize_units():
	_units["Construction Site"] = get_unit_description("Construction Site").get_scene();
	_units["Light Power Generator"] = get_unit_description("Power Generator").get_scene();
	_units["Light Energy Storage"] = get_unit_description("Energy Storage").get_scene();
	_units["Light Mass Storage"] = get_unit_description("Mass Storage").get_scene();
	_units["Light Transmission Tower"] = get_unit_description("Transmission Tower").get_scene();
	_units["Light Laser Emplacement"] = get_unit_description("Laser Emplacement").get_scene();
	_units["Light Engineering Tower"] = get_unit_description("Engineering Tower").get_scene();
	_units["Light Mining Tower"] = get_unit_description("Mining Tower").get_scene();
	
	_units["Enemy"] = get_unit_description("Enemy").get_scene();
	_units["Player"] = get_unit_description("Player").get_scene();


func initialize_building_options():
	var voxels := get_node("../../Voxels") as Voxels;
	
	await voxels.voxels_ready;
	
	#add wall option
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Wall";
	building_option._voxel_id = voxels._wall_voxel_id;
	building_option._mass_cost = 2.0;
	building_option._energy_cost = 20.0;
	building_option._progress_cost = 0.75;
						
	building_option._icon = StrategicIcons.structure(_icon_base_color, _icon_edge_color);
	building_option._icon = StrategicIcons.wall(building_option._icon, 8, 8, Color(0, 0, 0, 0), _icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _icon_base_color, _icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _icon_base_color, _icon_edge_color);
	building_option._icon.generate_mipmaps()
	
	add_building_option(building_option);
	
	add_building_option(get_unit_description("Power Generator").get_building_option());
	add_building_option(get_unit_description("Energy Storage").get_building_option());
	add_building_option(get_unit_description("Mining Tower").get_building_option());
	add_building_option(get_unit_description("Mass Storage").get_building_option());
	add_building_option(get_unit_description("Transmission Tower").get_building_option());
	add_building_option(get_unit_description("Laser Emplacement").get_building_option());
	add_building_option(get_unit_description("Engineering Tower").get_building_option());


func add_generator(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy", resource_type = BaseGeneratorComponent.ResourceType.COMMODITY) -> BaseGeneratorComponent:
	return get_component_description("Generator").apply_component_to(chassis, amount, resource_name, resource_type);


func add_storage(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy") -> BaseStorageComponent:
	return get_component_description("Storage").apply_component_to(chassis, amount, resource_name);


func add_conduit(chassis: BaseChassisComponent, range := 7, resource_name := "Energy", color := Color(1.0, 1.0, 0.0, 0.3)) -> BaseConduitComponent:
	return get_component_description("Conduit").apply_component_to(chassis, range, resource_name, color);


func add_rifle(chassis: BaseChassisComponent):
	return get_component_description("Rifle").apply_component_to(chassis);


func add_laser(chassis: BaseChassisComponent):
	return get_component_description("Laser").apply_component_to(chassis);


func add_shield_disruptor(chassis: BaseChassisComponent):
	return get_component_description("Shield Disruptor").apply_component_to(chassis);


func add_grenade_launcher(chassis: BaseChassisComponent):
	return get_component_description("Grenade Launcher").apply_component_to(chassis);


func add_shotgun(chassis: BaseChassisComponent):
	return get_component_description("Shotgun").apply_component_to(chassis);


func add_box(component, is_instance = false):
	var collision = CollisionShape3D.new();
	collision.name = "Collision"
	collision.shape = BoxShape3D.new();
	add_component_to(collision, component);
	
	var mesh = get_mesh("Box")
	mesh.name = "Mesh";
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.5, 0.5));
	if is_instance:
		mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, component);


func get_component_scene(scene_name: String):
	return _component_scenes[scene_name].instantiate(PackedScene.GEN_EDIT_STATE_INSTANCE);


func get_mesh(mesh_name: String) -> MeshInstance3D:
	return _meshes[mesh_name].instantiate(PackedScene.GEN_EDIT_STATE_INSTANCE) as MeshInstance3D


func add_component_description(component_description, component_name):
	component_description._faction = self;
	_component_descriptions[component_name] = component_description;


func get_component_description(component_name: String):
	return _component_descriptions[component_name];


func add_unit_description(unit_description, unit_name):
	unit_description._faction = self;
	_unit_descriptions[unit_name] = unit_description;


func get_unit_description(unit_name: String):
	return _unit_descriptions[unit_name];


func apply_stat_override(component: Node, stat: String, score: float):
	var stat_override := StatOverride.new();
	stat_override.name = stat;
	stat_override._score = score;
	add_component_to(stat_override, component);
	return stat_override;


func set_owner_recursive(component, component_owner):
	Util.set_owner_recursive(component, component_owner);


func add_component_to(component, parent_component):
	parent_component.add_child(component);
	if parent_component.owner != null:
		set_owner_recursive(component, parent_component.owner);
	else:
		set_owner_recursive(component, parent_component);
	
func add_building_option(building_option):
	_building_options.append(building_option);
	add_child(building_option);
