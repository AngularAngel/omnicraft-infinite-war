extends Node

class_name StatModifier


var _function: Callable;


func modify_stat(value):
	return _function.call(value);
