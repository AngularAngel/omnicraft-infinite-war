extends Attribute

class_name Stat

var _previous_score: float : get = get_previous_score;
var _current_score: float : set = set_current_score, get = get_current_score;


func get_previous_score():
	return _previous_score;


func set_current_score(_new_score):
	if _new_score != _current_score:
		_previous_score = _current_score;
		_current_score = _new_score;
		changed.emit()


func get_current_score():
	return _current_score;


func modify_current_score(change):
	set_current_score(get_current_score() + change);


func _recalculate():
	pass;
