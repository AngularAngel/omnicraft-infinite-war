extends Stat

class_name PrimaryStat


signal base_changed;


@export var _base_score := 100.0 : set = set_base_score;


func _ready():
	recalculate();


func set_base_score(new_score: float):
	if new_score != _base_score:
		_base_score = new_score
		base_changed.emit();
		recalculate();


func modify_base_score(change):
	set_base_score(_base_score + change);

func modify_base_score_with_min(change, minimum := 0):
	set_base_score(max(_base_score + change, minimum));


func recalculate():
	var cur_score = _base_score;
	for child in get_children():
		if child.has_method("modify_stat"):
			cur_score = child.modify_stat(cur_score);
	set_current_score(cur_score);
