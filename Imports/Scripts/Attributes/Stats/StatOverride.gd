extends Node

class_name StatOverride


@export var _score := 100.0


func _ready():
	var parent = get_parent();
	
	if not parent.is_node_ready():
		await parent.ready;
	#print("%s, %s: %s" % [get_parent(), get_name(), _score]);
	parent._stats.get_stat(get_name()).set_base_score(_score);
	queue_free();
