extends Node3D

class_name BuildingPreview


@export var _range_groups := [];


func show_range_groups_for_faction(faction: Faction):
	for group in _range_groups:
		get_tree().call_group(group, "show_range_for_faction", faction);


func hide_range_groups():
	for group in _range_groups:
		get_tree().call_group(group, "hide_range");
