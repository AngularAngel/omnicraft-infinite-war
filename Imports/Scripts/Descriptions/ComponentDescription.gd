class_name ComponentDescription;


const VALUE_SETTING = preload("res://Scenes/GUI/Common/Settings/Value/ValueSetting.tscn");
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")
const BUDGETED_COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/BudgetedComponentConfig/BudgetedComponentConfig.tscn")
const COMMODITY_DISPLAY_SCENE = preload("res://Scenes/GUI/CommodityDisplay/CommodityDisplay.tscn");
const RANGE_INDICATOR = preload("res://Scenes/GUI/RangeIndicator/RangeIndicator.tscn")


var _faction: Faction;


func add_generator(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy", resource_type = BaseGeneratorComponent.ResourceType.COMMODITY) -> BaseGeneratorComponent:
	return _faction.add_generator(chassis, amount, resource_name, resource_type);


func add_storage(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy") -> BaseStorageComponent:
	return _faction.add_storage(chassis, amount, resource_name);

func add_box(component, is_instance = false):
	return _faction.add_box(component, is_instance);


func get_component_scene(scene_name: String):
	return _faction.get_component_scene(scene_name);


func get_mesh(mesh_name: String) -> MeshInstance3D:
	return _faction.get_mesh(mesh_name);


func apply_stat_override(component: Node, stat: String, score: float):
	return _faction.apply_stat_override(component, stat, score);


func set_owner_recursive(component, component_owner):
	return _faction.set_owner_recursive(component, component_owner);


func add_component_to(component, parent_component):
	return _faction.add_component_to(component, parent_component);
