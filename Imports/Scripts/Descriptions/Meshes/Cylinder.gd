extends ComponentDescription

class_name CylinderMeshDescription;

static func add_component(faction: Faction):
	var cylinder_mesh = MeshInstance3D.new();
	cylinder_mesh.name = "Mesh";
	cylinder_mesh.mesh = CylinderMesh.new()
	cylinder_mesh.mesh.resource_local_to_scene = true;
	cylinder_mesh.mesh.material = faction._materials["CylinderMesh"];
	
	var scene = PackedScene.new();
	scene.pack(cylinder_mesh)
	
	faction._meshes["Cylinder"] = scene;
