extends ComponentDescription

class_name BoxMeshDescription;

static func add_component(faction: Faction):
	var box_mesh = MeshInstance3D.new();
	box_mesh.name = "Mesh";
	box_mesh.mesh = BoxMesh.new()
	box_mesh.mesh.resource_local_to_scene = true;
	box_mesh.mesh.material = faction._materials["BoxMesh"];
	
	var scene = PackedScene.new();
	scene.pack(box_mesh)
	
	faction._meshes["Box"] = scene;
