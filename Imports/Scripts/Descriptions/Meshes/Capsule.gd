extends ComponentDescription

class_name CapsuleMeshDescription;

static func add_component(faction: Faction):
	var capsule_mesh = MeshInstance3D.new();
	capsule_mesh.name = "Mesh";
	capsule_mesh.mesh = CapsuleMesh.new()
	capsule_mesh.mesh.resource_local_to_scene = true;
	capsule_mesh.mesh.material = faction._materials["CapsuleMesh"];
	capsule_mesh.set_instance_shader_parameter("texel_scale", Vector2(4, 4));
	
	var scene = PackedScene.new();
	scene.pack(capsule_mesh)
	
	faction._meshes["Capsule"] = scene;
