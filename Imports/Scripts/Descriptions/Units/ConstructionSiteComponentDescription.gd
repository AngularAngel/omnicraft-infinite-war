extends ComponentDescription

class_name ConstructionSiteComponentDescription;


func get_scene():
	var construction_site := get_component_scene("ConstructionSite") as ConstructionSite;
	var collision = CollisionShape3D.new();
	collision.name = "Collision"
	collision.shape = BoxShape3D.new();
	construction_site.set_scale(Vector3(0.9, 1.0, 0.9));
	add_component_to(collision, construction_site);
	
	var chassis := construction_site.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, construction_site);
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	add_box(chassis)
	
	var mesh = chassis.get_node("Mesh");
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.75));
	
	var scene = PackedScene.new();
	scene.pack(construction_site)
	
	return scene;
