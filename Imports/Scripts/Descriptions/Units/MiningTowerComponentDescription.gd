extends ComponentDescription

class_name MiningTowerComponentDescription;


func get_scene():
	var light_mining_tower := get_component_scene("BaseBuildingUnit") as BaseBuildingUnit;
	light_mining_tower.name = "LightMiningTower";
	light_mining_tower._dimensions = Vector3i(1, 3, 1);
	var shape = CylinderShape3D.new();
	shape.height = 3;
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, light_mining_tower);
	
	var chassis = light_mining_tower.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, light_mining_tower);
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	shape = CylinderShape3D.new();
	shape.radius = 0.35;
	shape.height = 1.8;
	collision = CollisionShape3D.new();
	collision.shape = shape;
	chassis.transform.origin.y = -0.5;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Cylinder")
	mesh.mesh.top_radius = 0.17;
	mesh.mesh.height = 1.8;
	mesh.set_instance_shader_parameter("texel_scale", Vector2(1.5, 2));
	add_component_to(mesh, chassis);
	
	add_storage(chassis, 25);
	
	var mass_storage = add_storage(chassis, 8, "Mass");
	mass_storage.name = "MassStorage";
	
	var mass_collector = _faction.get_component_description("Mass Collector").apply_component_to(chassis);
	mass_collector.transform.origin.y = 1.05;
	
	apply_stat_override(mass_collector, "Range", 10);
	apply_stat_override(mass_collector, "Progress Regen", 0.5);
	apply_stat_override(mass_collector, "Mass Efficiency", 0.9);
	
	mesh = mass_collector.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.6, 0.375));
	
	var ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, mass_collector);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent();
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", -360, 5.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	var conduit = _faction.add_conduit(chassis, 0);
	conduit.transform.origin.y = 1.375;
	conduit.rotation_degrees.y = 45;
	conduit.scale = Vector3(0.6, 0.35, 0.6);
	
	apply_stat_override(conduit, "Transfer Rate", 0);
	apply_stat_override(conduit, "Transfer Speed", 0);
	apply_stat_override(conduit, "Priority", 2);
	apply_stat_override(conduit, "Current Reception", 22);
	apply_stat_override(conduit, "Maximum Reception", 22);
	apply_stat_override(conduit, "Reception Regen", 20);
	
	mesh = conduit.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.375));
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, conduit);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent() as BaseConduitComponent;
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", 360, 10.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	conduit = _faction.add_conduit(chassis, 15, "Mass", Color(0, 0, 0.6, 0.3));
	conduit.transform.origin.y = 1.75;
	conduit.scale = Vector3(0.75, 0.4, 0.75);
	
	apply_stat_override(conduit, "Transfer Rate", 10);
	apply_stat_override(conduit, "Transfer Speed", 3);
	apply_stat_override(conduit, "Priority", -2);
	apply_stat_override(conduit, "Current Reception", 0);
	apply_stat_override(conduit, "Maximum Reception", 0);
	apply_stat_override(conduit, "Reception Regen", 0);
	
	mesh = conduit.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.375));
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, conduit);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent() as BaseConduitComponent;
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", -360, 20.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	var controller = get_component_scene("BaseUnitController") as BaseUnitController;
	controller.name = "Controller"
	add_component_to(controller, light_mining_tower);
	
	var base_mining = get_component_scene("BaseMiningBehavior") as BaseMiningBehavior;
	add_component_to(base_mining, controller)
	
	var scene = PackedScene.new();
	scene.pack(light_mining_tower);
	
	return scene;


func get_building_option():
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Mining Tower"
	building_option._building_scene = _faction._units["Light Mining Tower"];
	building_option._mass_cost = 4.0;
	building_option._energy_cost = 60.0;
	building_option._progress_cost = 2.0;
						
	building_option._icon = StrategicIcons.structure(_faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.mining(building_option._icon, 8, 8, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon.generate_mipmaps();
	
	var energy_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	energy_display._resource_name = "Energy";
	energy_display.transform.origin.y = -0.5;
	
	var mass_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	mass_display._resource_name = "Mass";
	mass_display.transform.origin.y = -0.5;
	
	var double_display = Node3D.new();
	add_component_to(energy_display, double_display);
	mass_display.transform.origin.y = -0.25;
	add_component_to(mass_display, double_display);
	
	var double_display_scene = PackedScene.new();
	double_display_scene.pack(double_display);
	
	building_option._commodity_display_scene = double_display_scene;
	
	var preview = BuildingPreview.new();
	preview._range_groups.append("EnergyConduits");
	preview._range_groups.append("EngineeringRanges");
	preview._range_groups.append("MiningRanges");
	
	var range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(0.0, 0.0, 0.2, 0.8));
	range_indicator.set_range(10);
	range_indicator.transform.origin = Vector3(0.5, 2.05, 0.5);
	
	add_component_to(range_indicator, preview);
	
	range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(0.0, 0.0, 0.6, 0.3));
	range_indicator.set_range(15);
	range_indicator.transform.origin = Vector3(0.5, 2.75, 0.5);
	
	add_component_to(range_indicator, preview);

	var scene = PackedScene.new();
	scene.pack(preview);

	building_option._preview_scene = scene;
	return building_option;
