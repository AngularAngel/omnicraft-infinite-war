extends ComponentDescription

class_name EngineeringTowerComponentDescription;


func get_scene():
	var light_engineering_tower := get_component_scene("BaseBuildingUnit") as BaseBuildingUnit;
	light_engineering_tower.name = "LightEngineeringTower";
	light_engineering_tower._dimensions = Vector3i(1, 3, 1);
	var shape = CylinderShape3D.new();
	shape.height = 3;
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, light_engineering_tower);
	
	var chassis = light_engineering_tower.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, light_engineering_tower);
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	shape = CylinderShape3D.new();
	shape.radius = 0.35;
	shape.height = 1.8;
	collision = CollisionShape3D.new();
	collision.shape = shape;
	chassis.transform.origin.y = -0.5;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Cylinder")
	mesh.mesh.top_radius = 0.17;
	mesh.mesh.height = 1.8;
	mesh.set_instance_shader_parameter("texel_scale", Vector2(1.5, 2));
	add_component_to(mesh, chassis);
	
	add_storage(chassis, 25);
	
	var mass_storage = add_storage(chassis, 8, "Mass");
	mass_storage.name = "MassStorage";
	
	var construction_beam = _faction.get_component_description("Construction Beam").apply_component_to(chassis);
	construction_beam.transform.origin.y = 1.05;
	
	apply_stat_override(construction_beam, "Range", 10);
	apply_stat_override(construction_beam, "Build Speed", 0.5);
	
	mesh = construction_beam.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.6, 0.375));
	
	var ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, construction_beam);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent();
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", -360, 5.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	var conduit = _faction.add_conduit(chassis, 0);
	conduit.transform.origin.y = 1.375;
	conduit.rotation_degrees.y = 45;
	conduit.scale = Vector3(0.6, 0.35, 0.6);
	
	apply_stat_override(conduit, "Transfer Rate", 0);
	apply_stat_override(conduit, "Transfer Speed", 0);
	apply_stat_override(conduit, "Priority", 2);
	apply_stat_override(conduit, "Current Reception", 22);
	apply_stat_override(conduit, "Maximum Reception", 22);
	apply_stat_override(conduit, "Reception Regen", 20);
	
	mesh = conduit.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.375));
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, conduit);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent() as BaseConduitComponent;
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", 360, 10.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	conduit = _faction.add_conduit(chassis, 0, "Mass");
	conduit.transform.origin.y = 1.75;
	conduit.scale = Vector3(0.75, 0.4, 0.75);
	
	apply_stat_override(conduit, "Transfer Rate", 0);
	apply_stat_override(conduit, "Transfer Speed", 0);
	apply_stat_override(conduit, "Priority", 2);
	apply_stat_override(conduit, "Current Reception", 6);
	apply_stat_override(conduit, "Maximum Reception", 6);
	apply_stat_override(conduit, "Reception Regen", 5);
	
	mesh = conduit.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.375));
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, conduit);
	
	ready_callback._callback = func(this):
		var parent = this.get_parent() as BaseConduitComponent;
		var tween: Tween = this.get_tree().create_tween();
		tween.tween_property(parent, "rotation_degrees:y", -360, 20.0);
		tween.tween_callback(func():
			parent.rotation_degrees.y = 0;
		);
		tween.set_loops();
	
	var controller = get_component_scene("BaseUnitController") as BaseUnitController;
	controller.name = "Controller"
	add_component_to(controller, light_engineering_tower);
	
	var base_engineering = get_component_scene("BaseEngineeringBehavior") as BaseEngineeringBehavior;
	add_component_to(base_engineering, controller);
	
	var target_acquisition = get_component_scene("TargetAcquisitionBehavior") as TargetAcquisitionBehavior;
	add_component_to(target_acquisition, controller);
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, controller)
	
	ready_callback._callback = func(this):
		var parent = this.get_parent() as BaseController;
		base_engineering = parent.get_node("BaseEngineeringBehavior") as BaseEngineeringBehavior;
		target_acquisition = parent.get_node("TargetAcquisitionBehavior") as TargetAcquisitionBehavior;
		target_acquisition.add_to_group("EngineeringRanges", true);
		target_acquisition._range_indicator.set_color(Color(0.0, 0.7, 0.0, 0.3));
		target_acquisition._target_area._area.collision_mask = 4;
		target_acquisition._target_area._target_condition = func(target):
			return target.has_method("get_faction") and target.get_faction() == parent.get_faction() and target.has_method("get_unit") and target.get_unit() != parent.get_parent();
		target_acquisition._target_priority_function = func(_this, target):
			var priority = -10.0;
			if target.get_meta("isRepairable", false):
				for defensive_layer: DefensiveLayerComponentProperty in target._defense_layers:
					if defensive_layer._health.get_current_percent() < 1.0:
						priority = 5 * (1.0 - defensive_layer._health.get_current_percent());
			if target.has_method("get_unit") and target.get_unit().get_meta("isConstructionSite", false):
				priority = target.get_unit()._progress.get_current_percent();
			return priority;
		target_acquisition.target_acquired.connect(base_engineering.set_target);
		await parent.ready;
		target_acquisition.set_range(base_engineering._construction_beam._range.get_current_score());
		base_engineering._construction_beam._range.changed.connect(
			func():
				target_acquisition.set_range(base_engineering._construction_beam._range.get_current_score());
		);
	
	var scene = PackedScene.new();
	scene.pack(light_engineering_tower);
	
	return scene;


func get_building_option():
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Engineering Tower"
	building_option._building_scene = _faction._units["Light Engineering Tower"];
	building_option._mass_cost = 4.0;
	building_option._energy_cost = 60.0;
	building_option._progress_cost = 2.0;
						
	building_option._icon = StrategicIcons.structure(_faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.engineering(building_option._icon, 8, 8, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon.generate_mipmaps();
	
	var energy_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	energy_display._resource_name = "Energy";
	energy_display.transform.origin.y = -0.5;
	
	var mass_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	mass_display._resource_name = "Mass";
	mass_display.transform.origin.y = -0.5;
	
	var double_display = Node3D.new();
	add_component_to(energy_display, double_display);
	mass_display.transform.origin.y = -0.25;
	add_component_to(mass_display, double_display);
	
	var double_display_scene = PackedScene.new();
	double_display_scene.pack(double_display);
	
	building_option._commodity_display_scene = double_display_scene;
	
	var preview = BuildingPreview.new();
	preview._range_groups.append("EnergyConduits");
	preview._range_groups.append("MassConduits");
	preview._range_groups.append("EngineeringRanges");
	
	var range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(0.0, 0.7, 0.0, 0.3));
	range_indicator.set_range(10);
	range_indicator.transform.origin = Vector3(0.5, 2.05, 0.5);
	
	add_component_to(range_indicator, preview);

	var scene = PackedScene.new();
	scene.pack(preview);

	building_option._preview_scene = scene;
	return building_option;
