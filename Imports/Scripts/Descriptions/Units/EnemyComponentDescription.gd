extends ComponentDescription

class_name EnemyComponentDescription;


func get_scene():
	var enemy := get_component_scene("BaseMobileUnit") as BaseMobileUnit;
	enemy.name = "Enemy";
	
	var shape = CapsuleShape3D.new();
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, enemy);
	
	var chassis = enemy.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, enemy);
	
	apply_stat_override(chassis.get_node("Armor"), "MaximumHealth", 25);
	apply_stat_override(chassis.get_node("Armor"), "CurrentHealth", 25);
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 50);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 50);
	
	shape = CapsuleShape3D.new();
	collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Capsule")
	mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, chassis);
	
	mesh = get_mesh("Capsule");
	mesh.name = "Shield Mesh"
	mesh.set_scale(Vector3(1.5, 1.5, 1.5));
	mesh.set_instance_shader_parameter("texel_scale", Vector2(3, 3));
	mesh.set_surface_override_material(0, _faction._materials["Shield"]);
	add_component_to(mesh, chassis);
	
	var battery = add_storage(chassis, 100);
	apply_stat_override(battery, "Current Storage", 100);
	
	var mass_storage = add_storage(chassis, 20, "Mass");
	mass_storage.name = "MassStorage";
	apply_stat_override(mass_storage, "Current Storage", 20);
	
	add_generator(chassis, 5);
	
	var mass_generator = add_generator(chassis, 0.2, "Mass");
	mass_generator.name = "MassGenerator";
	apply_stat_override(mass_generator, "Energy Cost", 20);
	
	var shield_generator = add_generator(chassis, 3, "Shield", BaseGeneratorComponent.ResourceType.DEFENSE);
	shield_generator.name = "ShieldGenerator";
	apply_stat_override(shield_generator, "Energy Cost", 3);
	
	
	
	var motivator = _faction.get_component_description("Motivator").apply_component_to(chassis);
	
	apply_stat_override(motivator, "Speed", 0.85);
	
	collision = CollisionShape3D.new();
	collision.shape = BoxShape3D.new();
	add_component_to(collision, motivator);
	
	mesh = get_mesh("Box");
	mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, motivator);
	
	var weapon = _faction.add_rifle(chassis);
	weapon.transform.origin = Vector3(0, -0.25, -0.5);
	
	weapon.get_node("Damage")._score = 15.0;
	weapon.get_node("ShieldPenetration/DamageMultiplier")._score = 1.5;
	weapon.get_node("ReloadingComponentProperty/Progress Regen")._score = 1.0;
	
	var scene = PackedScene.new();
	scene.pack(enemy)
	
	return scene;
