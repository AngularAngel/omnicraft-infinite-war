extends ComponentDescription

class_name MassStorageComponentDescription;


func get_scene():
	var mass_store := get_component_scene("BaseBuildingUnit") as BaseBuildingUnit;
	mass_store.name = "LightMassStorage";
	mass_store._dimensions = Vector3i(1, 2, 1);
	var shape = CylinderShape3D.new();
	shape.height = 2;
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, mass_store);
	
	var chassis = mass_store.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, mass_store);
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	shape = CylinderShape3D.new();
	shape.height = 1.4;
	collision = CollisionShape3D.new();
	collision.shape = shape;
	chassis.transform.origin.y = -0.3;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Cylinder")
	mesh.mesh.height = 1.4;
	mesh.set_instance_shader_parameter("texel_scale", Vector2(2, 2));
	add_component_to(mesh, chassis);
	
	add_storage(chassis, 50, "Mass");
	
	var conduit = _faction.add_conduit(chassis, 6, "Mass", Color(0, 0, 0.6, 0.3));
	conduit.transform.origin.y = 1;
	
	apply_stat_override(conduit, "Transfer Rate", 4);
	apply_stat_override(conduit, "Transfer Speed", 1.5);
	apply_stat_override(conduit, "Current Reception", 5);
	apply_stat_override(conduit, "Maximum Reception", 5);
	apply_stat_override(conduit, "Reception Regen", 4);
	
	var scene = PackedScene.new();
	scene.pack(mass_store);
	
	return scene;


func get_building_option():
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Mass Storage"
	building_option._building_scene = _faction._units["Light Mass Storage"];
	building_option._mass_cost = 6.0;
	building_option._energy_cost = 40.0;
	building_option._progress_cost = 2.0;
						
	building_option._icon = StrategicIcons.structure(_faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.mass_storage(building_option._icon, 8, 8, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon.generate_mipmaps();
	
	var mass_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	mass_display._resource_name = "Mass";
	mass_display.transform.origin.y = -0.5;
	
	var mass_display_scene = PackedScene.new();
	mass_display_scene.pack(mass_display);
	
	building_option._commodity_display_scene = mass_display_scene;
	
	var preview = BuildingPreview.new();
	preview._range_groups.append("MassConduits");
	preview._range_groups.append("EngineeringRanges");
	
	var range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(0.0, 0.0, 0.6, 0.3));
	range_indicator.set_range(6);
	range_indicator.transform.origin = Vector3(0.5, 1.7, 0.5);
	
	add_component_to(range_indicator, preview);

	var scene = PackedScene.new();
	scene.pack(preview);

	building_option._preview_scene = scene;
	return building_option;
