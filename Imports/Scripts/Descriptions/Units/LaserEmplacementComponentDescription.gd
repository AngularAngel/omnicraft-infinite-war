extends ComponentDescription

class_name LaserEmplacementComponentDescription;


func get_scene():
	var laser_emplacement := get_component_scene("BaseBuildingUnit") as BaseBuildingUnit;
	laser_emplacement.name = "LightLaserEmplacement";
	laser_emplacement._dimensions = Vector3i(1, 2, 1);
	var shape = BoxShape3D.new();
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, laser_emplacement);
	
	var chassis = laser_emplacement.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, laser_emplacement);
	
	add_box(chassis, true);
	
	chassis.transform.origin.y = -0.3;
	chassis.transform.origin.z = -0.075;
	for child in chassis.get_children():
		if child.has_method("set_scale"):
			child.set_scale(Vector3(0.8, 1.4, 0.8));
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	add_storage(chassis, 10);
	
	var conduit = _faction.add_conduit(chassis, 0);
	conduit.transform.origin = Vector3(0, 0.75, 0.25);
	
	conduit.get_node("Mesh").set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	
	apply_stat_override(conduit, "Priority", 2);
	apply_stat_override(conduit, "Current Reception", 12);
	apply_stat_override(conduit, "Maximum Reception", 12);
	apply_stat_override(conduit, "Reception Regen", 10);
	apply_stat_override(conduit, "Transfer Rate", 0);
	apply_stat_override(conduit, "Transfer Speed", 0);
	
	var weapon = _faction.add_laser(chassis);
	weapon.transform.origin = Vector3(-0.25, 0, -0.5);
	
	var controller = get_component_scene("BaseUnitController") as BaseUnitController;
	controller.name = "Controller"
	add_component_to(controller, laser_emplacement);
	
	var base_attack = get_component_scene("BaseAttackBehavior") as BaseAttackBehavior;
	add_component_to(base_attack, controller);
	
	var target_acquisition = get_component_scene("TargetAcquisitionBehavior") as TargetAcquisitionBehavior;
	add_component_to(target_acquisition, controller);
	
	var ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, controller)
	
	ready_callback._callback = func(this):
		var parent = this.get_parent();
		base_attack = parent.get_node("BaseAttackBehavior") as BaseAttackBehavior;
		target_acquisition = parent.get_node("TargetAcquisitionBehavior") as TargetAcquisitionBehavior;
		target_acquisition.add_to_group("AttackRanges", true);
		target_acquisition._range_indicator.set_color(Color(1.0, 0.0, 0.0, 0.3));
		target_acquisition._target_area._area.collision_mask = 1;
		target_acquisition._target_area._target_condition = func(target):
			return target.has_method("get_faction") and target.get_faction() != parent.get_faction();
		target_acquisition.target_acquired.connect(base_attack.set_target);
		await parent.ready;
		target_acquisition.set_range(base_attack._weapon._range.get_current_score());
		base_attack._weapon._range.changed.connect(
			func():
				target_acquisition.set_range(base_attack._weapon._range.get_current_score());
		);
	
	var scene = PackedScene.new();
	scene.pack(laser_emplacement);
	
	return scene;


func get_building_option():
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Laser Emplacement"
	building_option._building_scene = _faction._units["Light Laser Emplacement"];
	building_option._mass_cost = 15.0;
	building_option._energy_cost = 200.0;
	building_option._progress_cost = 5.0;
						
	building_option._icon = StrategicIcons.structure(_faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.direct_fire(building_option._icon, 8, 8, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon.generate_mipmaps();
	
	var energy_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	energy_display._resource_name = "Energy";
	energy_display.transform.origin.y = -0.5;
	
	var energy_display_scene = PackedScene.new();
	energy_display_scene.pack(energy_display);
	
	building_option._commodity_display_scene = energy_display_scene;
	
	var preview = BuildingPreview.new();
	preview._range_groups.append("EnergyConduits");
	preview._range_groups.append("AttackRanges");
	preview._range_groups.append("EngineeringRanges");
	
	var range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(1.0, 0.0, 0.0, 0.3));
	range_indicator.set_range(40);
	range_indicator.transform.origin = Vector3(0.5, 1.7, 0.5);
	
	add_component_to(range_indicator, preview);

	var scene = PackedScene.new();
	scene.pack(preview);

	building_option._preview_scene = scene;
	return building_option;
