extends ComponentDescription

class_name PlayerComponentDescription;


func get_scene():
	var player := get_component_scene("BaseMobileUnit") as BaseMobileUnit;
	player.name = "Player";
	
	var shape = CapsuleShape3D.new();
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, player);
	
	var chassis = player.get_node("Chassis") as BaseChassisComponent;
	
	shape = CapsuleShape3D.new();
	collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Capsule");
	mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, chassis);
	
	mesh = get_mesh("Capsule");
	mesh.name = "Shield Mesh"
	mesh.set_scale(Vector3(1.5, 1.5, 1.5));
	mesh.set_instance_shader_parameter("texel_scale", Vector2(3, 3));
	mesh.set_surface_override_material(0, _faction._materials["Shield"]);
	add_component_to(mesh, chassis);
	
	var battery = add_storage(chassis, 500);
	apply_stat_override(battery, "Current Storage", 500);
	
	var mass_storage = add_storage(chassis, 100, "Mass");
	mass_storage.name = "MassStorage";
	apply_stat_override(mass_storage, "Current Storage", 100);
	
	add_generator(chassis);
	
	var shield_generator = add_generator(chassis, 4, "Shield", BaseGeneratorComponent.ResourceType.DEFENSE);
	shield_generator.name = "ShieldGenerator";
	apply_stat_override(shield_generator, "Energy Cost", 2);
	
	#mass_generator = add_generator(chassis, 0.4, "Mass");
	#mass_generator.name = "MassGenerator";
	#apply_stat_override(mass_generator, "Energy Cost", 15);
	
	var conduit = _faction.add_conduit(chassis, 5);
	conduit.transform.origin = Vector3(-0.35, 0, 0.85);
	apply_stat_override(conduit, "Transfer Rate", 15);
	apply_stat_override(conduit, "Transfer Speed", 3);
	apply_stat_override(conduit, "Priority", 2);
	apply_stat_override(conduit, "Current Reception", 40);
	apply_stat_override(conduit, "Maximum Reception", 40);
	apply_stat_override(conduit, "Reception Regen", 30);
	conduit.get_node("Mesh").set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	
	conduit = _faction.add_conduit(chassis, 5, "Mass", Color(0, 0, 0.6, 0.3));
	conduit.transform.origin = Vector3(0.35, 0, 0.85);
	apply_stat_override(conduit, "Transfer Rate", 5);
	apply_stat_override(conduit, "Transfer Speed", 1);
	apply_stat_override(conduit, "Priority", 0);
	apply_stat_override(conduit, "Current Reception", 10);
	apply_stat_override(conduit, "Maximum Reception", 10);
	apply_stat_override(conduit, "Reception Regen", 8);
	conduit.get_node("Mesh").set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	
	_faction.get_component_description("Motivator").apply_component_to(chassis);
	
	var weapon = _faction.add_shotgun(chassis);
	weapon.transform.origin = Vector3(0, -0.25, -0.5);
	
	var mass_collector = _faction.get_component_description("Mass Collector").apply_component_to(chassis);
	mass_collector.transform.origin = Vector3(0.25, 0, -0.5);
	
	var construction_beam = _faction.get_component_description("Construction Beam").apply_component_to(chassis);
	construction_beam.transform.origin = Vector3(-0.25, 0, -0.5);
	
	var scene = PackedScene.new();
	scene.pack(player);
	
	return scene;


func get_scene_with_loadout(loadout):
	var player := get_component_scene("BaseMobileUnit") as BaseMobileUnit;
	player.name = "Player";
	
	var shape = CapsuleShape3D.new();
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, player);
	
	var chassis = player.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, player);
	
	shape = CapsuleShape3D.new();
	collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Capsule");
	mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, chassis);
	
	mesh = get_mesh("Capsule");
	mesh.name = "Shield Mesh"
	mesh.set_scale(Vector3(1.5, 1.5, 1.5));
	mesh.set_instance_shader_parameter("texel_scale", Vector2(3, 3));
	mesh.set_surface_override_material(0, _faction._materials["Shield"]);
	add_component_to(mesh, chassis);
	
	for component_config in loadout:
		match(component_config["Config Name"]):
			"Chassis":
				apply_stat_override(chassis.get_node("Armor"), "MaximumHealth", component_config["Maximum Armor"]);
				apply_stat_override(chassis.get_node("Armor"), "CurrentHealth", component_config["Maximum Armor"]);
				apply_stat_override(chassis.get_node("Integrity"), "MaximumHealth", component_config["Maximum Integrity"]);
				apply_stat_override(chassis.get_node("Integrity"), "CurrentHealth", component_config["Maximum Integrity"]);
			"Battery":
				_faction.get_component_description("Storage").apply_config_to(chassis, component_config);
			"Mass Storage":
				_faction.get_component_description("Storage").apply_config_to(chassis, component_config, "Mass");
			"Energy Generator":
				_faction.get_component_description("Generator").apply_config_to(chassis, component_config);
			"Shield Generator":
				_faction.get_component_description("Generator").apply_config_to(chassis, component_config, "Shield", BaseGeneratorComponent.ResourceType.DEFENSE);
			"Mass Generator":
				_faction.get_component_description("Generator").apply_config_to(chassis, component_config, "Mass");
			"Energy Conduit":
				var conduit = _faction.get_component_description("Conduit").apply_config_to(chassis, component_config);
				conduit.transform.origin = Vector3(-0.35, 0, 0.85);
			"Mass Conduit":
				var conduit = _faction.get_component_description("Conduit").apply_config_to(chassis, component_config, "Mass",  Color(0, 0, 0.6, 0.3));
				conduit.transform.origin = Vector3(0.35, 0, 0.85);
			"Motivator":
				_faction.get_component_description("Motivator").apply_config_to(chassis, component_config);
			"Mass Collector":
				var mass_collector = _faction.get_component_description("Mass Collector").apply_config_to(chassis, component_config);
				mass_collector.transform.origin = Vector3(0.25, 0, -0.5);
			"Construction Beam":
				var construction_beam = _faction.get_component_description("Construction Beam").apply_config_to(chassis, component_config);
				construction_beam.transform.origin = Vector3(-0.25, 0, -0.5);
			"Rifle":
				_faction.get_component_description("Rifle").apply_component_to(chassis, component_config);
			"Laser":
				_faction.get_component_description("Laser").apply_component_to(chassis, component_config);
			"Shield Disruptor":
				_faction.get_component_description("Shield Disruptor").apply_component_to(chassis, component_config);
			"Grenade Launcher":
				_faction.get_component_description("Grenade Launcher").apply_component_to(chassis, component_config);
			"Shotgun":
				_faction.get_component_description("Shotgun").apply_component_to(chassis, component_config);
	var weapon = chassis.get_node("Weapon");
	if weapon != null:
		weapon.transform.origin = Vector3(0, -0.25, -0.5);
	var scene = PackedScene.new();
	scene.pack(player);
	
	return scene;


static func get_player_chassis_config():
	var chassis_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	chassis_config.set_component_name("Chassis");
	chassis_config._removal_disabled = true;
	var max_armor = VALUE_SETTING.instantiate();
	max_armor._setting_name = "Maximum Armor";
	max_armor._maximum = 500.0;
	max_armor._minimum = 10.0;
	max_armor._step = 1.0;
	max_armor._default_value = 100.0;
	max_armor.set_meta("costMultiplier", 3.0);
	chassis_config.add_setting(max_armor);
	var max_integrity = VALUE_SETTING.instantiate();
	max_integrity._setting_name = "Maximum Integrity";
	max_integrity._maximum = 500.0;
	max_integrity._minimum = 50.0;
	max_integrity._step = 1.0;
	max_integrity._default_value = 100.0;
	max_integrity.set_meta("costMultiplier", 2.0);
	chassis_config.add_setting(max_integrity);
	return chassis_config;
