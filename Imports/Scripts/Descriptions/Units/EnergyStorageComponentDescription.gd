extends ComponentDescription

class_name EnergyStorageComponentDescription;


func get_scene():
	var energy_store := get_component_scene("BaseBuildingUnit") as BaseBuildingUnit;
	energy_store.name = "LightEnergyStorage";
	energy_store._dimensions = Vector3i(1, 2, 1);
	var shape = CylinderShape3D.new();
	shape.height = 2;
	var collision = CollisionShape3D.new();
	collision.shape = shape;
	add_component_to(collision, energy_store);
	
	var chassis = energy_store.get_node("Chassis") as BaseChassisComponent;
	set_owner_recursive(chassis, energy_store);
	
	apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", 0.0);
	apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", 0.0);
	
	shape = CylinderShape3D.new();
	shape.height = 1.4;
	collision = CollisionShape3D.new();
	collision.shape = shape;
	chassis.transform.origin.y = -0.3;
	add_component_to(collision, chassis);
	
	var mesh = get_mesh("Cylinder")
	mesh.mesh.height = 1.4;
	mesh.set_instance_shader_parameter("texel_scale", Vector2(2, 2));
	add_component_to(mesh, chassis);
	
	add_storage(chassis, 250);
	
	var conduit = _faction.add_conduit(chassis, 6);
	conduit.transform.origin.y = 1;
	
	var scene = PackedScene.new();
	scene.pack(energy_store);
	
	return scene;


func get_building_option():
	var building_option = BuildingOption.new();
	building_option.name = "Tech 1 Light Energy Storage"
	building_option._building_scene = _faction._units["Light Energy Storage"];
	building_option._mass_cost = 6.0;
	building_option._energy_cost = 40.0;
	building_option._progress_cost = 2.0;
						
	building_option._icon = StrategicIcons.structure(_faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.power_storage(building_option._icon, 8, 8, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 0, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon = StrategicIcons.add_pips(building_option._icon, 1, 8, 13, _faction._icon_base_color, _faction._icon_edge_color);
	building_option._icon.generate_mipmaps();
	
	var energy_display := COMMODITY_DISPLAY_SCENE.instantiate() as CommodityDisplay;
	energy_display._resource_name = "Energy";
	energy_display.transform.origin.y = -0.5;
	
	var energy_display_scene = PackedScene.new();
	energy_display_scene.pack(energy_display);
	
	building_option._commodity_display_scene = energy_display_scene;
	
	var preview = BuildingPreview.new();
	preview._range_groups.append("EnergyConduits");
	preview._range_groups.append("EngineeringRanges");
	
	var range_indicator = RANGE_INDICATOR.instantiate();
	range_indicator.set_color(Color(1.0, 1.0, 0.0, 0.3));
	range_indicator.set_range(6);
	range_indicator.transform.origin = Vector3(0.5, 1.7, 0.5);
	
	add_component_to(range_indicator, preview);

	var scene = PackedScene.new();
	scene.pack(preview);

	building_option._preview_scene = scene;
	return building_option;
