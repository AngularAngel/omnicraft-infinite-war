extends ComponentDescription

class_name BoxMeshMaterialDescription;

static func add_component(faction: Faction):
	var boxmesh_building_material = ShaderMaterial.new();
	boxmesh_building_material.shader = Shaders._dict["Building"];
	boxmesh_building_material.set_shader_parameter("base_color", faction._base_color);
	boxmesh_building_material.set_shader_parameter("edge_color", faction._edge_color);
	boxmesh_building_material.set_shader_parameter("edge_thickness", faction._edge_thickness);
	boxmesh_building_material.set_shader_parameter("uv1_scale", Vector2(3, 2));
	boxmesh_building_material.set_shader_parameter("is_instance", true);
	boxmesh_building_material.set_shader_parameter("vertical_edges", true);
	boxmesh_building_material.set_shader_parameter("horizontal_edges", true);
	
	faction._materials["BoxMesh"] = boxmesh_building_material;
