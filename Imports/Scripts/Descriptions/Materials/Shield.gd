extends ComponentDescription

class_name ShieldMaterialDescription;

static func add_component(faction: Faction):
	var shield_material = ShaderMaterial.new();
	shield_material.shader = Shaders._dict["Shield"];
	
	faction._materials["Shield"] = shield_material;
