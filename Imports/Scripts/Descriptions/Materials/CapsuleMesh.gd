extends ComponentDescription

class_name CapsuleMeshMaterialDescription;

static func add_component(faction: Faction):
	var capsule_building_material = ShaderMaterial.new();
	capsule_building_material.shader = Shaders._dict["Building"];
	capsule_building_material.set_shader_parameter("base_color", faction._base_color);
	capsule_building_material.set_shader_parameter("edge_color", faction._edge_color);
	capsule_building_material.set_shader_parameter("edge_thickness", faction._edge_thickness);
	capsule_building_material.set_shader_parameter("is_instance", true);
	capsule_building_material.set_shader_parameter("vertical_edges", true);
	
	faction._materials["CapsuleMesh"] = capsule_building_material;
