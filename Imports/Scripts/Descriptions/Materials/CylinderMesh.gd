extends ComponentDescription

class_name CylinderMeshMaterialDescription;

static func add_component(faction: Faction):
	var cylinder_building_material = ShaderMaterial.new();
	cylinder_building_material.shader = Shaders._dict["Building"];
	cylinder_building_material.set_shader_parameter("base_color", faction._base_color);
	cylinder_building_material.set_shader_parameter("edge_color", faction._edge_color);
	cylinder_building_material.set_shader_parameter("edge_thickness", faction._edge_thickness);
	cylinder_building_material.set_shader_parameter("uv1_scale", Vector2(2, 2));
	cylinder_building_material.set_shader_parameter("is_instance", true);
	cylinder_building_material.set_shader_parameter("vertical_edges", true);
	
	faction._materials["CylinderMesh"] = cylinder_building_material;
