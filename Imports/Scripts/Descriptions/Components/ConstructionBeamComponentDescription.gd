extends ComponentDescription

class_name ConstructionBeamComponentDescription;


func apply_component_to(chassis: BaseChassisComponent):
	var construction_beam := get_component_scene("ConstructionBeam") as ConstructionBeam;
	construction_beam.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(construction_beam, chassis);
	
	apply_stat_override(construction_beam, "SurfaceArea", 0.2);
	
	add_box(construction_beam, true);
	return construction_beam;


static func get_component_config():
	var construction_beam_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	construction_beam_config.set_component_name("Construction Beam");
	construction_beam_config.set_meta("baseCost", 400.0);
	var collection_speed = VALUE_SETTING.instantiate();
	collection_speed._setting_name = "Build Speed";
	collection_speed._maximum = 5.0;
	collection_speed._minimum = 0.1;
	collection_speed._step = 0.05;
	collection_speed._default_value = 1.0;
	collection_speed.set_meta("costMultiplier", 200.0);
	construction_beam_config.add_setting(collection_speed);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Build Range";
	range._maximum = 10.0;
	range._minimum = 1.0;
	range._step = 1.0;
	range._default_value = 5.0;
	range.set_meta("costMultiplier", 80.0);
	construction_beam_config.add_setting(range);
	var energy_expenditure = VALUE_SETTING.instantiate();
	energy_expenditure._setting_name = "Energy Expenditure";
	energy_expenditure._maximum = 5.0;
	energy_expenditure._minimum = 1.0;
	energy_expenditure._step = 0.1;
	energy_expenditure._default_value = 1.0;
	energy_expenditure.set_meta("costMultiplier", -250.0);
	construction_beam_config.add_setting(energy_expenditure);
	var mass_expenditure = VALUE_SETTING.instantiate();
	mass_expenditure._setting_name = "Mass Expenditure";
	mass_expenditure._maximum = 5.0;
	mass_expenditure._minimum = 1.0;
	mass_expenditure._step = 0.01;
	mass_expenditure._default_value = 1.0;
	mass_expenditure.set_meta("costMultiplier", -250.0);
	construction_beam_config.add_setting(mass_expenditure);
	return construction_beam_config;


func apply_config_to(chassis: BaseChassisComponent, component_config):
	var construction_beam = apply_component_to(chassis);
	apply_stat_override(construction_beam, "Build Speed", component_config["Build Speed"]);
	apply_stat_override(construction_beam, "Range", component_config["Build Range"]);
	apply_stat_override(construction_beam, "Energy Expenditure", component_config["Energy Expenditure"]);
	apply_stat_override(construction_beam, "Mass Expenditure", component_config["Mass Expenditure"]);
	return construction_beam;
