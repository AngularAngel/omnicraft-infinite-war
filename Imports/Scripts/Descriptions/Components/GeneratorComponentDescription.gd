extends ComponentDescription

class_name GeneratorComponentDescription;


func apply_component_to(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy", resource_type = BaseGeneratorComponent.ResourceType.COMMODITY) -> BaseGeneratorComponent:
	var generator := get_component_scene("Generator") as BaseGeneratorComponent;
	generator._resource_name = resource_name;
	generator._resource_type = resource_type;
	apply_stat_override(generator, "Generation Amount", amount);
	add_component_to(generator, chassis);
	return generator;


static func get_energy_generator_config():
	var generator_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	generator_config.set_component_name("Energy Generator");
	var generation_amount = VALUE_SETTING.instantiate();
	generation_amount._setting_name = "Amount/Sec";
	generation_amount._maximum = 50.0;
	generation_amount._minimum = 0.0;
	generation_amount._step = 1.0;
	generation_amount._default_value = 10.0;
	generation_amount.set_meta("costMultiplier", 50.0);
	generator_config.add_setting(generation_amount);
	return generator_config;


static func get_shield_generator_config():
	var generator_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	generator_config.set_component_name("Shield Generator");
	var maximum_shield := VALUE_SETTING.instantiate() as ValueSetting;
	maximum_shield._setting_name = "Maximum Shield";
	maximum_shield._maximum = 500.0;
	maximum_shield._minimum = 25.0;
	maximum_shield._step = 1.0;
	maximum_shield._default_value = 100.0;
	maximum_shield.set_meta("costMultiplier", 2.0);
	generator_config.add_setting(maximum_shield);
	var generation_amount := VALUE_SETTING.instantiate() as ValueSetting;
	generation_amount._setting_name = "Amount/Sec";
	generation_amount._maximum = 25.0;
	generation_amount._minimum = 0.0;
	generation_amount._step = 1.0;
	generation_amount._default_value = 4.0;
	generation_amount.set_meta("costMultiplier", 100.0);
	generator_config.add_setting(generation_amount);
	var generation_cost := VALUE_SETTING.instantiate() as ValueSetting;
	generation_cost._setting_name = "Energy Cost/Amount";
	generation_cost._maximum = 10.0;
	generation_cost._minimum = 0.5;
	generation_cost._step = 0.5;
	generation_cost._default_value = 2.0;
	generation_cost.set_meta("costMultiplier", -50.0);
	generator_config.add_setting(generation_cost);
	generator_config._cost_function = func(setting: ValueSetting):
		if setting._setting_name == "Generation Cost":
			var shield_regen = generator_config.get_setting("Shield Regen");
			var max_reduction = shield_regen.get_value() * shield_regen.get_meta("costMultiplier", 1.0)
			return max(setting.get_value() * setting.get_meta("costMultiplier", 1.0), -max_reduction);
		return setting.get_value() * setting.get_meta("costMultiplier", 1.0);
	return generator_config;


static func get_mass_generator_config():
	var generator_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	generator_config.set_component_name("Mass Generator");
	generator_config.set_meta("baseCost", 100.0);
	var generation_amount := VALUE_SETTING.instantiate() as ValueSetting;
	generation_amount._setting_name = "Amount/Sec";
	generation_amount._maximum = 10.0;
	generation_amount._minimum = 0.0;
	generation_amount._step = 0.2;
	generation_amount._default_value = 0.4;
	generation_amount.set_meta("costMultiplier", 500.0);
	generator_config.add_setting(generation_amount);
	var generation_cost := VALUE_SETTING.instantiate() as ValueSetting;
	generation_cost._setting_name = "Energy Cost/Amount";
	generation_cost._maximum = 25.0;
	generation_cost._minimum = 10.5;
	generation_cost._step = 0.5;
	generation_cost._default_value = 15.0;
	generation_cost.set_meta("costMultiplier", -10.0);
	generator_config.add_setting(generation_cost);
	return generator_config;


func apply_config_to(chassis: BaseChassisComponent, component_config: Dictionary, resource_name := "Energy", resource_type = BaseGeneratorComponent.ResourceType.COMMODITY):
	var generator = apply_component_to(chassis, component_config["Amount/Sec"], resource_name, resource_type);
	if component_config.has("Maximum Shield"):
		apply_stat_override(chassis.get_node("Shield"), "MaximumHealth", component_config["Maximum Shield"]);
		apply_stat_override(chassis.get_node("Shield"), "CurrentHealth", component_config["Maximum Shield"]);
	if component_config.has("Energy Cost/Amount"):
		apply_stat_override(generator, "Energy Cost", component_config["Energy Cost/Amount"]);
	return generator;
