extends ComponentDescription

class_name ShieldDisruptorComponentDescription;


const FIRING_SOUND = preload("res://Imports/Audio/12-Gauge-Pump-Action-Shotgun-Close-Gunshot-A-www.fesliyanstudios.com.mp3")


func apply_component_to(chassis: BaseChassisComponent, component_config: Dictionary = {}):
	var weapon = get_component_scene("BaseWeapon") as BaseWeaponComponent;
	weapon.name = "Weapon";
	weapon.transform.origin = Vector3(0, -0.25, -0.5);
	weapon.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(weapon, chassis);
	
	apply_stat_override(weapon, "Damage", 0);
	
	apply_stat_override(weapon, "SurfaceArea", 0.2);
	
	var recoil = get_component_scene("RecoilProperty");
	add_component_to(recoil, weapon);
	
	var firing_sound = get_component_scene("FiringSoundProperty");
	firing_sound.get_node("Audio").stream = FIRING_SOUND;
	add_component_to(firing_sound, weapon);
	
	var bullet_hole = get_component_scene("BulletHoleProperty");
	add_component_to(bullet_hole, weapon);
	
	var charged_shot = get_component_scene("ChargedShotProperty");
	add_component_to(charged_shot, weapon);
	
	apply_stat_override(charged_shot, "Base Damage Multiplier", 0.0);
	if component_config.has("Charge Time"):
		apply_stat_override(charged_shot, "Charge Regen", 1.0 / component_config["Charge Time"]);
	
	var charge_glow = get_component_scene("ChargeGlowProperty");
	charge_glow.transform.origin.z = -0.5;
	add_component_to(charge_glow, charged_shot);
	charge_glow._color = _faction._energy_color;
	
	var charge_bar = get_component_scene("ChargeBarProperty");
	add_component_to(charge_bar, charged_shot);
	
	apply_stat_override(charge_glow, "Light Intensity", 5.0);
	
	var bullet_property = get_component_scene("BulletProperty");
	add_component_to(bullet_property, weapon);
	
	var bullet := get_component_scene("Bullet") as Bullet;
	var energy_glow := get_component_scene("EnergyGlowEffect") as EnergyGlow;
	add_component_to(energy_glow, bullet);
	var ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, bullet_property)
	
	ready_callback._callback = func(this):
		this.get_parent().fired.connect(
			func(bullet_property, bullet: Bullet):
				var charge_property = bullet_property.get_node("../ChargedShotComponentProperty");
				var charge_percent = charge_property._charge.get_current_percent();
				bullet.set_meta("damageMultiplier", charge_property.get_damage_multiplier());
				bullet.set_meta("chargePercent", charge_percent);
				var bullet_glow = bullet.get_node("EnergyGlow")
				bullet_glow._color = _faction._energy_color;
				var light_intensity = bullet_property.get_node("../ChargedShotComponentProperty/ChargeGlowComponentProperty/Stats/Light Intensity");
				bullet_glow._light_intensity = light_intensity.get_current_score();
				bullet.set_scale(bullet_property.get_parent().get_scale());
				
				if not bullet.is_node_ready():
					await bullet.ready;
					
				bullet_glow.set_color(charge_percent);
		);
	
	var projectile_scene = PackedScene.new();
	projectile_scene.pack(bullet);
	bullet_property._projectile_scene = projectile_scene;
	bullet_property.transform.origin.z = -0.5;
	bullet_property._destroy_projectile = false;
	
	apply_stat_override(bullet_property, "GravityMult", 0.0);
	apply_stat_override(bullet_property, "SpeedBase", 75.0);
	apply_stat_override(bullet_property, "SpeedMult", 80.0);
	apply_stat_override(bullet_property, "LifetimeBase", 5.0);
	apply_stat_override(bullet_property, "LifetimeMult", 10.0);
	
	var area_effect = get_component_scene("AreaEffect");
	
	if component_config.has("Burst Radius"):
		apply_stat_override(area_effect.get_node("TargetArea"), "Range", component_config["Burst Radius"]);
	else:
		apply_stat_override(area_effect.get_node("TargetArea"), "Range", 3.0);
	
	var area_damage = get_component_scene("AreaDamageProperty") as AreaDamageComponentProperty;
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, area_damage)
	
	ready_callback._callback = func(this):
		await this.get_parent().ready;
		var damage = this.get_parent()._damage_stats.get_stat("Damage");
		var damage_modifier = StatModifier.new()
		var duration = this.get_parent().get_parent().get_meta("duration", 1.0);
		damage_modifier.set_meta("multiplier", 1.0)
		damage_modifier._function = func(stat_value):
			return stat_value * damage_modifier.get_meta("multiplier", 1.0);
		damage.add_child(damage_modifier);
		var range = this.get_parent().get_parent().get_node("TargetArea/Stats/Range")
		var range_modifier = StatModifier.new()
		range_modifier.set_meta("multiplier", 0.0)
		range_modifier._function = func(stat_value):
			return stat_value * range_modifier.get_meta("multiplier", 0.0);
		range.add_child(range_modifier);
		var tween = _faction.get_tree().create_tween();
		tween.set_parallel(true);
		tween.tween_method(
			func(number):
				damage_modifier.set_meta("multiplier", number);
				range_modifier.set_meta("multiplier", 1.0 - pow(number, 4));
				damage.recalculate();
				range.recalculate();
		, 1.0, 0.0, duration);
		damage.tree_exiting.connect(tween.kill);
	
	add_component_to(area_damage, area_effect);
	
	if component_config.has("Damage"):
		apply_stat_override(area_damage, "Damage", component_config["Damage"]);
	else:
		apply_stat_override(area_damage, "Damage", 30.0);
	
	var shield_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	shield_pen._defense_name = "Shield";
	apply_stat_override(shield_pen, "DamageMultiplier", 2);
	apply_stat_override(shield_pen, "PenMultiplier", 4);
	apply_stat_override(shield_pen, "FlatPen", 15);
	add_component_to(shield_pen, area_damage);
	
	var armor_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	armor_pen._defense_name = "Armor";
	apply_stat_override(armor_pen, "DamageMultiplier", 0.2);
	apply_stat_override(armor_pen, "PenMultiplier", 0.5);
	add_component_to(armor_pen, area_damage);
	
	var integrity_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	integrity_pen._defense_name = "Integrity";
	apply_stat_override(integrity_pen, "DamageMultiplier", 0.5);
	add_component_to(integrity_pen, area_damage);
	
	var explosion_effect := get_component_scene("ExplosionEffect") as ExplosionEffect;
	explosion_effect._explosion_color = _faction._energy_color * 5;
	explosion_effect._explosion_color.a = 1.0
	explosion_effect._smoke_color = Color(1.0, 1.0, 1.0, 1.0);
	explosion_effect.get_node("SparkParticles").hide();
	add_component_to(explosion_effect, area_effect);
	
	var detonation_scene = PackedScene.new();
	detonation_scene.pack(area_effect);
	
	var projectile_detonation_property = get_component_scene("ProjectileDetonationProperty");
	projectile_detonation_property._detonation_scene = detonation_scene;
	add_component_to(projectile_detonation_property, bullet_property);
	
	add_box(weapon, true);
	
	return weapon;


static func get_component_config():
	var shield_disruptor_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	shield_disruptor_config.set_component_name("Shield Disruptor");
	var damage = VALUE_SETTING.instantiate();
	damage._setting_name = "Damage";
	damage._maximum = 1000.0;
	damage._minimum = 5.0;
	damage._step = 1.0;
	damage._default_value = 20.0;
	damage.set_meta("costMultiplier", 25.0);
	shield_disruptor_config.add_setting(damage);
	var area_radius = VALUE_SETTING.instantiate();
	area_radius._setting_name = "Burst Radius";
	area_radius._maximum = 10.0;
	area_radius._minimum = 1.0;
	area_radius._step = 0.5;
	area_radius._default_value = 3.0;
	area_radius.set_meta("costMultiplier", 50.0);
	shield_disruptor_config.add_setting(area_radius);
	var charge_time = VALUE_SETTING.instantiate();
	charge_time._setting_name = "Charge Time";
	charge_time._maximum = 10.0;
	charge_time._minimum = 0.5;
	charge_time._step = 0.25;
	charge_time._default_value = 3.0;
	charge_time.set_meta("costMultiplier", -50.0);
	shield_disruptor_config.add_setting(charge_time);
	return shield_disruptor_config;
