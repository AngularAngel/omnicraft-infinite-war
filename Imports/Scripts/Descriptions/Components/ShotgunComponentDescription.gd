extends ComponentDescription

class_name ShotgunComponentDescription;


const FIRING_SOUND = preload("res://Imports/Audio/12-Gauge-Pump-Action-Shotgun-Close-Gunshot-A-www.fesliyanstudios.com.mp3")


func apply_component_to(chassis: BaseChassisComponent, component_config: Dictionary = {}):
	var weapon = get_component_scene("BaseWeapon") as BaseWeaponComponent;
	weapon.name = "Weapon";
	weapon.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(weapon, chassis);
	
	if component_config.has("Damage"):
		apply_stat_override(weapon, "Damage", component_config["Damage"]);
	else:
		apply_stat_override(weapon, "Damage", 30);
	
	if component_config.has("Range"):
		apply_stat_override(weapon, "Range", component_config["Range"]);
	else:
		apply_stat_override(weapon, "Range", 60);
	
	apply_stat_override(weapon, "SurfaceArea", 0.2);
	
	var shield_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	shield_pen._defense_name = "Shield";
	apply_stat_override(shield_pen, "DamageMultiplier", 1.0);
	apply_stat_override(shield_pen, "PenMultiplier", 1.5);
	apply_stat_override(shield_pen, "FlatPen", 5);
	add_component_to(shield_pen, weapon);
	
	var armor_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	armor_pen._defense_name = "Armor";
	apply_stat_override(armor_pen, "DamageMultiplier", 1.2);
	apply_stat_override(armor_pen, "PenMultiplier", 0.5);
	add_component_to(armor_pen, weapon);
	
	var integrity_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	integrity_pen._defense_name = "Integrity";
	apply_stat_override(integrity_pen, "DamageMultiplier", 1.5);
	add_component_to(integrity_pen, weapon);
	
	var extra_shots = get_component_scene("ExtraShotsProperty");
	add_component_to(extra_shots, weapon);
	
	if component_config.has("Shots"):
		apply_stat_override(extra_shots, "ExtraShots", component_config["Shots"] - 1.0);
	else:
		apply_stat_override(extra_shots, "ExtraShots", 4);
	
	var recoil = get_component_scene("RecoilProperty");
	add_component_to(recoil, weapon);
	
	if component_config.has("Inaccuracy"):
		apply_stat_override(recoil, "Inaccuracy", component_config["Inaccuracy"]);
		
	if component_config.has("Recoil"):
		apply_stat_override(recoil, "Max Recoil", component_config["Recoil"]);
	
	var firing_sound = get_component_scene("FiringSoundProperty");
	firing_sound.get_node("Audio").stream = FIRING_SOUND;
	add_component_to(firing_sound, weapon);
	
	var bullet_hole = get_component_scene("BulletHoleProperty");
	add_component_to(bullet_hole, weapon);
	
	var reloading = get_component_scene("ReloadingProperty");
	add_component_to(reloading, weapon);
	
	if component_config.has("Reload Time"):
		apply_stat_override(reloading, "Progress Regen", 1.0 / component_config["Reload Time"]);
	else:
		apply_stat_override(reloading, "Progress Regen", 4);
	
	var ammo = get_component_scene("AmmoProperty");
	add_component_to(ammo, weapon);
	
	if component_config.has("Magazine Size"):
		apply_stat_override(ammo, "Maximum Ammo", component_config["Magazine Size"]);
	
	var fabricator = get_component_scene("Fabricator") as BaseFabricatorComponent;
	fabricator.name = "Ammo Fabricator";
	add_component_to(fabricator, weapon);
	
	if component_config.has("Fabrication Time"):
		apply_stat_override(fabricator, "Progress Regen", 1.0 / component_config["Fabrication Time"]);
	else:
		apply_stat_override(fabricator, "Progress Regen", 2.5);
	
	apply_stat_override(fabricator, "Energy Cost", 0.5);
	apply_stat_override(fabricator, "Mass Cost", 0.25);
	
	add_box(weapon, true);
	
	return weapon;


static func get_component_config():
	var shotgun_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	shotgun_config.set_component_name("Shotgun");
	shotgun_config.set_meta("baseCost", 3.0);
	var damage = VALUE_SETTING.instantiate();
	damage._setting_name = "Damage";
	damage._maximum = 1000.0;
	damage._minimum = 5.0;
	damage._step = 1.0;
	damage._default_value = 30.0;
	damage.set_meta("costMultiplier", 10.0);
	shotgun_config.add_setting(damage);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Range";
	range._maximum = 250.0;
	range._minimum = 5.0;
	range._step = 1.0;
	range._default_value = 40.0;
	range.set_meta("costMultiplier", 5.0);
	shotgun_config.add_setting(range);
	var shots = VALUE_SETTING.instantiate();
	shots._setting_name = "Shots";
	shots._maximum = 10.0;
	shots._minimum = 1.0;
	shots._step = 1.0;
	shots._default_value = 5.0;
	shots.set_meta("costMultiplier", 20.0);
	shotgun_config.add_setting(shots);
	var inaccuracy = VALUE_SETTING.instantiate();
	inaccuracy._setting_name = "Inaccuracy";
	inaccuracy._maximum = 10.0;
	inaccuracy._minimum = 0.0;
	inaccuracy._step = 0.25;
	inaccuracy._default_value = 2.0;
	inaccuracy.set_meta("costMultiplier", -20.0);
	shotgun_config.add_setting(inaccuracy);
	var recoil = VALUE_SETTING.instantiate();
	recoil._setting_name = "Recoil";
	recoil._maximum = 10.0;
	recoil._minimum = 0.0;
	recoil._step = 0.25;
	recoil._default_value = 3.5;
	recoil.set_meta("costMultiplier", -20.0);
	shotgun_config.add_setting(recoil);
	var reload_time = VALUE_SETTING.instantiate();
	reload_time._setting_name = "Reload Time";
	reload_time._maximum = 20.0;
	reload_time._minimum = 0.05;
	reload_time._step = 0.05;
	reload_time._default_value = 0.3;
	reload_time.set_meta("costMultiplier", -100.0);
	shotgun_config.add_setting(reload_time);
	var magazine_size = VALUE_SETTING.instantiate();
	magazine_size._setting_name = "Magazine Size";
	magazine_size._maximum = 50.0;
	magazine_size._minimum = 5.0;
	magazine_size._step = 1.0;
	magazine_size._default_value = 20.0;
	magazine_size.set_meta("costMultiplier", 2.0);
	shotgun_config.add_setting(magazine_size);
	var fabrication_time = VALUE_SETTING.instantiate();
	fabrication_time._setting_name = "Fabrication Time";
	fabrication_time._maximum = 10.0;
	fabrication_time._minimum = 0.05;
	fabrication_time._step = 0.05;
	fabrication_time._default_value = 0.15;
	fabrication_time.set_meta("costMultiplier", -20.0);
	shotgun_config.add_setting(fabrication_time);
	return shotgun_config;
