extends ComponentDescription

class_name StorageComponentDescription;


func apply_component_to(chassis: BaseChassisComponent, amount := 10.0, resource_name := "Energy") -> BaseStorageComponent:
	var storage := get_component_scene("Storage") as BaseStorageComponent;
	storage._resource_name = resource_name;
	storage._amount = amount;
	add_component_to(storage, chassis);
	return storage;


static func get_battery_config():
	var battery_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	battery_config.set_component_name("Battery");
	battery_config._removal_disabled = true;
	var storage_amount = VALUE_SETTING.instantiate();
	storage_amount._setting_name = "Storage Amount";
	storage_amount._maximum = 5000.0;
	storage_amount._minimum = 50.0;
	storage_amount._step = 5.0;
	storage_amount._default_value = 500.0;
	battery_config.add_setting(storage_amount);
	return battery_config;


static func get_mass_storage_config():
	var storage_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	storage_config.set_component_name("Mass Storage");
	var storage_amount = VALUE_SETTING.instantiate();
	storage_amount._setting_name = "Storage Amount";
	storage_amount._maximum = 500.0;
	storage_amount._minimum = 5.0;
	storage_amount._step = 1.0;
	storage_amount._default_value = 100.0;
	storage_amount.set_meta("costMultiplier", 5.0);
	storage_config.add_setting(storage_amount);
	return storage_config;


func apply_config_to(chassis: BaseChassisComponent, component_config, resource_name := "Energy"):
	var storage = apply_component_to(chassis, component_config["Storage Amount"], resource_name);
	apply_stat_override(storage, "Current Storage", component_config["Storage Amount"]);
	return storage;
