extends ComponentDescription

class_name GrenadeLauncherComponentDescription;


const FIRING_SOUND = preload("res://Imports/Audio/12-Gauge-Pump-Action-Shotgun-Close-Gunshot-A-www.fesliyanstudios.com.mp3")


func apply_component_to(chassis: BaseChassisComponent, component_config: Dictionary = {}):
	var weapon = get_component_scene("BaseWeapon") as BaseWeaponComponent;
	weapon.name = "Weapon";
	weapon.transform.origin = Vector3(0, -0.25, -0.5);
	weapon.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(weapon, chassis);
	
	apply_stat_override(weapon, "Damage", 0);
	
	apply_stat_override(weapon, "SurfaceArea", 0.2);
	
	var recoil = get_component_scene("RecoilProperty");
	add_component_to(recoil, weapon);
	
	if component_config.has("Inaccuracy"):
		apply_stat_override(recoil, "Inaccuracy", component_config["Inaccuracy"]);
	else:
		apply_stat_override(recoil, "Inaccuracy", 1);
		
	if component_config.has("Recoil"):
		apply_stat_override(recoil, "Max Recoil", component_config["Recoil"]);
	
	var reloading = get_component_scene("ReloadingProperty");
	apply_stat_override(reloading, "Progress Regen", 0.85);
	add_component_to(reloading, weapon);
	
	var ammo = get_component_scene("AmmoProperty");
	add_component_to(ammo, weapon);
	
	var firing_sound = get_component_scene("FiringSoundProperty");
	firing_sound.get_node("Audio").stream = FIRING_SOUND;
	add_component_to(firing_sound, weapon);
	
	var bullet_property = get_component_scene("BulletProperty");
	add_component_to(bullet_property, weapon);
	
	var bullet := get_component_scene("Bullet") as Bullet;
	
	var mesh = get_mesh("Capsule")
	mesh.rotation_degrees.x = 90;
	mesh.set_instance_shader_parameter("instance_seed", randi_range(1, 1000000000));
	add_component_to(mesh, bullet);
	
	var ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, bullet_property)
	
	ready_callback._callback = func(this):
		this.get_parent().fired.connect(
			func(bullet_property, bullet: Bullet):
				bullet.set_scale(bullet_property.get_parent().get_scale());
				
				if not bullet.is_node_ready():
					await bullet.ready;
		);
	
	var projectile_scene = PackedScene.new();
	projectile_scene.pack(bullet);
	bullet_property._projectile_scene = projectile_scene;
	bullet_property.transform.origin.z = -0.5;
	bullet_property._destroy_projectile = false;
	
	apply_stat_override(bullet_property, "LifetimeBase", 10.0);
	
	apply_stat_override(bullet_property, "SpeedBase", 110.0);
	
	var area_effect = get_component_scene("AreaEffect");
	
	apply_stat_override(area_effect.get_node("TargetArea"), "Range", 3.0);
	
	var area_damage = get_component_scene("AreaDamageProperty") as AreaDamageComponentProperty;
	
	ready_callback = get_component_scene("ReadyCallbackProperty") as ReadyCallbackComponentProperty;
	add_component_to(ready_callback, area_damage)
	
	ready_callback._callback = func(this):
		await this.get_parent().ready;
		var damage = this.get_parent()._damage_stats.get_stat("Damage");
		var damage_modifier = StatModifier.new()
		var duration = this.get_parent().get_parent().get_meta("duration", 1.0);
		damage_modifier.set_meta("multiplier", 1.0)
		damage_modifier._function = func(stat_value):
			return stat_value * damage_modifier.get_meta("multiplier", 1.0);
		damage.add_child(damage_modifier);
		var range = this.get_parent().get_parent().get_node("TargetArea/Stats/Range")
		var range_modifier = StatModifier.new()
		range_modifier.set_meta("multiplier", 0.0)
		range_modifier._function = func(stat_value):
			return stat_value * range_modifier.get_meta("multiplier", 0.0);
		range.add_child(range_modifier);
		var tween = _faction.get_tree().create_tween();
		tween.set_parallel(true);
		tween.tween_method(
			func(number):
				damage_modifier.set_meta("multiplier", number);
				range_modifier.set_meta("multiplier", 1.0 - pow(number, 4));
				damage.recalculate();
				range.recalculate();
		, 1.0, 0.0, duration);
		damage.tree_exiting.connect(tween.kill);
	
	add_component_to(area_damage, area_effect);
	
	
	if component_config.has("Damage"):
		apply_stat_override(area_damage, "Damage", component_config["Damage"]);
	else:
		apply_stat_override(area_damage, "Damage", 90);
	
	var shield_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	shield_pen._defense_name = "Shield";
	apply_stat_override(shield_pen, "PenMultiplier", 1.5);
	apply_stat_override(shield_pen, "FlatPen", 15);
	add_component_to(shield_pen, area_damage);
	
	var armor_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	armor_pen._defense_name = "Armor";
	apply_stat_override(armor_pen, "DamageMultiplier", 1.2);
	add_component_to(armor_pen, area_damage);
	
	var integrity_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	integrity_pen._defense_name = "Integrity";
	apply_stat_override(integrity_pen, "DamageMultiplier", 1.5);
	add_component_to(integrity_pen, area_damage);
	
	var explosion_effect := get_component_scene("ExplosionEffect") as ExplosionEffect;
	#explosion_effect._explosion_color = _energy_color * 5;
	#explosion_effect._explosion_color.a = 1.0
	#explosion_effect._smoke_color = Color(1.0, 1.0, 1.0, 1.0);
	add_component_to(explosion_effect, area_effect);
	
	var detonation_scene = PackedScene.new();
	detonation_scene.pack(area_effect);
	
	var projectile_detonation_property = get_component_scene("ProjectileDetonationProperty");
	projectile_detonation_property._detonation_scene = detonation_scene;
	add_component_to(projectile_detonation_property, bullet_property);
	
	var fabricator = get_component_scene("Fabricator") as BaseFabricatorComponent;
	fabricator.name = "Ammo Fabricator";
	add_component_to(fabricator, weapon);
	
	add_box(weapon, true);
	
	return weapon;


static func get_component_config():
	var grenade_launcher_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	grenade_launcher_config.set_component_name("Grenade Launcher");
	grenade_launcher_config.set_meta("baseCost", 0.0);
	var damage = VALUE_SETTING.instantiate();
	damage._setting_name = "Damage";
	damage._maximum = 1000.0;
	damage._minimum = 5.0;
	damage._step = 1.0;
	damage._default_value = 90.0;
	damage.set_meta("costMultiplier", 10.0);
	grenade_launcher_config.add_setting(damage);
	var inaccuracy = VALUE_SETTING.instantiate();
	inaccuracy._setting_name = "Inaccuracy";
	inaccuracy._maximum = 10.0;
	inaccuracy._minimum = 0.0;
	inaccuracy._step = 0.25;
	inaccuracy._default_value = 2.0;
	inaccuracy.set_meta("costMultiplier", -50.0);
	grenade_launcher_config.add_setting(inaccuracy);
	var recoil = VALUE_SETTING.instantiate();
	recoil._setting_name = "Recoil";
	recoil._maximum = 10.0;
	recoil._minimum = 0.0;
	recoil._step = 0.25;
	recoil._default_value = 4.0;
	recoil.set_meta("costMultiplier", -25.0);
	grenade_launcher_config.add_setting(recoil);
	var reload_time = VALUE_SETTING.instantiate();
	reload_time._setting_name = "Reload Time";
	reload_time._maximum = 20.0;
	reload_time._minimum = 0.05;
	reload_time._step = 0.05;
	reload_time._default_value = 1.25;
	reload_time.set_meta("costMultiplier", -100.0);
	grenade_launcher_config.add_setting(reload_time);
	var magazine_size = VALUE_SETTING.instantiate();
	magazine_size._setting_name = "Magazine Size";
	magazine_size._maximum = 50.0;
	magazine_size._minimum = 5.0;
	magazine_size._step = 1.0;
	magazine_size._default_value = 5.0;
	magazine_size.set_meta("costMultiplier", 5.0);
	grenade_launcher_config.add_setting(magazine_size);
	var fabrication_time = VALUE_SETTING.instantiate();
	fabrication_time._setting_name = "Fabrication Time";
	fabrication_time._maximum = 60.0;
	fabrication_time._minimum = 0.2;
	fabrication_time._step = 0.2;
	fabrication_time._default_value = 4.0;
	fabrication_time.set_meta("costMultiplier", -25.0);
	grenade_launcher_config.add_setting(fabrication_time);
	return grenade_launcher_config;
