extends ComponentDescription

class_name LaserComponentDescription;


func apply_component_to(chassis: BaseChassisComponent, component_config: Dictionary = {}):
	var weapon := get_component_scene("BaseWeapon") as BaseWeaponComponent;
	weapon.name = "Weapon";
	weapon.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(weapon, chassis);
	
	add_box(weapon, true);
	
	if component_config.has("Damage"):
		apply_stat_override(weapon, "Damage", component_config["Damage"]);
	else:
		apply_stat_override(weapon, "Damage", 30);
	
	if component_config.has("Range"):
		apply_stat_override(weapon, "Range", component_config["Range"]);
	else:
		apply_stat_override(weapon, "Range", 40);
	
	apply_stat_override(weapon, "SurfaceArea", 0.2);
	
	var shield_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	shield_pen._defense_name = "Shield";
	apply_stat_override(shield_pen, "DamageMultiplier", 0.5);
	apply_stat_override(shield_pen, "FlatPen", 25);
	apply_stat_override(shield_pen, "PenMultiplier", 4);
	add_component_to(shield_pen, weapon);
	
	var armor_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	armor_pen._defense_name = "Armor";
	apply_stat_override(armor_pen, "DamageMultiplier", 2);
	apply_stat_override(armor_pen, "FlatPen", 30);
	apply_stat_override(armor_pen, "PenMultiplier", 1.5);
	add_component_to(armor_pen, weapon);
	
	var integrity_pen = get_component_scene("DefensePenetrationProperty") as DefensePenetrationComponentProperty;
	integrity_pen._defense_name = "Integrity";
	apply_stat_override(integrity_pen, "DamageMultiplier", 2.25);
	add_component_to(integrity_pen, weapon);
	
	var energy_cost = get_component_scene("EnergyCostProperty");
	add_component_to(energy_cost, weapon);
	
	if component_config.has("Range"):
		apply_stat_override(energy_cost, "Energy Cost", component_config["Energy/Sec"]);
	
	var energy_beam = get_component_scene("EnergyBeamProperty");
	add_component_to(energy_beam, weapon);
	
	var damage_delta = get_component_scene("DamageDeltaProperty");
	add_component_to(damage_delta, weapon);
	
	return weapon;


static func get_component_config():
	var laser_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	laser_config.set_component_name("Laser");
	var damage = VALUE_SETTING.instantiate();
	damage._setting_name = "Damage";
	damage._maximum = 1000.0;
	damage._minimum = 5.0;
	damage._step = 1.0;
	damage._default_value = 30.0;
	damage.set_meta("costMultiplier", 15.0);
	laser_config.add_setting(damage);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Range";
	range._maximum = 250.0;
	range._minimum = 5.0;
	range._step = 1.0;
	range._default_value = 40.0;
	range.set_meta("costMultiplier", 5.0);
	laser_config.add_setting(range);
	var energy_cost = VALUE_SETTING.instantiate();
	energy_cost._setting_name = "Energy/Sec";
	energy_cost._maximum = 20.0;
	energy_cost._minimum = 1.0;
	energy_cost._step = 0.05;
	energy_cost._default_value = 3.0;
	energy_cost.set_meta("costMultiplier", -50.0);
	laser_config.add_setting(energy_cost);
	return laser_config;
