extends ComponentDescription

class_name MotivatorComponentDescription;


func apply_component_to(chassis: BaseChassisComponent):
	var motivator := get_component_scene("Motivator") as BaseMotivatorComponent;
	motivator.name = "Motivator";
	motivator.transform.origin.y = -0.5;
	motivator.transform.origin.z = 0.25;
	add_component_to(motivator, chassis);
	
	apply_stat_override(motivator, "SurfaceArea", 0.75);
	
	add_box(motivator, true);
	return motivator;


static func get_component_config():
	var motivator_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	motivator_config.set_component_name("Motivator");
	var speed = VALUE_SETTING.instantiate();
	speed._setting_name = "Speed";
	speed._maximum = 60.0;
	speed._minimum = 0.5;
	speed._step = 0.1;
	speed._default_value = 6.0;
	speed.set_meta("costMultiplier", 50.0);
	motivator_config.add_setting(speed);
	var jump_velocity = VALUE_SETTING.instantiate();
	jump_velocity._setting_name = "Jump Velocity";
	jump_velocity._maximum = 20.0;
	jump_velocity._minimum = 0.5;
	jump_velocity._step = 0.1;
	jump_velocity._default_value = 5.0;
	jump_velocity.set_meta("costMultiplier", 40.0);
	motivator_config.add_setting(jump_velocity);
	return motivator_config;


func apply_config_to(chassis: BaseChassisComponent, component_config):
	var motivator = apply_component_to(chassis);
	apply_stat_override(motivator, "Speed", component_config["Speed"]);
	apply_stat_override(motivator, "Jump Velocity", component_config["Jump Velocity"]);
	return motivator;
