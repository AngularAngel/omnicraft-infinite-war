extends ComponentDescription

class_name ConduitComponentDescription;


func apply_component_to(chassis: BaseChassisComponent, range := 7, resource_name := "Energy", color := Color(1.0, 1.0, 0.0, 0.3)) -> BaseConduitComponent:
	var conduit := get_component_scene("Conduit") as BaseConduitComponent;
	conduit.name = "Conduit";
	conduit._resource_name = resource_name;
	conduit.set_scale(Vector3(0.6, 0.6, 0.6));
	conduit.add_to_group(resource_name + "Conduits", true);
	conduit.get_node("TargetArea/Area/RangeIndicator").set_color(color);
	add_component_to(conduit, chassis);
	
	apply_stat_override(conduit, "SurfaceArea", 0.35);
	
	apply_stat_override(conduit.get_node("TargetArea"), "Range", range);
	add_box(conduit);
	
	var mesh = conduit.get_node("Mesh")
	mesh.set_instance_shader_parameter("texel_scale", Vector2(0.75, 0.75));
	return conduit;


static func get_energy_conduit_config():
	var conduit_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	conduit_config.set_component_name("Energy Conduit");
	conduit_config.set_meta("baseCost", 55.0);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Range";
	range._maximum = 100.0;
	range._minimum = 0.0;
	range._step = 1.0;
	range._default_value = 5.0;
	range.set_meta("costMultiplier", 20.0);
	conduit_config.add_setting(range);
	var transfer_rate = VALUE_SETTING.instantiate();
	transfer_rate._setting_name = "Amount/Sec";
	transfer_rate._maximum = 100.0;
	transfer_rate._minimum = 0.0;
	transfer_rate._step = 1.0;
	transfer_rate._default_value = 15.0;
	transfer_rate.set_meta("costMultiplier", 10.0);
	conduit_config.add_setting(transfer_rate);
	var transfer_speed = VALUE_SETTING.instantiate();
	transfer_speed._setting_name = "Transfers/Sec";
	transfer_speed._maximum = 10.0;
	transfer_speed._minimum = 0.0;
	transfer_speed._step = 1.0;
	transfer_speed._default_value = 3.0;
	transfer_speed.set_meta("costMultiplier", 25.0);
	conduit_config.add_setting(transfer_speed);
	var priority = VALUE_SETTING.instantiate();
	priority._setting_name = "Priority";
	priority._maximum = 10.0;
	priority._minimum = -10.0;
	priority._step = 1.0;
	priority._default_value = 2.0;
	priority.set_meta("costMultiplier", 0.0);
	conduit_config.add_setting(priority);
	var max_reception = VALUE_SETTING.instantiate();
	max_reception._setting_name = "Maximum Reception";
	max_reception._maximum = 200.0;
	max_reception._minimum = 0.0;
	max_reception._step = 1.0;
	max_reception._default_value = 20.0;
	max_reception.set_meta("costMultiplier", 1.0);
	conduit_config.add_setting(max_reception);
	var reception_rate = VALUE_SETTING.instantiate();
	reception_rate._setting_name = "Reception Rate";
	reception_rate._maximum = 100.0;
	reception_rate._minimum = 0.0;
	reception_rate._step = 1.0;
	reception_rate._default_value = 20.0;
	reception_rate.set_meta("costMultiplier", 5.0);
	conduit_config.add_setting(reception_rate);
	return conduit_config;


static func get_mass_conduit_config():
	var conduit_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	conduit_config.set_component_name("Mass Conduit");
	conduit_config.set_meta("baseCost", 55.0);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Range";
	range._maximum = 100.0;
	range._minimum = 0.0;
	range._step = 1.0;
	range._default_value = 5.0;
	range.set_meta("costMultiplier", 20.0);
	conduit_config.add_setting(range);
	var transfer_rate = VALUE_SETTING.instantiate();
	transfer_rate._setting_name = "Amount/Sec";
	transfer_rate._maximum = 20.0;
	transfer_rate._minimum = 0.0;
	transfer_rate._step = 1.0;
	transfer_rate._default_value = 5.0;
	transfer_rate.set_meta("costMultiplier", 30.0);
	conduit_config.add_setting(transfer_rate);
	var transfer_speed = VALUE_SETTING.instantiate();
	transfer_speed._setting_name = "Transfers/Sec";
	transfer_speed._maximum = 5.0;
	transfer_speed._minimum = 0.0;
	transfer_speed._step = 1.0;
	transfer_speed._default_value = 1.0;
	transfer_speed.set_meta("costMultiplier", 75.0);
	conduit_config.add_setting(transfer_speed);
	var priority = VALUE_SETTING.instantiate();
	priority._setting_name = "Priority";
	priority._maximum = 10.0;
	priority._minimum = -10.0;
	priority._step = 1.0;
	priority._default_value = 0.0;
	priority.set_meta("costMultiplier", 0.0);
	conduit_config.add_setting(priority);
	var max_reception = VALUE_SETTING.instantiate();
	max_reception._setting_name = "Maximum Reception";
	max_reception._maximum = 150.0;
	max_reception._minimum = 0.0;
	max_reception._step = 1.0;
	max_reception._default_value = 10.0;
	max_reception.set_meta("costMultiplier", 2.0);
	conduit_config.add_setting(max_reception);
	var reception_rate = VALUE_SETTING.instantiate();
	reception_rate._setting_name = "Reception Rate";
	reception_rate._maximum = 50.0;
	reception_rate._minimum = 0.0;
	reception_rate._step = 1.0;
	reception_rate._default_value = 10.0;
	reception_rate.set_meta("costMultiplier", 10.0);
	conduit_config.add_setting(reception_rate);
	return conduit_config;


func apply_config_to(chassis: BaseChassisComponent, component_config, resource_name := "Energy", color := Color(1.0, 1.0, 0.0, 0.3)):
	var conduit := apply_component_to(chassis, component_config["Range"], resource_name, color);
	apply_stat_override(conduit, "Transfer Rate", component_config["Amount/Sec"]);
	apply_stat_override(conduit, "Transfer Speed", component_config["Transfers/Sec"]);
	apply_stat_override(conduit, "Priority", component_config["Priority"]);
	apply_stat_override(conduit, "Current Reception", component_config["Maximum Reception"]);
	apply_stat_override(conduit, "Maximum Reception", component_config["Maximum Reception"]);
	apply_stat_override(conduit, "Reception Regen", component_config["Reception Rate"]);
	return conduit;
