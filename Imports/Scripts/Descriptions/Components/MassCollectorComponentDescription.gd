extends ComponentDescription

class_name MassCollectorComponentDescription;


func apply_component_to(chassis: BaseChassisComponent):
	var mass_collector := get_component_scene("MassCollector") as MassCollector;
	mass_collector.set_scale(Vector3(0.3, 0.3, 0.3));
	add_component_to(mass_collector, chassis);
	
	apply_stat_override(mass_collector, "SurfaceArea", 0.2);
	
	add_box(mass_collector, true);
	return mass_collector;


static func get_component_config():
	var mass_collector_config = BUDGETED_COMPONENT_CONFIG.instantiate() as BudgetedComponentConfig;
	mass_collector_config.set_component_name("Mass Collector");
	var collection_speed = VALUE_SETTING.instantiate();
	collection_speed._setting_name = "Collection Speed";
	collection_speed._maximum = 5.0;
	collection_speed._minimum = 0.1;
	collection_speed._step = 0.05;
	collection_speed._default_value = 1.0;
	collection_speed.set_meta("costMultiplier", 150.0);
	mass_collector_config.add_setting(collection_speed);
	var range = VALUE_SETTING.instantiate();
	range._setting_name = "Collection Range";
	range._maximum = 10.0;
	range._minimum = 1.0;
	range._step = 1.0;
	range._default_value = 5.0;
	range.set_meta("costMultiplier", 40.0);
	mass_collector_config.add_setting(range);
	var energy_expenditure = VALUE_SETTING.instantiate();
	energy_expenditure._setting_name = "Energy Expenditure";
	energy_expenditure._maximum = 5.0;
	energy_expenditure._minimum = 0.1;
	energy_expenditure._step = 0.1;
	energy_expenditure._default_value = 1.0;
	energy_expenditure.set_meta("costMultiplier", -50.0);
	mass_collector_config.add_setting(energy_expenditure);
	var mass_efficiency = VALUE_SETTING.instantiate();
	mass_efficiency._setting_name = "Mass Efficiency";
	mass_efficiency._maximum = 1.0;
	mass_efficiency._minimum = 0.0;
	mass_efficiency._step = 0.01;
	mass_efficiency._default_value = 1.0;
	mass_efficiency.set_meta("costMultiplier", 200.0);
	mass_collector_config.add_setting(mass_efficiency);
	return mass_collector_config;


func apply_config_to(chassis: BaseChassisComponent, component_config):
	var mass_collector = apply_component_to(chassis);
	apply_stat_override(mass_collector, "Progress Regen", component_config["Collection Speed"]);
	apply_stat_override(mass_collector, "Range", component_config["Collection Range"]);
	apply_stat_override(mass_collector, "Energy Expenditure", component_config["Energy Expenditure"]);
	apply_stat_override(mass_collector, "Mass Efficiency", component_config["Mass Efficiency"]);
	return mass_collector;
