extends Node

class_name BuildingOption


@export var _voxel_id := 4;
@export var _mass_cost := 0.0;
@export var _energy_cost := 0.0;
@export var _progress_cost := 0.0;
@export var _icon: Image;
@export var _building_scene: PackedScene;
@export var _commodity_display_scene: PackedScene;
@export var _preview_scene: PackedScene;


func get_construction_site() -> ConstructionSite:
	var construction_site := get_parent()._units["Construction Site"].instantiate() as ConstructionSite;
	construction_site._voxel_id = _voxel_id;
	if _building_scene != null:
		var building = _building_scene.instantiate()
		if _commodity_display_scene != null:
			var commodity_display = _commodity_display_scene.instantiate();
			commodity_display.transform.origin.y += float(building._dimensions.y);
			building.get_node("Chassis").add_child(commodity_display);
		construction_site.set_building(building)
	construction_site.get_node("Stats/Resources/Progress/Maximum Progress").set_base_score(_progress_cost)
	construction_site.get_node("Stats/Mass Cost").set_base_score(_mass_cost);
	construction_site.get_node("Stats/Energy Cost").set_base_score(_energy_cost);
	return construction_site;
