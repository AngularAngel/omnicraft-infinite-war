extends Node

class_name UnitPositioner


@export var _target_position := Vector3();
@export var _max_y_increase := 5.0;
@export var _padding := Vector3(5.0, 5.0, 5.0);


var _terrain_tool: VoxelTool;


func _process(_delta: float) -> void:
	var entity = get_parent();
	var map = entity.get_world_3d().navigation_map
	if not NavigationServer3D.map_is_active(map):
		return;
	var target_point = NavigationServer3D.map_get_closest_point(map, _target_position);
	if target_point.y - _target_position.y < _max_y_increase and \
		target_point.distance_squared_to(_target_position) > 0 and \
		_terrain_tool.is_area_editable(AABB(
			get_parent()._aabb.position + target_point - _padding,
			get_parent()._aabb.size + _padding)
		):
		entity.position = target_point;
		queue_free();
