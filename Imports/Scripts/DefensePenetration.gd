extends Object

class_name DefensePenetration


var _damage_multiplier := 1.0;
var _flat_pen := 0.0;
var _pen_multiplier := 1.0;


func get_damage(raw_quantity):
	return raw_quantity * _damage_multiplier;


func get_reduction(reduction):
	return max((reduction - _flat_pen) / (_damage_multiplier * _pen_multiplier), 0);
