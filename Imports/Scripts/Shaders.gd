extends Object

class_name Shaders

static var _dict = {
	"Earth": preload("res://Imports/Shaders/Earth.gdshader"),
	"Crystal": preload("res://Imports/Shaders/Crystal.gdshader"),
	"Building": preload("res://Imports/Shaders/Building.gdshader"),
	"Shield": preload("res://Imports/Shaders/Shield.tres")
}
