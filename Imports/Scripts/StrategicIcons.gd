class_name StrategicIcons


#Utility functions
static func set_pixel_if_visible(image: Image, x: int, y: int, color: Color):
	if color.a > 0.0:
		image.set_pixel(x, y, color);


static func draw_rect(image: Image, width: int, height: int, x_offset: int, y_offset: int, background_color: Color, edge_color := Color.BLACK):
	for i in range(height):
		for j in range(width):
			var color;
			if i == 0 or j == 0 or i == height - 1 or j == width - 1:
				color = edge_color;
			else:
				color = background_color;
			set_pixel_if_visible(image, j + x_offset, i + y_offset, color);


#Mobility type functions
static func structure(background_color: Color, edge_color := Color.BLACK) -> Image:
	var image = Image.create_empty(17, 17, false, Image.FORMAT_RGBAF);
	
	draw_rect(image, 11, 11, 3, 3, background_color, edge_color);
	
	return image;


static func factory(background_color: Color, edge_color := Color.BLACK) -> Image:
	var image = Image.create_empty(17, 17, false, Image.FORMAT_RGBAF);
	
	draw_rect(image, 15, 9, 1, 4, background_color, edge_color);
	
	return image;


#Purpose functions
static func wall(image, x_center: int, y_center: int, background_color := Color(0, 0, 0, 0), edge_color := Color.BLACK) -> Image:
	draw_rect(image, 3, 3, x_center - 1, y_center - 2, background_color, edge_color);
	draw_rect(image, 3, 3, x_center - 2, y_center, background_color, edge_color);
	draw_rect(image, 3, 3, x_center, y_center, background_color, edge_color);
	
	return image;


static func power_generation(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	image.set_pixel(x_center - 1, y_center - 2, edge_color);
	image.set_pixel(x_center, y_center - 1, edge_color);
	image.set_pixel(x_center + 1, y_center, edge_color);
	
	image.set_pixel(x_center, y_center, edge_color);
	
	image.set_pixel(x_center - 1, y_center, edge_color);
	image.set_pixel(x_center, y_center + 1, edge_color);
	image.set_pixel(x_center + 1, y_center + 2, edge_color);
	
	return image;


static func power_storage(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	image.set_pixel(x_center - 2, y_center - 1, edge_color);
	image.set_pixel(x_center - 1, y_center, edge_color);
	image.set_pixel(x_center, y_center + 1, edge_color);
	
	image.set_pixel(x_center, y_center, edge_color);
	
	image.set_pixel(x_center, y_center - 1, edge_color);
	image.set_pixel(x_center + 1, y_center, edge_color);
	image.set_pixel(x_center + 2, y_center + 1, edge_color);
	
	return image;
	
	
static func supply(image, x_center: int, y_center: int, background_color := Color(0, 0, 0, 0), edge_color := Color.BLACK) -> Image:
	draw_rect(image, x_center - 3, y_center - 3, 6, 6, background_color, edge_color);
	
	return image


static func direct_fire(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	image.set_pixel(x_center - 1, y_center, edge_color);
	image.set_pixel(x_center - 2, y_center, edge_color);
	
	image.set_pixel(x_center + 1, y_center, edge_color);
	image.set_pixel(x_center + 2, y_center, edge_color);
	
	image.set_pixel(x_center, y_center - 2, edge_color);
	image.set_pixel(x_center, y_center - 1, edge_color);
	
	image.set_pixel(x_center, y_center + 1, edge_color);
	image.set_pixel(x_center, y_center + 2, edge_color);
	
	return image;


static func engineering(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	draw_rect(image, 7, 1, x_center - 3, y_center - 1, Color(), edge_color);
	
	draw_rect(image, 1, 3, x_center - 3, y_center - 1, Color(), edge_color);
	draw_rect(image, 1, 3, x_center, y_center - 1, Color(), edge_color);
	draw_rect(image, 1, 3, x_center + 3, y_center - 1, Color(), edge_color);
	
	return image;


static func mining(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	draw_rect(image, 3, 1, x_center - 1, y_center - 2, Color(), edge_color);
	
	image.set_pixel(x_center - 2, y_center - 1, edge_color);
	image.set_pixel(x_center - 2, y_center, edge_color);
	image.set_pixel(x_center - 1, y_center, edge_color);
	
	image.set_pixel(x_center + 2, y_center - 1, edge_color);
	image.set_pixel(x_center + 2, y_center, edge_color);
	image.set_pixel(x_center + 1, y_center, edge_color);
	
	image.set_pixel(x_center, y_center + 2, edge_color);
	
	return image;


static func mass_storage(image, x_center: int, y_center: int, edge_color := Color.BLACK) -> Image:
	draw_rect(image, 1, 3, x_center - 3, y_center - 1, Color(), edge_color);
	
	image.set_pixel(x_center - 2, y_center - 2, edge_color);
	image.set_pixel(x_center - 1, y_center - 2, edge_color);
	image.set_pixel(x_center - 1, y_center - 1, edge_color);
	
	image.set_pixel(x_center - 2, y_center + 2, edge_color);
	image.set_pixel(x_center - 1, y_center + 2, edge_color);
	image.set_pixel(x_center - 1, y_center + 1, edge_color);
	
	draw_rect(image, 1, 3, x_center + 3, y_center - 1, Color(), edge_color);
	
	image.set_pixel(x_center + 2, y_center - 2, edge_color);
	image.set_pixel(x_center + 1, y_center - 2, edge_color);
	image.set_pixel(x_center + 1, y_center - 1, edge_color);
	
	image.set_pixel(x_center + 2, y_center + 2, edge_color);
	image.set_pixel(x_center + 1, y_center + 2, edge_color);
	image.set_pixel(x_center + 1, y_center + 1, edge_color);
	
	return image;


#Decorator functions
static func add_pips(image, number, x_center, y_offset, background_color: Color, edge_color := Color.BLACK) -> Image:
	for i in range(number):
		draw_rect(image, 3, 4, x_center - number + i * 2, y_offset, Color.WHITE, edge_color);
	
	return image;
