extends GameMaterial

class_name StoneMaterial


var _layers := [];


func set_shader_material_parameters(shader_material):
	super.set_shader_material_parameters(shader_material);
	shader_material.set_shader_parameter("layers", _layers);
	
