extends Constants

class_name MaterialComponent

var albedo: Vector3;
var light := Color(0, 0, 0, 0);
var specular := 0.0;
var metallicity := 0.0;
var roughness := 0.0;


func get_value(channel: CHANNEL):
	match(channel):
		(CHANNEL.COLOR):
			return albedo;
		(CHANNEL.LIGHT):
			return light;
		(CHANNEL.SPECULAR):
			return specular;
		(CHANNEL.METALLICITY):
			return metallicity;
		(CHANNEL.ROUGHNESS):
			return roughness;
