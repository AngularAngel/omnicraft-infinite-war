extends Constants

class_name GameMaterial

var _name: String;

var _components = [];
var _percentages = [];


func add_component(component, percentage):
	_components.append(component);
	_percentages.append(percentage);


func get_values(channel):
	var values = [];
	for component in _components:
		values.append(component.get_value(channel));
	
	return values;


func set_shader_material_parameters(shader_material):
	shader_material.set_shader_parameter("colors", get_values(CHANNEL.COLOR));
	shader_material.set_shader_parameter("speculars", get_values(CHANNEL.SPECULAR))
	shader_material.set_shader_parameter("roughnesses", get_values(CHANNEL.ROUGHNESS))
