extends Object

class_name Meshes


var _dict = {}


func add_mesh(name: String, mesh: Mesh):
	_dict[name] = mesh;


func _init():
	#Create the Cube Mesh.
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_cuboid(st, Vector3(0, 0, 0), Vector3(1, 1, 1))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()
	
	add_mesh("Cube", st.commit())
	
	#Create the small cube mesh
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_cuboid(st, Vector3(4.0 / 16.0, 0, 5.0 / 16.0), Vector3(13.0 / 16.0, 9.0 / 16.0, 13.0 / 16.0))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()
	
	add_mesh("Small Cube", st.commit())
	
	#Create the Cross Mesh.
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_vertical_quad(st, Vector3(0, 0, 0), Vector3(1, 1, 1))
	create_vertical_quad(st, Vector3(0, 0, 1), Vector3(1, 1, 0))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()

	add_mesh("Cross", st.commit())
	
	#Create the Large Cross Mesh.
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_vertical_quad(st, Vector3(0, 0, 0), Vector3(2, 2, 2))
	create_vertical_quad(st, Vector3(0, 0, 2), Vector3(2, 2, 0))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()
	
	var large_cross = st.commit();
	
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_vertical_quad(st, Vector3(0, 0, 1), Vector3(1, 1, 0))
	create_vertical_quad(st, Vector3(1, 0, 0), Vector3(2, 1, 1))
	create_vertical_quad(st, Vector3(2, 0, 1), Vector3(1, 1, 2))
	create_vertical_quad(st, Vector3(1, 0, 2), Vector3(0, 1, 1))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()

	add_mesh("Large Cross", st.commit(large_cross))
	
	#Create the Grid Mesh.
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_vertical_quad(st, Vector3(0, 0, 5.0 / 16.0), Vector3(1, 1, 5.0 / 16.0))
	create_vertical_quad(st, Vector3(0, 0, 11.0 / 16.0), Vector3(1, 1, 11.0 / 16.0))
	
	create_vertical_quad(st, Vector3(5.0 / 16.0, 0, 0), Vector3(5.0 / 16.0, 1, 1))
	create_vertical_quad(st, Vector3(11.0 / 16.0, 0, 0), Vector3(11.0 / 16.0, 1, 1))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()
	
	var grid_mesh := st.commit()
	
	add_mesh("Grid", grid_mesh)
	
	#Create the berry bush mesh
	var berry_bush := grid_mesh.duplicate(true)
	
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_cuboid(st, Vector3(4.0 / 16.0, 6.0 / 16.0, 5.0 / 16.0), Vector3(5.0 / 16.0, 6.0 / 16.0, 6.0 / 16.0))
	create_cuboid(st, Vector3(11.0 / 16.0, 14.0 / 16.0, 13.0 / 16.0), Vector3(12.0 / 16.0, 15.0 / 16.0, 14.0 / 16.0))
	create_cuboid(st, Vector3(4.0 / 16.0, 11.0 / 16.0, 10.0 / 16.0), Vector3(5.0 / 16.0, 12.0 / 16.0, 11.0 / 16.0))
	create_cuboid(st, Vector3(11.0 / 16.0, 8.0 / 16.0, 5.0 / 16.0), Vector3(12.0 / 16.0, 9.0 / 16.0, 6.0 / 16.0))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()
	
	st.commit(berry_bush)
	
	add_mesh("Berry Bush", berry_bush)
	
	#Create the Large Grid Mesh.
	st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	create_vertical_quad(st, Vector3(0, 0, 0.35), Vector3(2, 1, 0.35))
	create_vertical_quad(st, Vector3(0, 0, 0.65), Vector3(2, 1, 0.65))
	create_vertical_quad(st, Vector3(0, 0, 1.00), Vector3(2, 1, 1.00))
	create_vertical_quad(st, Vector3(0, 0, 1.35), Vector3(2, 1, 1.35))
	create_vertical_quad(st, Vector3(0, 0, 1.65), Vector3(2, 1, 1.65))
	
	create_vertical_quad(st, Vector3(0.35, 0, 0), Vector3(0.35, 1, 2))
	create_vertical_quad(st, Vector3(0.65, 0, 0), Vector3(0.65, 1, 2))
	create_vertical_quad(st, Vector3(1.00, 0, 0), Vector3(1.00, 1, 2))
	create_vertical_quad(st, Vector3(1.35, 0, 0), Vector3(1.35, 1, 2))
	create_vertical_quad(st, Vector3(1.65, 0, 0), Vector3(1.65, 1, 2))
	
	st.generate_normals()
	st.generate_tangents()
	st.index()

	add_mesh("Large Grid", st.commit())


func create_cuboid(st : SurfaceTool, startPos : Vector3, endPos : Vector3):
	
	create_horizontal_quad(st, Vector3(startPos.x, endPos.y, startPos.z), Vector3(endPos.x, endPos.y, endPos.z), startPos.x, endPos.x, startPos.z, endPos.z) #Top
	create_horizontal_quad(st, Vector3(startPos.x, startPos.y, endPos.z), Vector3(endPos.x, startPos.y, startPos.z), startPos.x, endPos.x, startPos.z, endPos.z) #Bottom
	
	create_vertical_quad(st, startPos, Vector3(endPos.x, endPos.y, startPos.z), startPos.x, endPos.x, startPos.y, endPos.y) #Front
	create_vertical_quad(st, Vector3(endPos.x, startPos.y, endPos.z), Vector3(startPos.x, endPos.y, endPos.z), startPos.x, endPos.x, startPos.y, endPos.y) #Back
	
	create_vertical_quad(st, Vector3(startPos.x, startPos.y, endPos.z), Vector3(startPos.x, endPos.y, startPos.z), startPos.z, endPos.z, startPos.y, endPos.y) #Left
	create_vertical_quad(st, Vector3(endPos.x, startPos.y, startPos.z), Vector3(endPos.x, endPos.y, endPos.z), startPos.z, endPos.z, startPos.y, endPos.y) #Right


#Creates a horizontally oriented quad. You may specify the start and end UVs.
func create_horizontal_quad(st : SurfaceTool, startPos : Vector3, endPos : Vector3, startUVX = 0.0, endUVX = 1.0, startUVY = 1.0, endUVY = 0.0):
	st.set_uv(Vector2(startUVX, startUVY))
	st.add_vertex(startPos)
	st.set_uv(Vector2(endUVX, endUVY))
	st.add_vertex(endPos)
	st.set_uv(Vector2(startUVX, endUVY))
	st.add_vertex(Vector3(startPos.x, startPos.y, endPos.z))
	
	st.set_uv(Vector2(startUVX, startUVY))
	st.add_vertex(startPos)
	st.set_uv(Vector2(endUVX, startUVY))
	st.add_vertex(Vector3(endPos.x, endPos.y, startPos.z))
	st.set_uv(Vector2(endUVX, endUVY))
	st.add_vertex(endPos)


#Creates a vertically oriented quad. You may specify the start and end UVs.
func create_vertical_quad(st : SurfaceTool, startPos : Vector3, endPos : Vector3, startUVX = 0.0, endUVX = 1.0, startUVY = 1.0, endUVY = 0.0):
	st.set_uv(Vector2(startUVX, startUVY))
	st.add_vertex(startPos)
	st.set_uv(Vector2(endUVX, endUVY))
	st.add_vertex(endPos)
	st.set_uv(Vector2(startUVX, endUVY))
	st.add_vertex(Vector3(startPos.x, endPos.y, startPos.z))
	
	st.set_uv(Vector2(startUVX, startUVY))
	st.add_vertex(startPos)
	st.set_uv(Vector2(endUVX, startUVY))
	st.add_vertex(Vector3(endPos.x, startPos.y, endPos.z))
	st.set_uv(Vector2(endUVX, endUVY))
	st.add_vertex(endPos)
