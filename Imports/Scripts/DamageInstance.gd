extends Object

class_name DamageInstance;


enum DAMAGE_SOURCE{RAYCAST, AREA}


signal damaged_defense(defense_name, amount);
signal damaged_terrain();


var source = null;
var source_type := DAMAGE_SOURCE.RAYCAST;
var _raw_quantity := 0.0;

var _defense_penetrations := {};


func get_damage_against(defense_name):
	if _defense_penetrations.has(defense_name):
		return _defense_penetrations[defense_name].get_damage(_raw_quantity);
	else:
		return _raw_quantity;


func reduce_damage_from(defense_name, reduction):
	if _defense_penetrations.has(defense_name):
		_raw_quantity -= _defense_penetrations[defense_name].get_reduction(reduction);
	else:
		_raw_quantity -= reduction;
