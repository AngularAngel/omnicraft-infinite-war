extends Node3D

class_name EnergyGlow

@onready var _glow_mesh = $GlowMesh as MeshInstance3D;
@onready var _glow_particles = $GlowParticles as GPUParticles3D;
@onready var _glow_light = $GlowLight as OmniLight3D;


@export var _color := Color(0.95, 0.5, 0.5)
@export var _light_intensity := 1;


func set_color(intensity):
	var color = Color(_color,
					_color.a * intensity);
	_glow_mesh.set_instance_shader_parameter("albedo", color);
	_glow_mesh.set_instance_shader_parameter("emission", _color);
	var emission_energy = color.a * _light_intensity;
	_glow_mesh.set_instance_shader_parameter("emission_energy", emission_energy);
	_glow_particles.set_instance_shader_parameter("albedo", color);
	_glow_particles.set_instance_shader_parameter("emission", _color);
	_glow_particles.set_instance_shader_parameter("emission_energy", emission_energy);
	_glow_light.light_color = color;
	_glow_light.light_energy = emission_energy;
