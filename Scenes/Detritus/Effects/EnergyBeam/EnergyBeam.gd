extends Node3D

class_name EnergyBeam


@onready var _beam_mesh := $BeamMesh as MeshInstance3D
@onready var _beam_particles = $BeamMesh/BeamParticles as GPUParticles3D;
@onready var _end_particles = $BeamMesh/EndParticles as GPUParticles3D;
@onready var _beam_ray = $BeamRay as RayCast3D;

var _color := Color(0.95, 0.5, 0.5);
var _target;
var _tween: Tween = null;


func _ready() -> void:
	_beam_ray.add_exception(get_parent().get_parent());


func _physics_process(_delta: float) -> void:
	set_position(get_parent().get_global_position());
	_beam_ray.set_position(get_global_position());
	if visible:
		#print(_target);
		if _target != null:
			_beam_ray.look_at(_target.get_global_position(), Vector3.FORWARD);
			_beam_ray.force_raycast_update();
			cast_to(_beam_ray.get_collision_point());


func reverse():
	_beam_particles.rotation_degrees.z = -_beam_particles.rotation_degrees.z;


func fade_in_out(duration, terminate := false, lose_target := true):
	await fade_in(duration / 8.0);
	await get_tree().create_timer(duration * 3.0 / 8.0).timeout;
	fade_out(duration * 4.0 / 8.0, terminate, lose_target);


func fade_in(duration):
	if _tween != null:
		_tween.kill();
	if not is_inside_tree():
		return;
	_tween = get_tree().create_tween();
	_tween.set_parallel(true);
	_tween.tween_property(_beam_mesh, "scale:x", 1, duration);
	_tween.tween_property(_beam_mesh, "scale:z", 1, duration);
	_tween.tween_method(set_color, 0, 1, duration);
	visible = true;
	_beam_particles.emitting = true;
	_end_particles.emitting = true;
	return _tween;


func fade_out(duration, terminate := false, lose_target := true):
	if _tween != null:
		_tween.kill();
	if not is_inside_tree():
		return;
	_tween = get_tree().create_tween();
	_tween.set_parallel(true);
	_tween.tween_property(_beam_mesh, "scale:x", 0.001, duration);
	_tween.tween_property(_beam_mesh, "scale:z", 0.001, duration);
	_tween.tween_method(set_color, 1, 0, duration);
	_tween.finished.connect(
		func():
			if terminate:
				queue_free();
			else:
				visible = false;
				_beam_particles.emitting = false;
				_end_particles.emitting = false;
				if lose_target:
					_target = null;
	);
	return _tween;


func set_color(intensity):
	var color = Color(_color,
					_color.a * intensity);
	_beam_mesh.set_instance_shader_parameter("albedo", color);
	_beam_mesh.set_instance_shader_parameter("emission", _color);
	_beam_mesh.set_instance_shader_parameter("emission_energy", color.a);
	_beam_particles.set_instance_shader_parameter("albedo", color);
	_beam_particles.set_instance_shader_parameter("emission", _color);
	_beam_particles.set_instance_shader_parameter("emission_energy", color.a);
	_end_particles.set_instance_shader_parameter("albedo", color);
	_end_particles.set_instance_shader_parameter("emission", _color);
	_end_particles.set_instance_shader_parameter("emission_energy", color.a);


func cast_to(cast_point: Vector3):
	var position_diff = cast_point - get_global_position();
	scale.z = position_diff.length();
	
	var normal = position_diff.normalized();
	if normal != Vector3.UP:
		look_at(cast_point, Vector3.UP);
		#rotation_degrees.y += 90;
