extends Node3D

class_name ExplosionEffect


@export var _explosion_color := Color(5.0, 2.0, 1.0, 1.0);
@export var _smoke_color := Color(0.0, 0.0, 0.0, 1.0);


@onready var _spark_particles: GPUParticles3D = $SparkParticles
@onready var _flash_particle: GPUParticles3D = $FlashParticle
@onready var _flare_particles: GPUParticles3D = $FlareParticles
@onready var _smoke_particles: GPUParticles3D = $SmokeParticles
@onready var _burst_particle: GPUParticles3D = $BurstParticle


func _ready() -> void:
	_spark_particles.set_instance_shader_parameter("spark_color", _explosion_color);
	_flare_particles.set_instance_shader_parameter("flare_color", _explosion_color);
	_burst_particle.set_instance_shader_parameter("burst_color", _explosion_color);
	var flash_color = _explosion_color / 2.0;
	flash_color.a = 1.0;
	_flash_particle.set_instance_shader_parameter("flash_color", flash_color);
	_smoke_particles.set_instance_shader_parameter("smoke_color", _smoke_color);
	for child in get_children():
		if child.has_method("restart"):
			child.restart();


func multiply_lifetimes(multiplier):
	_spark_particles.lifetime *= multiplier;
	_flash_particle.lifetime *= multiplier;
	_flare_particles.lifetime *= multiplier;
	_smoke_particles.lifetime *= multiplier;
	_burst_particle.lifetime *= multiplier;
