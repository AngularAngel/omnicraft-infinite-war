extends Node3D

class_name Game

const BLOCK_SIZE := 16;
const DEFAULT_ENTITY_SAVE_FILE_PATH = "/entities";
const DEFAULT_ENVIRONMENT_SAVE_FILE_PATH = "/environment";


const PlayerControllerScene = preload("res://Scenes/Entities/Controllers/Player/PlayerController.tscn")
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")
const TEXT_SETTING = preload("res://Scenes/GUI/Common/Settings/Text/TextSetting.tscn");


enum ENTITY_TYPE {ENVIRONMENTAL, MOBILE_UNIT, BUILDING}


@onready var _terrain: OmniTerrain = $Terrain;
@onready var _navigation_generator: NavigationGenerator = $NavigationGenerator
@onready var _voxels: Voxels = $Voxels;
@onready var _environment: Node = $Environment


var _save_name: String;
var _save_dir: String;
var _random := RandomNumberGenerator.new();
var _player: BaseMobileUnit;

var _empty_voxel_ids := [];
var _solid_voxel_ids := [];

var _load := false;

var _block_aabb := AABB(Vector3(0, 0, 0), Vector3(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE));

var _entity_dir;


func _ready() -> void:
	if _load:
		self.load();
	_entity_dir = _save_dir + DEFAULT_ENTITY_SAVE_FILE_PATH;
	
	_terrain.block_unloaded.connect(unload_entities);
	_terrain.mesh_block_exited.connect(unload_entities);
	
	_terrain.mesh_block_entered.connect(load_entities);


func unload_entities(block_pos: Vector3i):
			for environment_entity in _environment.get_children():
				var pos = environment_entity.get_position() - Vector3(block_pos * BLOCK_SIZE);
				if _block_aabb.has_point(pos):
					var path = get_block_path(block_pos) + DEFAULT_ENVIRONMENT_SAVE_FILE_PATH;
					unload_entity(environment_entity, path);


func load_entities(block_pos: Vector3i):
	var block_path = get_block_path(block_pos) + DEFAULT_ENVIRONMENT_SAVE_FILE_PATH;
	if DirAccess.dir_exists_absolute(block_path):
		var files = DirAccess.get_files_at(block_path);
		for file in files:
			var environment_entity = load_scene(block_path + "/" + file);
			add_entity(environment_entity, ENTITY_TYPE.ENVIRONMENTAL);


func add_entity(entity, type: ENTITY_TYPE):
	match(type):
		ENTITY_TYPE.ENVIRONMENTAL:
			var block_pos = Vector3i(floor(entity.get_position() / 16));
			if _terrain._block_descriptions.has(block_pos):
				_environment.add_child(entity);
			else:
				var path = get_block_path(block_pos) + DEFAULT_ENVIRONMENT_SAVE_FILE_PATH;
				unload_entity(entity, path);


func get_entity_filename(pos: Vector3i):
	return "/%s_%s_%s.res" % [pos.x, pos.y, pos.z];


func get_block_path(pos: Vector3i):
	return _entity_dir + ("/%s_%s_%s" % [pos.x, pos.y, pos.z]);


func instantiate_player(loadout):
	var player_faction = $Factions/PlayerFaction;
	
	var player_scene = player_faction.get_unit_description("Player").get_scene_with_loadout(loadout);
	_player = player_scene.instantiate();
	_player._faction = player_faction;
	_player.transform.origin.y = 66;
	
	var player_controller = PlayerControllerScene.instantiate();
	player_controller.name = "Controller";
	player_controller.transform.origin.y = 1;
	_player.add_child(player_controller);
	var unit_positioner = UnitPositioner.new();
	unit_positioner._terrain_tool = _terrain._tool;
	_player.add_child(unit_positioner);
	
	$Units.add_child(_player);


func add_component(component_config: Dictionary):
	match(component_config["Config Name"]):
		"Endless Horde":
			var rule = EndlessHorde.get_component_from_config(component_config);
			rule._faction = $Factions/EnemyFaction;
			rule._player = _player;
			$Rules.add_child(rule);


func get_save_data() -> Dictionary:
	var save_dict = {
		"save name": _save_name,
		"random seed": _random.seed,
		"random state": _random.state,
		"rules": [],
	};
	
	for rule in $Rules.get_children():
		save_dict["rules"].append(rule.get_save_data());
	
	return save_dict;


func load_save_data(save_dict: Dictionary):
	_save_name = save_dict["save name"];
	_random.seed = save_dict["random seed"];
	_random.state = save_dict["random state"];
	for component_config in save_dict["rules"]:
		add_component(component_config);


func save():
	var file := FileAccess.open(_save_dir + "/game.save", FileAccess.WRITE);
	file.store_string(JSON.stringify(get_save_data()));
	file.close();
	_terrain.generator.save(_save_dir);
	_voxels.save();


func load():
	_voxels.load();
	var file := FileAccess.open(_save_dir + "/game.save", FileAccess.READ);
	load_save_data(JSON.parse_string(file.get_as_text()));
	file.close();


func save_scene(scene, path):
	Util.set_owner_recursive(scene, scene);
	var packed_scene = PackedScene.new();
	packed_scene.pack(scene);
	ResourceSaver.save(packed_scene, path);


func load_scene(path):
	var scene = load(path).instantiate();
	DirAccess.remove_absolute(path);
	return scene;


func save_entity(entity, path):
	DirAccess.make_dir_recursive_absolute(path);
	save_scene(entity, path + get_entity_filename(Vector3i(entity.get_position())))


func unload_entity(entity, path):
	save_entity(entity, path);
	entity.queue_free();


func exit_player():
	save_scene(_player, _save_dir + "/player.tscn");
	_player.queue_free();


func load_player():
	var player_faction = $Factions/PlayerFaction;
	
	_player = load(_save_dir + "/player.tscn").instantiate()
	_player._faction = player_faction;
	$Units.add_child(_player);


static func get_save_name_config():
	var save_name_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	save_name_config.set_component_name("Save Name");
	save_name_config._removal_disabled = true;
	save_name_config._reposition_disabled = true;
	var save_name = TEXT_SETTING.instantiate();
	save_name._setting_name = "Save Name";
	save_name._default_value = "New World";
	save_name_config.add_setting(save_name);
	return save_name_config;
