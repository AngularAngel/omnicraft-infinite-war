extends Node

class_name NavigationGenerator;


const DEFAULT_NAV_SAVE_FILE_PATH = "/nav"


enum BlockNavStatus {QUEUED, STARTED, FINISHED, UNLOADED}


class BlockNavDescription:
	var pos: Vector3i;
	var status := BlockNavStatus.QUEUED;
	var task_id := -1;
	var region : NavigationRegion3D;


@onready var _walkable_regions = $WalkableRegions;


var _block_size: int;
var _padding := 2;
var _terrain_tool: VoxelTool;
var _async_tasks: Array[int]
var _waiting_regions := [];
var _async := true;
var _max_tasks := 15000
var _region_descriptions := {};
var _nav_save_dir;


func _ready():
	_nav_save_dir = get_parent()._save_dir + DEFAULT_NAV_SAVE_FILE_PATH;
	DirAccess.make_dir_recursive_absolute(_nav_save_dir);
	
	await get_parent().ready;
	_terrain_tool = get_parent()._terrain.get_voxel_tool()
	_terrain_tool.channel = VoxelBuffer.CHANNEL_TYPE
	
	var terrain = get_parent()._terrain as VoxelTerrain;
	_block_size = terrain.mesh_block_size;
	terrain.mesh_block_entered.connect(func(position: Vector3i):
		position = position * _block_size
		if position.y <= 64:
			queue_nav_mesh(position);
	);
	get_parent()._terrain.block_changed.connect(func(voxel_position):
		var adjustment = voxel_position % _block_size;
		if adjustment.x < 0:
			adjustment.x += _block_size;
		if adjustment.y < 0:
			adjustment.y += _block_size;
		if adjustment.z < 0:
			adjustment.z += _block_size;
		var position = voxel_position - adjustment;
		for i in range(-1, 2):
			for j in range(-1, 2):
				queue_nav_mesh(position + Vector3i(i * _block_size, 0, j * _block_size));
	);
	terrain.mesh_block_exited.connect(func(position: Vector3i):
		position = position * _block_size
		if _region_descriptions.has(position):
			var block_nav_desc = _region_descriptions[position];
			if block_nav_desc.status == BlockNavStatus.QUEUED:
				_waiting_regions.erase(block_nav_desc);
			elif block_nav_desc.status == BlockNavStatus.FINISHED:
				block_nav_desc.region.queue_free();
			block_nav_desc.status = BlockNavStatus.UNLOADED;
			_region_descriptions.erase(position);
	);
	

func _process(_delta):
	if _async:
		for task_id in _async_tasks:
			if WorkerThreadPool.is_task_completed(task_id):
				_async_tasks.erase(task_id);
		var i := 0;
		while i < 3 and _async_tasks.size() < _max_tasks and _waiting_regions.size() > 0:
			var block_nav_desc := _waiting_regions.pop_front() as BlockNavDescription;
			if block_nav_desc.status == BlockNavStatus.UNLOADED:
				continue;
			var job = Callable(self, "nav_mesh_async_job").bind(block_nav_desc.pos);
			var task_id = WorkerThreadPool.add_task(job, true, "Generate navmesh at %s" % block_nav_desc.pos);
			block_nav_desc.status = BlockNavStatus.STARTED;
			block_nav_desc.task_id = task_id;
			_async_tasks.append(task_id);
			i += 1;
	else:
		if _waiting_regions.size() > 0:
			var block_nav_desc := _waiting_regions.pop_front() as BlockNavDescription;
			if block_nav_desc.status == BlockNavStatus.UNLOADED:
				return;
			block_nav_desc.status = BlockNavStatus.STARTED;
			nav_mesh_job(block_nav_desc.pos)


func queue_nav_mesh(pos: Vector3i):
	var block_nav_desc;
	if _region_descriptions.has(pos):
		if _region_descriptions[pos].status == BlockNavStatus.QUEUED:
			return;
		block_nav_desc = _region_descriptions[pos];
		block_nav_desc.status = BlockNavStatus.QUEUED;
	else:
		block_nav_desc = BlockNavDescription.new();
		block_nav_desc.pos = pos;
		_region_descriptions[pos] = block_nav_desc;
	_waiting_regions.append(block_nav_desc);


func nav_mesh_job(pos: Vector3i):
	if not _region_descriptions.has(pos):
		return;
	
	var mesh;
	
	if FileAccess.file_exists(get_mesh_path(pos)):
		mesh = load_mesh(pos);
	else:
		mesh = generate_nav_mesh(pos);
	
	if mesh != null:
		add_mesh(mesh, pos);


func nav_mesh_async_job(pos: Vector3i):
	var mesh;
	
	if FileAccess.file_exists(get_mesh_path(pos)):
		mesh = load_mesh(pos);
	else:
		mesh = generate_nav_mesh(pos);
	
	if mesh != null:
		call_deferred("add_mesh", mesh, pos);


func add_mesh(mesh: NavigationMesh, pos: Vector3i):
	if not _region_descriptions.has(pos):
		return;
		
	var block_nav_desc = _region_descriptions[pos];
	if block_nav_desc.region != null:
		block_nav_desc.region.set_navigation_mesh(mesh);
	else:
		block_nav_desc.region = NavigationRegion3D.new();
		block_nav_desc.region.set_navigation_mesh(mesh);
		_walkable_regions.add_child(block_nav_desc.region);
	block_nav_desc.status = BlockNavStatus.FINISHED;
	block_nav_desc.task_id = -1;
	#print("Nav meshes generated for: %s" % pos);


func save_mesh(mesh: NavigationMesh, pos: Vector3i):
	var path = get_mesh_path(pos);
	ResourceSaver.save(mesh, path);


func load_mesh(pos: Vector3i):
	var path = get_mesh_path(pos);
	return load(path);


func get_mesh_path(pos: Vector3i):
	return _nav_save_dir + ("/%s_%s_%s.res" % [pos.x, pos.y, pos.z]);


func generate_nav_mesh(pos: Vector3i):
	var source_geometry = NavigationMeshSourceGeometryData3D.new();
	var mesh_pos = pos;
	for i in range(_block_size):
		greedy_mesh_layer(mesh_pos, source_geometry);
		mesh_pos.y += 1;
		
	
	if not source_geometry.has_data():
		return null;
	
	var mesh = NavigationMesh.new();
	mesh.agent_radius = 0.75;
	mesh.agent_max_climb = 1;
	mesh.filter_baking_aabb = AABB(Vector3(), Vector3(_block_size + _padding * 2, 
													  _block_size + _padding * 2,
													  _block_size + _padding * 2))
	mesh.filter_baking_aabb_offset = Vector3(pos) - Vector3(_padding, _padding, _padding);
	mesh.border_size = _padding;
	NavigationServer3D.bake_from_source_geometry_data(
		mesh,
		source_geometry,
	)
	
	save_mesh(mesh, pos);
	
	return mesh;


func greedy_mesh_layer(start_pos: Vector3i, source_geometry: NavigationMeshSourceGeometryData3D):
	var is_meshed_array := [];
	for i in range(_block_size + _padding * 2):
		is_meshed_array.append([]);
		for j in range(_block_size + _padding * 2):
			is_meshed_array[i].append(false);
	var current_offset := Vector2i(0, 0);
	while (current_offset.x < _block_size + _padding * 2):
		while (current_offset.y < _block_size + _padding * 2):
			var current_pos = Vector3i(start_pos);
			current_pos.x += current_offset.x - _padding;
			current_pos.z += current_offset.y - _padding;
			var mesh_size = greedy_mesh_expansion(current_pos, current_offset, is_meshed_array, Vector2i(_block_size + _padding * 2, _block_size + _padding * 2) - current_offset, source_geometry);
			for i in range(mesh_size.x):
				for j in range(mesh_size.y):
					is_meshed_array[current_offset.x + i][current_offset.y + j] = true;
			current_offset.y += mesh_size.y;
		current_offset.y = -1;
		current_offset.x += 1;


func greedy_mesh_expansion(start_pos: Vector3i, current_offset: Vector2i, is_meshed_array: Array, max_dimensions: Vector2i, source_geometry: NavigationMeshSourceGeometryData3D) -> Vector2i:
	if is_meshed_array[current_offset.x][current_offset.y] == true or not _is_navigable(start_pos):
		return Vector2i(1, 1);
	var dimensions = Vector2i(1, 1);
	while (dimensions.x < max_dimensions.x):
		if expanded_mesh_is_valid(start_pos, current_offset, is_meshed_array, dimensions, Vector2i(1, 0)):
			dimensions.x += 1;
		else:
			break;
	while (dimensions.y < max_dimensions.y):
		if expanded_mesh_is_valid(start_pos, current_offset, is_meshed_array, dimensions, Vector2i(0, 1)):
			dimensions.y += 1;
		else:
			break;
	source_geometry.add_faces(PackedVector3Array([
		Vector3(0.0, 1.0, 0.0),
		Vector3(dimensions.x, 1.0, dimensions.y),
		Vector3(0.0, 1.0, dimensions.y),
		Vector3(0.0, 1.0, 0.0),
		Vector3(dimensions.x, 1.0, 0.0),
		Vector3(dimensions.x, 1.0, dimensions.y),
	]), Transform3D(Basis(), start_pos));
	
	return dimensions;


func expanded_mesh_is_valid(start_pos: Vector3i, current_offset: Vector2i, is_meshed_array: Array, dimensions: Vector2i, increase: Vector2i) -> bool:
	var pos := Vector3i(start_pos);
	for i in range(dimensions.x + increase.x):
		for j in range(dimensions.y + increase.y):
			if (i >= dimensions.x or j >= dimensions.y) and (is_meshed_array[current_offset.x + i][current_offset.y + j] == true or not _is_navigable(pos)):
				return false;
			pos.z += 1;
		pos.z = start_pos.z;
		pos.x += 1;
	return true;


func _is_navigable(pos: Vector3i) -> bool:
	if _terrain_tool.get_voxel(pos) not in get_parent()._voxels._solid_voxel_ids:
		return false;
	# Skip if we are under another voxel
	for i in range(1, 4):
		if _terrain_tool.get_voxel(pos + Vector3i.UP * i) not in get_parent()._voxels._empty_voxel_ids:
			return false;
	return true;
