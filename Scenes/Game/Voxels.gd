extends Node

class_name Voxels


signal voxels_ready;


const DEFAULT_VOXELS_SAVE_FILE_PATH = "/voxels"


@onready var _voxel_library : VoxelBlockyTypeLibrary = $"../Terrain".mesher.library;


var _random: RandomNumberGenerator;
var _meshes := Meshes.new();
var _stone_layers := 8;
var _empty_id := 0;
var _multiblock_x_id := 1;
var _multiblock_y_id := 2;
var _multiblock_z_id := 3;
var _voxel_count := 0;
var _hard_crystal_id;
var _small_crystal_full_id;
var _large_crystal_full_id;
var _wall_voxel_id;
var _empty_voxel_ids := [];
var _solid_voxel_ids := [];
var _stone_voxel_ids := [];
var _voxels_save_dir;


func _ready():
	_voxels_save_dir = get_parent()._save_dir + DEFAULT_VOXELS_SAVE_FILE_PATH;
	DirAccess.make_dir_recursive_absolute(_voxels_save_dir);
	await get_parent().ready;
	
	_random = get_parent()._random;
	
	if not get_parent()._load:
		initialize();


func initialize():
	var air := addVoxelType("Air");
	
	air.base_model = VoxelBlockyModelEmpty.new();
	air.set_meta("isEmpty", true);
	_empty_voxel_ids.append(_voxel_count);
	
	_voxel_count += 1;
	
	var multiblock := addVoxelType("Multiblock");
	
	multiblock.base_model = VoxelBlockyModelMesh.new();
	
	multiblock.base_model.collision_aabbs = [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),];
	
	multiblock.attributes = [VoxelBlockyAttributeAxis.new()];
	
	_voxel_count += 3;
	
	var entity_voxel := addVoxelType("Entity Voxel");
	
	entity_voxel.base_model = VoxelBlockyModelMesh.new();
	
	entity_voxel.base_model.collision_aabbs = [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),];
	
	entity_voxel.set_meta("isUncollectable", true);
	
	_voxel_count += 1;
	
	for i in range(_stone_layers):
		var stone := addVoxelType("Stone %s" % i);
		
		stone.base_model = VoxelBlockyModelCube.new();
		stone.base_model.atlas_size_in_tiles = Vector2i(1, 1);
		stone.set_meta("isSolid", true);
		stone.set_meta("isStone", true);
		
		_solid_voxel_ids.append(_voxel_count);
		_stone_voxel_ids.append(_voxel_count);
	
		_voxel_count += 1;
	
		var stoneGameMaterial = generate_stone_material("Stone %s" % i)
		
		var stoneShaderMaterial := ShaderMaterial.new();
		stoneShaderMaterial.set_shader(Shaders._dict["Earth"]);
		
		stoneGameMaterial.set_shader_material_parameters(stoneShaderMaterial);
		
		stone.set_meta("energyCost", 10);
		stone.base_model.set_material_override(0, stoneShaderMaterial);
	
	var hard_crystal := addVoxelType("Hard Crystal");
		
	hard_crystal.base_model = VoxelBlockyModelCube.new();
	hard_crystal.base_model.atlas_size_in_tiles = Vector2i(1, 1);
	hard_crystal.set_meta("isSolid", true);
	
	_hard_crystal_id = _voxel_count;
	_solid_voxel_ids.append(_hard_crystal_id);

	_voxel_count += 1;

	var hardCrystalGameMaterial = generate_stone_material("Hard Crystal", Vector4(0.0005, 0.005495, 0.00049, 0.00548));
	
	var hardCrystalShaderMaterial := ShaderMaterial.new();
	hardCrystalShaderMaterial.set_shader(Shaders._dict["Earth"]);
	
	hardCrystalGameMaterial.set_shader_material_parameters(hardCrystalShaderMaterial);
	
	hard_crystal.set_meta("energyCost", 1000);
	hard_crystal.set_meta("collectionSpeed", 0.001);
	hard_crystal.set_meta("mass", 10);
	hard_crystal.base_model.set_material_override(0, hardCrystalShaderMaterial);
	
	var small_growth_stages = VoxelBlockyAttributeCustom.new();
	small_growth_stages.attribute_name = "Growth";
	var small_crystal = addMeshVoxelType("Small Crystal", _meshes._dict["Cross"].duplicate(), [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),], 1, null, [small_growth_stages]);
	
	small_crystal.set_meta("energyCost", 5);
	small_crystal.set_meta("collectionSpeed", 4);
	small_crystal.set_meta("mass", 2.5);
	small_crystal.set_meta("growthStages", 2);
	
	var small_crystal_full_shader_material := ShaderMaterial.new()
	small_crystal_full_shader_material.set_shader(Shaders._dict["Crystal"])
	small_crystal_full_shader_material.set_shader_parameter("uv1_scale", Vector2(sqrt(2), 1));
	
	small_crystal.base_model.set_material_override(0, small_crystal_full_shader_material);
	
	var small_crystal_partial_shader_material := ShaderMaterial.new()
	small_crystal_partial_shader_material.set_shader(Shaders._dict["Crystal"])
	small_crystal_partial_shader_material.set_shader_parameter("uv1_scale", Vector2(sqrt(2), 1));
	small_crystal_partial_shader_material.set_shader_parameter("crystal_threshold", 0.35);
	
	var variant_model = VoxelBlockyModelMesh.new();
	variant_model.mesh = _meshes._dict["Cross"].duplicate();
	variant_model.set_material_override(0, small_crystal_partial_shader_material);
	variant_model.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),]);
	variant_model.collision_mask = 1;
	small_crystal.set_variant_model([StringName("Growth"), 1], variant_model);
	
	_small_crystal_full_id = _voxel_count;
	
	_voxel_count += 2;
	
	var large_growth_stages = VoxelBlockyAttributeCustom.new();
	large_growth_stages.attribute_name = "Growth";
	large_growth_stages.value_count = 10;
	var large_crystal = addMeshVoxelType("Large Crystal", _meshes._dict["Large Cross"].duplicate(), [AABB(Vector3(0, 0, 0), Vector3(2, 2, 2)),], 1, null, [large_growth_stages]);
	
	large_crystal.set_meta("energyCost", 5);
	large_crystal.set_meta("collectionSpeed", 2);
	large_crystal.set_meta("mass", 5);
	large_crystal.set_meta("growthStages", 10);
	
	var large_crystal_shader_material := ShaderMaterial.new()
	large_crystal_shader_material.set_shader(Shaders._dict["Crystal"])
	large_crystal_shader_material.set_shader_parameter("uv1_scale", Vector2(sqrt(8), 2));
	
	large_crystal.base_model.set_material_override(0, large_crystal_shader_material)
	large_crystal.base_model.set_material_override(1, small_crystal_full_shader_material)
	
	for i in range(1, 10):
		large_crystal_shader_material = ShaderMaterial.new()
		large_crystal_shader_material.set_shader(Shaders._dict["Crystal"])
		large_crystal_shader_material.set_shader_parameter("uv1_scale", Vector2(sqrt(8), 2));
		large_crystal_shader_material.set_shader_parameter("crystal_threshold", 0.75 - 0.075 * i);
		small_crystal_partial_shader_material = ShaderMaterial.new()
		small_crystal_partial_shader_material.set_shader(Shaders._dict["Crystal"])
		small_crystal_partial_shader_material.set_shader_parameter("uv1_scale", Vector2(sqrt(2), 1));
		small_crystal_partial_shader_material.set_shader_parameter("crystal_threshold", 0.75 - 0.075 * i);
		variant_model = VoxelBlockyModelMesh.new();
		variant_model.mesh = _meshes._dict["Large Cross"].duplicate();
		variant_model.set_material_override(0, large_crystal_shader_material);
		variant_model.set_material_override(1, small_crystal_partial_shader_material);
		variant_model.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(2, 2, 2)),]);
		variant_model.collision_mask = 1;
		large_crystal.set_variant_model([StringName("Growth"), i], variant_model);
	
	_large_crystal_full_id = _voxel_count;
	
	_voxel_count += 10;
	
	var wall := addMeshVoxelType("Wall", _meshes._dict["Cube"]);
	
	var wallShaderMaterial := ShaderMaterial.new()
	wallShaderMaterial.set_shader(Shaders._dict["Building"])
	
	wallShaderMaterial.set_shader_parameter("horizontal_edges", true);
	wallShaderMaterial.set_shader_parameter("vertical_edges", true);
	
	wall.set_meta("energyCost", 10);
	wall.set_meta("collectionSpeed", 0.25);
	wall.set_meta("mass", 2);
	
	wall.base_model.set_material_override(0, wallShaderMaterial);
	
	_wall_voxel_id = _voxel_count;
	
	_voxel_count += 1;
	
	wall.set_meta("isSolid", true);
	
	_solid_voxel_ids.append(_wall_voxel_id);
	
	_voxel_library.bake();
	
	voxels_ready.emit();


func generate_stone_material(stone_name, brightening_vector := Vector4(0.08, 0.0495, 0.049, 0.048)) -> GameMaterial:
	
	var material := StoneMaterial.new();
	material._name = stone_name;
	var old_component := MaterialComponent.new();
	old_component.specular = 300.0 / 255.0
	for i in range(5):
		var new_component = MaterialComponent.new();
		var brightening = _random.randf() * brightening_vector.x;
		new_component.albedo = old_component.albedo + Vector3(
			brightening + _random.randf() * brightening_vector.y, 
			brightening + _random.randf() * brightening_vector.z, 
			brightening + _random.randf() * brightening_vector.w);
		new_component.specular = old_component.specular - _random.randf() * (100.0 / 255.0)
		new_component.roughness = old_component.roughness + _random.randf() * (100.0 / 255.0)
		material.add_component(new_component, 0.2);
		old_component = new_component
	
	var big_warp = 400 + _random.randi_range(0, 1248);
	var medium_warp = 10 + _random.randi_range(0, 44);
	
	material._layers = [
		Vector4(big_warp, big_warp, big_warp, 0),
		Vector4(medium_warp, 1 + _random.randf_range(0, 3.0), medium_warp, 0.222 + _random.randf_range(0, 0.888)),
		Vector4(0, medium_warp, 0, 0),
		Vector4(1.0 + _random.randf_range(0, 4.0), 3.0 + _random.randf_range(0, 10.0), 2.0 + _random.randf_range(0, 8.0), 0.111 + _random.randf_range(0, 0.444)),
		Vector4(),
		Vector4(0.025, 0, 0, 0)
	];
		
	return material;


func appendVoxelType(voxel_type: VoxelBlockyType):
	var types = _voxel_library.types;
	types.append(voxel_type)
	_voxel_library.set_types(types);


func addVoxelType(voxelName: String, voxel_family = null, attributes = null) -> VoxelBlockyType:
	var voxel_type = VoxelBlockyType.new()
	voxel_type.unique_name = voxelName;
	if attributes != null:
		voxel_type.attributes = attributes;
	appendVoxelType(voxel_type);
	voxel_type.set_meta("baseID", _voxel_count);
	if voxel_family != null:
		voxel_type.set_meta("voxelFamily", voxel_family);
	return voxel_type;


func addMeshVoxelType(voxelName : String, mesh : Mesh, collision_aabbs = [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),], collision_mask := 1, voxel_family = null, attributes = null) -> VoxelBlockyType:
	var mesh_voxel_type = addVoxelType(voxelName, voxel_family, attributes)
	mesh_voxel_type.base_model = VoxelBlockyModelMesh.new();
	mesh_voxel_type.base_model.mesh = mesh
	mesh_voxel_type.base_model.set_collision_aabbs(collision_aabbs)
	mesh_voxel_type.set_meta("selectionSize", collision_aabbs[0].size)
	mesh_voxel_type.base_model.collision_mask = collision_mask;
	return mesh_voxel_type;


func save():
	for voxel_type: VoxelBlockyType in _voxel_library.types:
		save_voxel_type(voxel_type);


func load():
	var files := DirAccess.get_files_at(_voxels_save_dir);
	var voxel_types := [];
	for file in files:
		voxel_types.append(load( _voxels_save_dir + "/" + file));
	voxel_types.sort_custom(
		func(a, b):
			return a.get_meta("baseID") < b.get_meta("baseID");
	)
	for voxel_type: VoxelBlockyType in voxel_types:
		appendVoxelType(voxel_type);
		var base_id = voxel_type.get_meta("baseID");
		if voxel_type.unique_name.begins_with("Hard Crystal"):
			_hard_crystal_id = base_id;
		elif voxel_type.unique_name.begins_with("Small Crystal"):
			_small_crystal_full_id = base_id;
		elif voxel_type.unique_name.begins_with("Large Crystal"):
			_large_crystal_full_id = base_id;
		elif voxel_type.unique_name.begins_with("Wall"):
			_wall_voxel_id = base_id;
		if voxel_type.get_meta("isEmpty", false):
			_empty_voxel_ids.append(base_id);
		if voxel_type.get_meta("isSolid", false):
			_solid_voxel_ids.append(base_id);
		if voxel_type.get_meta("isStone", false):
			_stone_voxel_ids.append(base_id);
	
	_voxel_library.bake();
	
	voxels_ready.emit();


func save_voxel_type(voxel_type: VoxelBlockyType):
	var path = get_voxel_type_path(voxel_type);
	ResourceSaver.save(voxel_type, path);


func get_voxel_type_path(voxel_type: VoxelBlockyType):
	return _voxels_save_dir + ("/%s.res" % voxel_type.unique_name);
