extends Node

class_name Units


var _voxel_entities = {};

var _emptiness = 0;

var _multiblock_x := 1;
var _multiblock_y := 2;
var _multiblock_z := 3;

var _entity_voxel = 4;


func get_voxel_entity(voxel_position) -> BaseBuildingUnit:
	return _voxel_entities[voxel_position];


func add_voxel_entity(voxel_entity):
	var voxel_position := Vector3i(voxel_entity.transform.origin);
		
	_voxel_entities[voxel_position] = voxel_entity;
	
	for x in range(voxel_entity._dimensions.x):
		for y in range(voxel_entity._dimensions.y):
			for z in range(voxel_entity._dimensions.z):
				var id = _entity_voxel;
				if x > 0:
					id = _multiblock_x;
				elif z > 0:
					id = _multiblock_z;
				elif y > 0:
					id = _multiblock_y;
					
				get_parent()._terrain.set_voxel(voxel_position + Vector3i(x, y, z), id);
	
	var this = self;
	voxel_entity.destroyed.connect(
		func():
			this.remove_voxel_entity(voxel_entity);
	)
	add_child(voxel_entity);


func remove_voxel_entity(voxel_entity):
	var voxel_position := Vector3i(voxel_entity.global_position);

	_voxel_entities.erase(voxel_position)
	
	for x in range(voxel_entity._dimensions.x):
		for y in range(voxel_entity._dimensions.y):
			for z in range(voxel_entity._dimensions.z):
				get_parent()._terrain.set_voxel(voxel_position + Vector3i(x, y, z), _emptiness);
				
