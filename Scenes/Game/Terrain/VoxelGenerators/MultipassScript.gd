extends VoxelGeneratorMultipassCB

class_name MultipassScript;


const MASSFRACTURE = preload("res://Scenes/Entities/Environmental/MassFracture/MassFracture.tscn");
const VALUE_SETTING = preload("res://Scenes/GUI/Common/Settings/Value/ValueSetting.tscn");
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")


var _seed;

var _game: Game;
var _voxels: Voxels;
var _random_generator: RandomNumberGenerator = null;


var _voxel_fillers := [];
var _features := [];
var _components := [];

func initialize():
	for i in range(pass_count):
		_features.append([[]]);


func _generate_pass(voxel_tool: VoxelToolMultipassGenerator, pass_index: int):
	var min_pos := voxel_tool.get_main_area_min()
	var max_pos := voxel_tool.get_main_area_max()
	
	var block_pos := Vector2i(min_pos.x, min_pos.z);
	var block_size := Vector2i(max_pos.x - min_pos.x, max_pos.z - min_pos.z);

	if pass_index == 0:
		 # Base terrain
		voxel_tool.set_mode(VoxelTool.MODE_SET);
		voxel_tool.set_channel(VoxelBuffer.CHANNEL_TYPE);
		for gz in range(min_pos.z, max_pos.z):
			for gx in range(min_pos.x, max_pos.x):
				for voxel_filler: VoxelFiller in _voxel_fillers:
					voxel_filler.fill_voxel_column(voxel_tool, gx, gz);
	var size = 0;
	for feature_list in _features[pass_index]:
		if feature_list.size() > 0:
			for i in range(1 + size * 2):
				for j in range(1 + size * 2):
					if i == 0 or j == 0 or i == size * 2 or j == size * 2:
						var rng := RandomNumberGenerator.new()
						rng.seed = hash(block_pos + Vector2i(block_size.x * i, block_size.y * j)) + _seed
						for feature: Feature in feature_list:
							feature.place(voxel_tool, Vector3(0, 0, 0), rng);
		size += 1;


func increase_pass_count(new_count):
	var current_count = pass_count;
	if new_count > current_count:
		pass_count = new_count;
		for i in range(new_count - current_count):
			_features.append([[]]);


func add_component(component_config: Dictionary):
	_components.append(component_config);
	match(component_config["Config Name"]):
		"Stone Filler":
			var stone_filler = StoneFiller.get_component_from_config(component_config, _random_generator);
			stone_filler._stone_ids = _voxels._stone_voxel_ids;
			_voxel_fillers.append(stone_filler);
		"Canyon Carver":
			_voxel_fillers.append(CanyonCarver.get_component_from_config(component_config, _random_generator));
		"Multilevel Canyon Carver":
			_voxel_fillers.append(MultilevelCanyonCarver.get_component_from_config(component_config, _random_generator));
		"Cave Carver 2D":
			_voxel_fillers.append(CaveCarver2D.get_component_from_config(component_config, _random_generator));
		"Multilevel Cave Carver 2D":
			_voxel_fillers.append(MultilevelCaveCarver2D.get_component_from_config(component_config, _random_generator));
		"Cave Carver 3D":
			_voxel_fillers.append(CaveCarver3D.get_component_from_config(component_config, _random_generator));
		"Hard Crystal Sphere":
			var min_radius = component_config["Min Radius"];
			var max_radius = component_config["Max Radius"];
			
			var dimension = 1 + max_radius * 2;
			
			var hard_crystal_sphere_placer = CallbackFeature.new();
			hard_crystal_sphere_placer._dimensions = Vector3i(dimension, dimension, dimension);
			hard_crystal_sphere_placer._callback = func(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
				voxel_tool.set_mode(VoxelTool.MODE_SET);
				voxel_tool.set_value(_voxels._hard_crystal_id);
				voxel_tool.set_channel(VoxelBuffer.CHANNEL_TYPE);
				
				var radius = random.randi_range(min_radius, max_radius);
				
				voxel_tool.do_sphere(position + Vector3i(max_radius, max_radius, max_radius), radius);
				return true;
			
			hard_crystal_sphere_placer = MetaFeature.wrap_feature(hard_crystal_sphere_placer, OffsetPlacement.new());
			hard_crystal_sphere_placer._offset = Vector3i(-max_radius, -max_radius, -max_radius);
			
			hard_crystal_sphere_placer = MetaFeature.wrap_feature(hard_crystal_sphere_placer, VoxelRequirement.new());
			hard_crystal_sphere_placer._override_dimensions = Vector3i(1, 1, 1);
			hard_crystal_sphere_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._solid_voxel_ids;
				
			hard_crystal_sphere_placer = MetaFeature.wrap_feature(hard_crystal_sphere_placer, DirectionalPlacement.new());
			hard_crystal_sphere_placer._offset = Vector3i(0, -1, 0);
			
			hard_crystal_sphere_placer = MetaFeature.wrap_feature(hard_crystal_sphere_placer, ScatteredPlacement.new());
			
			hard_crystal_sphere_placer = MetaFeature.wrap_feature(hard_crystal_sphere_placer, ChancePlacement.new());
			hard_crystal_sphere_placer._chance = component_config["Sphere Chance"];
			
			add_feature(hard_crystal_sphere_placer, 1);
		"Small Pit Placer":
			var small_pit_placer = VoxelFeature.new();
			
			small_pit_placer = MetaFeature.wrap_feature(small_pit_placer, VoxelRequirement.new());
			small_pit_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			small_pit_placer = MetaFeature.wrap_feature(small_pit_placer, OffsetRequirement.new());
			small_pit_placer._offset = Vector3i(0, 1, 0);
			small_pit_placer._requirement = VoxelRequirement.new();
			small_pit_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			small_pit_placer = MetaFeature.wrap_feature(small_pit_placer, DirectionalPlacement.new());
			small_pit_placer._offset = Vector3i(0, -1, 0);
			
			small_pit_placer = MetaFeature.wrap_feature(small_pit_placer, ScatteredPlacement.new());
			
			small_pit_placer = MetaFeature.wrap_feature(small_pit_placer, CountFeature.new());
			small_pit_placer._min_count = component_config["Min"];
			small_pit_placer._max_count = component_config["Max"];
			
			add_feature(small_pit_placer, 1);
		"Large Pit Placer":
			var large_pit = HomogenousMultiVoxelFeature.new();
			large_pit._dimensions = Vector3i(1, 2, 1);
				
			var side_pitting = VoxelFeature.new();
			
			side_pitting = MetaFeature.wrap_feature(side_pitting, VoxelRequirement.new());
			side_pitting._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			side_pitting = MetaFeature.wrap_feature(side_pitting, OffsetRequirement.new());
			side_pitting._offset = Vector3i(0, 1, 0);
			side_pitting._requirement = VoxelRequirement.new();
			side_pitting._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			side_pitting = MetaFeature.wrap_feature(side_pitting, DirectionalPlacement.new());
			side_pitting._offset = Vector3i(0, -1, 0);
			side_pitting._attempts = 2;
				
			side_pitting = MetaFeature.wrap_feature(side_pitting, SpreadPlacement.new());
			side_pitting._spread = Vector2i(2, 2);
				
			side_pitting = MetaFeature.wrap_feature(side_pitting, CountFeature.new());
			side_pitting._min_count = 3;
			side_pitting._max_count = 8;
			side_pitting._attempts = 10;
			
			side_pitting = MetaFeature.wrap_feature(side_pitting, OffsetPlacement.new());
			side_pitting._offset = Vector3i(0, 1, 0);
				
			var large_pit_placer = CompositeFeature.new();
			large_pit_placer._features.append(large_pit);
			large_pit_placer._features.append(side_pitting);
			
			large_pit_placer = MetaFeature.wrap_feature(large_pit_placer, VoxelRequirement.new());
			large_pit_placer._override_dimensions = Vector3i(1, 2, 1);
			large_pit_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			large_pit_placer = MetaFeature.wrap_feature(large_pit_placer, OffsetRequirement.new());
			large_pit_placer._offset = Vector3i(0, 2, 0);
			large_pit_placer._requirement = VoxelRequirement.new();
			large_pit_placer._requirement._override_dimensions = Vector3i(1, 1, 1);
			large_pit_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
			
			large_pit_placer = MetaFeature.wrap_feature(large_pit_placer, DirectionalPlacement.new());
			large_pit_placer._offset = Vector3i(0, -1, 0);
			
			large_pit_placer = MetaFeature.wrap_feature(large_pit_placer, ScatteredPlacement.new());
			
			large_pit_placer = MetaFeature.wrap_feature(large_pit_placer, CountFeature.new());
			large_pit_placer._min_count = component_config["Min"];
			large_pit_placer._max_count = component_config["Max"];
			
			add_feature(large_pit_placer, 1);
		"Small Stone Placer":
			var small_stone_placer = VoxelFeature.new();
			small_stone_placer._voxel_id = _voxels._stone_voxel_ids[0];
			
			small_stone_placer = MetaFeature.wrap_feature(small_stone_placer, VoxelRequirement.new());
			small_stone_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			small_stone_placer = MetaFeature.wrap_feature(small_stone_placer, OffsetRequirement.new());
			small_stone_placer._offset = Vector3i(0, -1, 0);
			small_stone_placer._requirement = VoxelRequirement.new();
			small_stone_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			small_stone_placer = MetaFeature.wrap_feature(small_stone_placer, DirectionalPlacement.new());
			small_stone_placer._offset = Vector3i(0, -1, 0);
			
			small_stone_placer = MetaFeature.wrap_feature(small_stone_placer, ScatteredPlacement.new());
			
			small_stone_placer = MetaFeature.wrap_feature(small_stone_placer, CountFeature.new());
			small_stone_placer._min_count = component_config["Min"];
			small_stone_placer._max_count = component_config["Max"];
			
			add_feature(small_stone_placer, 1);
		"Large Stone Placer":
			var large_stone_placer = HomogenousMultiVoxelFeature.new();
			large_stone_placer._dimensions = Vector3i(2, 2, 2);
			large_stone_placer._voxel_id = _voxels._stone_voxel_ids[0];
			
			large_stone_placer = MetaFeature.wrap_feature(large_stone_placer, VoxelRequirement.new());
			large_stone_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			large_stone_placer = MetaFeature.wrap_feature(large_stone_placer, OffsetRequirement.new());
			large_stone_placer._offset = Vector3i(0, -1, 0);
			large_stone_placer._requirement = VoxelRequirement.new();
			large_stone_placer._requirement._override_dimensions = Vector3i(2, 1, 2);
			large_stone_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			large_stone_placer = MetaFeature.wrap_feature(large_stone_placer, DirectionalPlacement.new());
			large_stone_placer._offset = Vector3i(0, -1, 0);
			
			large_stone_placer = MetaFeature.wrap_feature(large_stone_placer, ScatteredPlacement.new());
			
			large_stone_placer = MetaFeature.wrap_feature(large_stone_placer, CountFeature.new());
			large_stone_placer._min_count = component_config["Min"];
			large_stone_placer._max_count = component_config["Max"];
			
			add_feature(large_stone_placer, 1);
		"Crystal Placer":
			var small_crystal_placer = VoxelFeature.new();
			small_crystal_placer._voxel_id = _voxels._small_crystal_full_id;
			
			small_crystal_placer = MetaFeature.wrap_feature(small_crystal_placer, VoxelRequirement.new());
			small_crystal_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			small_crystal_placer = MetaFeature.wrap_feature(small_crystal_placer, OffsetRequirement.new());
			small_crystal_placer._offset = Vector3i(0, -1, 0);
			small_crystal_placer._requirement = VoxelRequirement.new();
			small_crystal_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			small_crystal_placer = MetaFeature.wrap_feature(small_crystal_placer, SpreadPlacement.new());
				
			small_crystal_placer = MetaFeature.wrap_feature(small_crystal_placer, CountFeature.new());
			small_crystal_placer._min_count = component_config["Small Crystal Min"];
			small_crystal_placer._max_count = component_config["Small Crystal Max"];
			
			var large_crystal_placer = MultiVoxelFeature.new();
			large_crystal_placer._voxel_id = _voxels._large_crystal_full_id;
			large_crystal_placer._dimensions = Vector3i(2, 2, 2);
			
			large_crystal_placer = MetaFeature.wrap_feature(large_crystal_placer, VoxelRequirement.new());
			large_crystal_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			large_crystal_placer = MetaFeature.wrap_feature(large_crystal_placer, OffsetRequirement.new());
			large_crystal_placer._offset = Vector3i(0, -1, 0);
			large_crystal_placer._requirement = VoxelRequirement.new();
			large_crystal_placer._requirement._override_dimensions = Vector3i(2, 1, 2);
			large_crystal_placer._requirement._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			large_crystal_placer = MetaFeature.wrap_feature(large_crystal_placer, SpreadPlacement.new());
			large_crystal_placer._spread = Vector2i(5, 5);
				
			large_crystal_placer = MetaFeature.wrap_feature(large_crystal_placer, CountFeature.new());
			large_crystal_placer._min_count = component_config["Large Crystal Min"];
			large_crystal_placer._max_count = component_config["Large Crystal Max"];
			
			var mass_fracture_placer = EntityFeature.new();
			mass_fracture_placer._entity_scene = MASSFRACTURE;
			mass_fracture_placer._entity_callback = _game.add_entity.bind(_game.ENTITY_TYPE.ENVIRONMENTAL);
			
			mass_fracture_placer = MetaFeature.wrap_feature(mass_fracture_placer, OffsetPlacement.new());
			mass_fracture_placer._offset = Vector3i(0, 2, 0);
			
			
			var crystal_placer = CompositeFeature.new();
			crystal_placer._features.append(mass_fracture_placer);
			crystal_placer._features.append(small_crystal_placer);
			crystal_placer._features.append(large_crystal_placer);
			
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, VoxelRequirement.new());
			crystal_placer._override_dimensions = Vector3i(1, 1, 1);
			crystal_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._empty_voxel_ids;
				
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, OffsetPlacement.new());
			crystal_placer._offset = Vector3i(0, 1, 0);
			
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, VoxelRequirement.new());
			crystal_placer._override_dimensions = Vector3i(1, 1, 1);
			crystal_placer._requirement = func(voxel_id):
				return voxel_id in _voxels._stone_voxel_ids;
				
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, DirectionalPlacement.new());
			crystal_placer._offset = Vector3i(0, -1, 0);
			
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, ScatteredPlacement.new());
			
			crystal_placer = MetaFeature.wrap_feature(crystal_placer, ChancePlacement.new());
			crystal_placer._chance = component_config["Crystal Chance"];
			
			add_feature(crystal_placer, 1);


func add_feature(feature, pass_index, size := 1):
	increase_pass_count(pass_index + 1);
	var feature_lists = _features[pass_index];
	for i in range (size):
		if feature_lists.size() <= i:
			feature_lists.append([]);
	feature_lists[size - 1].append(feature);


func get_save_data() -> Dictionary:
	var save_dict = {
		"Seed": _seed,
		"Components": _components
	}
	
	return save_dict;


func load_save_data(save_dict: Dictionary):
	_seed = save_dict["Seed"];
	
	for component in save_dict["Components"]:
		add_component(component);


func save(save_dir):
	var file := FileAccess.open(save_dir + "/terrain_generator.save", FileAccess.WRITE);
	file.store_string(JSON.stringify(get_save_data()));
	file.close();


func load(save_dir):
	var file := FileAccess.open(save_dir + "/terrain_generator.save", FileAccess.READ);
	load_save_data(JSON.parse_string(file.get_as_text()));
	file.close();


static func get_hard_crystal_sphere_config():
	var hard_crystal_sphere_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	hard_crystal_sphere_config.set_component_name("Hard Crystal Sphere");
	var hard_crystal_sphere_chance = VALUE_SETTING.instantiate();
	hard_crystal_sphere_chance._setting_name = "Sphere Chance";
	hard_crystal_sphere_chance._default_value = 0.125;
	hard_crystal_sphere_chance._step = 0.001;
	hard_crystal_sphere_config.add_setting(hard_crystal_sphere_chance);
	var min_radius = VALUE_SETTING.instantiate();
	min_radius._setting_name = "Min Radius";
	min_radius._default_value = 2.0;
	min_radius._step = 0.25;
	min_radius._maximum = 100.0;
	hard_crystal_sphere_config.add_setting(min_radius);
	var max_radius = VALUE_SETTING.instantiate();
	max_radius._setting_name = "Max Radius";
	max_radius._default_value = 10;
	max_radius._step = 0.25;
	max_radius._maximum = 100.0;
	hard_crystal_sphere_config.add_setting(max_radius);
	return hard_crystal_sphere_config;


static func get_small_pit_config():
	var small_pit_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	small_pit_config.set_component_name("Small Pit Placer");
	var small_pit_min = VALUE_SETTING.instantiate();
	small_pit_min._setting_name = "Min";
	small_pit_min._default_value = 3;
	small_pit_min._step = 1;
	small_pit_min._maximum = 100.0;
	small_pit_config.add_setting(small_pit_min);
	var small_pit_max = VALUE_SETTING.instantiate();
	small_pit_max._setting_name = "Max";
	small_pit_max._step = 1;
	small_pit_max._default_value = 5;
	small_pit_max._maximum = 100.0;
	small_pit_config.add_setting(small_pit_max);
	return small_pit_config;


static func get_large_pit_config():
	var large_pit_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	large_pit_config.set_component_name("Large Pit Placer");
	var large_large_pit_min = VALUE_SETTING.instantiate();
	large_large_pit_min._setting_name = "Min";
	large_large_pit_min._default_value = 1;
	large_large_pit_min._step = 1;
	large_large_pit_min._maximum = 100.0;
	large_pit_config.add_setting(large_large_pit_min);
	var large_large_pit_max = VALUE_SETTING.instantiate();
	large_large_pit_max._setting_name = "Max";
	large_large_pit_max._default_value = 3;
	large_large_pit_max._step = 1;
	large_large_pit_max._maximum = 100.0;
	large_pit_config.add_setting(large_large_pit_max);
	return large_pit_config;


static func get_small_stone_config():
	var small_stone_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	small_stone_config.set_component_name("Small Stone Placer");
	var small_stone_min = VALUE_SETTING.instantiate();
	small_stone_min._setting_name = "Min";
	small_stone_min._default_value = 5;
	small_stone_min._step = 1;
	small_stone_min._maximum = 100.0;
	small_stone_config.add_setting(small_stone_min);
	var small_stone_max = VALUE_SETTING.instantiate();
	small_stone_max._setting_name = "Max";
	small_stone_max._default_value = 10;
	small_stone_max._step = 1;
	small_stone_max._maximum = 100.0;
	small_stone_config.add_setting(small_stone_max);
	return small_stone_config;


static func get_large_stone_config():
	var large_stone_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	large_stone_config.set_component_name("Large Stone Placer");
	var large_large_stone_min = VALUE_SETTING.instantiate();
	large_large_stone_min._setting_name = "Min";
	large_large_stone_min._default_value = 2;
	large_large_stone_min._step = 1;
	large_large_stone_min._maximum = 100.0;
	large_stone_config.add_setting(large_large_stone_min);
	var large_large_stone_max = VALUE_SETTING.instantiate();
	large_large_stone_max._setting_name = "Max";
	large_large_stone_max._default_value = 4;
	large_large_stone_max._step = 1;
	large_large_stone_max._maximum = 100.0;
	large_stone_config.add_setting(large_large_stone_max);
	return large_stone_config;


static func get_crystal_config():
	var crystal_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	crystal_config.set_component_name("Crystal Placer");
	var crystal_chance = VALUE_SETTING.instantiate();
	crystal_chance._setting_name = "Crystal Chance";
	crystal_chance._default_value = 0.125;
	crystal_chance._step = 0.001;
	crystal_config.add_setting(crystal_chance);
	var small_crystal_min = VALUE_SETTING.instantiate();
	small_crystal_min._setting_name = "Small Crystal Min";
	small_crystal_min._default_value = 10;
	small_crystal_min._step = 1;
	small_crystal_min._maximum = 100.0;
	crystal_config.add_setting(small_crystal_min);
	var small_crystal_max = VALUE_SETTING.instantiate();
	small_crystal_max._setting_name = "Small Crystal Max";
	small_crystal_max._default_value = 20;
	small_crystal_max._step = 1;
	small_crystal_max._maximum = 100.0;
	crystal_config.add_setting(small_crystal_max);
	var large_crystal_min = VALUE_SETTING.instantiate();
	large_crystal_min._setting_name = "Large Crystal Min";
	large_crystal_min._default_value = 2;
	large_crystal_min._step = 1;
	large_crystal_min._maximum = 100.0;
	crystal_config.add_setting(large_crystal_min);
	var large_crystal_max = VALUE_SETTING.instantiate();
	large_crystal_max._setting_name = "Large Crystal Max";
	large_crystal_max._default_value = 6;
	large_crystal_max._step = 1;
	large_crystal_max._maximum = 100.0;
	crystal_config.add_setting(large_crystal_max);
	return crystal_config;
