extends Feature

class_name VoxelFeature


var _channel := VoxelBuffer.CHANNEL_TYPE;
var _voxel_id := 0;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, _random: RandomNumberGenerator):
	voxel_tool.set_mode(VoxelTool.MODE_SET);
	voxel_tool.set_value(_voxel_id);
	voxel_tool.set_channel(_channel);
	voxel_tool.set_voxel(position, _voxel_id);
	return true;
