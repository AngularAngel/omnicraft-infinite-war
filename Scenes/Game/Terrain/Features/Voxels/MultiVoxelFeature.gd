extends HomogenousMultiVoxelFeature

class_name MultiVoxelFeature


var _multiblock_x := 1;
var _multiblock_y := 2;
var _multiblock_z := 3;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, _random: RandomNumberGenerator):
	voxel_tool.channel = _channel;
	for x in range(_dimensions.x):
		for y in range(_dimensions.y):
			for z in range(_dimensions.z):
				var id = _voxel_id;
				if x > 0:
					id = _multiblock_x;
				elif z > 0:
					id = _multiblock_z;
				elif y > 0:
					id = _multiblock_y;
				
				voxel_tool.set_voxel(position + Vector3i(x, y, z), id);
	return true;
