extends VoxelFeature

class_name  HomogenousMultiVoxelFeature


var _dimensions : Vector3i


func get_dimensions():
	return _dimensions;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, _random: RandomNumberGenerator):
	voxel_tool.set_mode(VoxelTool.MODE_SET);
	voxel_tool.set_value(_voxel_id);
	voxel_tool.set_channel(_channel);
	voxel_tool.do_box(position, position + _dimensions - Vector3i(1, 1, 1));
	return true;
