extends HomogenousMultiVoxelFeature

class_name CallbackFeature


var _callback: Callable;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	return _callback.call(voxel_tool, position, random);
