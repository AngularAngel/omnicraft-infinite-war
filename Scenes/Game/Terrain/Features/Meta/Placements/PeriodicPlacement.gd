extends MetaFeature

class_name PeriodicPlacement;


var _offset := Vector2i(0, 0);
var _period := Vector2i(10, 10);
var _height := 1;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	var min_pos := voxel_tool.get_main_area_min();
	var max_pos := voxel_tool.get_main_area_max();
	
	var placed := false;
	
	var first_placement = position + Vector3i(
		min_pos.x + posmod(_offset.x + _period.x - min_pos.x, _period.x),
		_height,
		min_pos.z + posmod(_offset.y + _period.y - min_pos.z, _period.y)
	);
	var placement = Vector3i(first_placement);
	var i = 0;
	while placement.x + get_dimensions().x - 1 < max_pos.x:
		while placement.z + get_dimensions().z - 1 < max_pos.z:
			placed = _feature.place(voxel_tool, placement, random) or placed;
			placement = Vector3i(placement.x, placement.y, placement.z + _period.y);
		i += 1;
		placement = Vector3i(first_placement.x + _period.x * i, first_placement.y, first_placement.z);
	return placed;
