extends MetaFeature

class_name OffsetPlacement


var _offset := Vector3i(0, 0, 0);


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	return _feature.place(voxel_tool, position + _offset, random);
