extends OffsetPlacement

class_name DirectionalPlacement


var _attempts := 100;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	for i in range(_attempts):
		if _feature.place(voxel_tool, position + _offset * i, random):
			return true;
	return false;
