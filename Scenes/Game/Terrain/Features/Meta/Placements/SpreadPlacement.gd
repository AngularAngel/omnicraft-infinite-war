extends MetaFeature

class_name SpreadPlacement


var _spread := Vector2i(10, 10)


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	var placement = Vector3i(position);
	var cur_spread := Vector2(
		random.randf_range(-1.0, 1.0),
		random.randf_range(-1.0, 1.0)
	).normalized() * random.randf();
	placement.x += int(_spread.x * cur_spread.x);
	placement.z += int(_spread.y * cur_spread.y);
	return _feature.place(voxel_tool, placement, random);


func get_dimensions():
	return super.get_dimensions() + Vector3i(_spread.x * 2, 0, _spread.y * 2);
