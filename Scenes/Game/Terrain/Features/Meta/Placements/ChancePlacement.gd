extends MetaFeature

class_name ChancePlacement


var _chance := 0.01;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	if random.randf() < _chance:
		return _feature.place(voxel_tool, position, random);
	else:
		return false;
