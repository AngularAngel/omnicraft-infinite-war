extends MetaFeature

class_name ScatteredPlacement


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	var min_pos := voxel_tool.get_main_area_min();
	var max_pos := voxel_tool.get_main_area_max();
	var clearance = voxel_tool.get_editable_area_max() - max_pos;
	var padding = get_dimensions() - clearance;
	padding = clamp(padding, Vector3i(0, 0, 0), padding);
	var center = position + Vector3i(
		random.randi_range(min_pos.x, max_pos.x - padding.x),
		random.randi_range(min_pos.y + padding.y, max_pos.y - padding.y),
		random.randi_range(min_pos.z, max_pos.z - padding.z)
	);
	return _feature.place(voxel_tool, center, random);
