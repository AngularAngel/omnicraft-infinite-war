extends MetaFeature

class_name CountFeature


var _min_count := 1;
var _max_count := 10;
var _attempts := 20;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	var count = random.randi_range(_min_count, _max_count);
	var placed := 0;
	for i in range(_attempts):
		if _feature.place(voxel_tool, position, random):
			placed += 1;
		if placed >= count:
			break;
	return placed > 0;
