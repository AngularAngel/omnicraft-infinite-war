extends Feature

class_name MetaFeature


var _feature: Feature;


func get_dimensions() -> Vector3i:
	return _feature.get_dimensions();

static func wrap_feature(feature: Feature, wrapper: MetaFeature) -> MetaFeature:
	wrapper._feature = feature;
	return wrapper;
