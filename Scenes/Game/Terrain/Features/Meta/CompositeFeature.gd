extends Feature

class_name CompositeFeature;


var _features := [];


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	var placed := false;
	for feature: Feature in _features:
		placed = feature.place(voxel_tool, position, random) or placed;
	return placed;


func get_dimensions():
	var dimensions = Vector3i(0, 0, 0);
	
	for feature: Feature in _features:
		var f_dim = feature.get_dimensions();
		dimensions = Vector3i(
			max(dimensions.x, f_dim.x),
			max(dimensions.y, f_dim.y),
			max(dimensions.z, f_dim.z)
		);
	
	return dimensions;
