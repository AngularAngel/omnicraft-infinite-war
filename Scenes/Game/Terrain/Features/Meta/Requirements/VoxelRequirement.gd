extends FeatureRequirement

class_name VoxelRequirement;


var _channel := VoxelBuffer.CHANNEL_TYPE;
var _requirement := func(voxel_id):
	return voxel_id == 0;
var _override_dimensions = null;


func meets_requirement(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i) -> bool:
	voxel_tool.channel = _channel;
	var dimensions = _override_dimensions;
	if dimensions == null:
		dimensions = _feature.get_dimensions();
	for x in range(dimensions.x):
		for y in range(dimensions.y):
			for z in range(dimensions.z):
				if not _requirement.call(voxel_tool.get_voxel(position + Vector3i(x, y, z))):
					return false;
	return true;
