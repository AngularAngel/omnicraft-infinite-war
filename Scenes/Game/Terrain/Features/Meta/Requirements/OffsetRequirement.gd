extends FeatureRequirement

class_name OffsetRequirement;

var _offset := Vector3i(0, 0, 0);
var _requirement: FeatureRequirement: set = set_requirement;


func set_requirement(requirement: FeatureRequirement):
	_requirement = requirement;
	requirement._feature = _feature;


func meets_requirement(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i) -> bool:
	return _requirement.meets_requirement(voxel_tool, position + _offset);
