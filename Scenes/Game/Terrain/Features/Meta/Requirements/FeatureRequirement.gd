extends MetaFeature

class_name FeatureRequirement;


func place(voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, random: RandomNumberGenerator):
	if meets_requirement(voxel_tool, position):
		return _feature.place(voxel_tool, position, random);
	else:
		return false;


func meets_requirement(_voxel_tool: VoxelToolMultipassGenerator, _position: Vector3i) -> bool:
	return false;
