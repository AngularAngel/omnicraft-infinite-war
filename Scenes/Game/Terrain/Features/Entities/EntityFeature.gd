extends Feature

class_name EntityFeature


var _entity_scene: PackedScene;
var _entity_callback: Callable;


func place(_voxel_tool: VoxelToolMultipassGenerator, position: Vector3i, _random: RandomNumberGenerator):
	var entity = _entity_scene.instantiate();
	entity.position = Vector3(position);
	_entity_callback.call_deferred(entity);
	return true;
