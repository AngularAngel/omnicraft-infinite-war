extends VoxelTerrain

class_name OmniTerrain;


signal block_changed(pos: Vector3i);


const PARTICLES = preload("res://Scenes/Detritus/Particles/BaseParticles.tscn")
const VALUE_SETTING = preload("res://Scenes/GUI/Common/Settings/Value/ValueSetting.tscn");
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")


enum BlockStatus {LOADED, UNLOADED}


class BlockDescription:
	var pos: Vector3i;
	var status := BlockStatus.UNLOADED;


var _block_descriptions := {};
var _tool: VoxelToolTerrain;


func _ready() -> void:
	stream.directory = get_parent()._save_dir + "/regions";
	DirAccess.make_dir_recursive_absolute(stream.directory);
	mesher.library = VoxelBlockyTypeLibrary.new();
	set_meta("isTerrain", true);
	await get_parent().ready;
	generator = MultipassScript.new();
	generator.pass_count = 3;
	generator.set_pass_extent_blocks(1, 1);
	generator._game = get_parent();
	generator._voxels = $"../Voxels";
	generator._random_generator = get_parent()._random;
	generator.initialize();
	_tool = get_voxel_tool();
	_tool.channel = VoxelBuffer.CHANNEL_TYPE;
	if get_parent()._load:
		generator.load(get_parent()._save_dir);
	
	block_unloaded.connect(remove_block_description);
	mesh_block_exited.connect(remove_block_description);
	
	mesh_block_entered.connect(add_block_description);
	block_loaded.connect(add_block_description);


func add_block_description(block_pos: Vector3i):
	var block_desc;
	if _block_descriptions.has(block_pos):
		if _block_descriptions[block_pos].status == BlockStatus.LOADED:
			return;
		block_desc = _block_descriptions[block_pos];
		block_desc.status = BlockStatus.LOADED;
	else:
		block_desc = BlockDescription.new();
		block_desc.pos = block_pos;
		block_desc.status = BlockStatus.LOADED;
		_block_descriptions[block_pos] = block_desc;


func remove_block_description(block_pos: Vector3i):
	if _block_descriptions.has(position):
		var block_desc = _block_descriptions[position];
		block_desc.status = BlockStatus.UNLOADED;
		_block_descriptions.erase(position);


func take_damage(damage: DamageInstance) -> bool:
	if damage.source_type == DamageInstance.DAMAGE_SOURCE.RAYCAST and randf() < damage._raw_quantity / 2.0:
		var raycast := damage.source as RayCast3D;
		var collision_point = raycast.get_collision_point();
		var collision_normal = raycast.get_collision_normal();
		var terrain_particles = PARTICLES.instantiate();
		terrain_particles.transform.origin = collision_point;
		$"../Particles".add_child(terrain_particles);
		if collision_normal != Vector3.BACK:
			terrain_particles.look_at((collision_point + collision_normal), Vector3.BACK)
		
	damage.damaged_terrain.emit()
	return true;
	
	
func set_voxel(pos: Vector3i, voxel_id: int):
	_tool.set_voxel(pos, voxel_id);
	block_changed.emit(pos);


static func get_seed_config():
	var seed_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	seed_config.set_component_name("Seed");
	seed_config._removal_disabled = true;
	seed_config._reposition_disabled = true;
	var seed_value = VALUE_SETTING.instantiate();
	seed_value._setting_name = "Seed";
	seed_value._default_value = randi();
	seed_value._step = 1;
	seed_value._maximum = pow(2, 32) - 1;
	seed_config.add_setting(seed_value);
	return seed_config;
