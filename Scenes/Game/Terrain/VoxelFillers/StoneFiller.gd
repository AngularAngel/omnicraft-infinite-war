extends VoxelFiller

class_name StoneFiller


var _stone_ids;
var _noise := FastNoiseLite.new();
var _noise_multiplier := 0.25;
var _padding;


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var floor = voxel_tool.get_main_area_min().y;
	var ceiling = voxel_tool.get_main_area_max().y - 1;
	for gy in range(floor, ceiling):
		var stone_id = _padding + (1.0 - _padding * 2.0) * float(gy - floor) / float(ceiling - floor);
		stone_id += _noise.get_noise_3d(gx, gy, gz) * _noise_multiplier;
		stone_id = clamp(stone_id, 0.0, 0.999);
		stone_id = _stone_ids[int(stone_id * _stone_ids.size())];
		voxel_tool.set_voxel(Vector3i(gx, gy, gz), stone_id);


static func get_component_config():
	var stone_filler_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	stone_filler_config.set_component_name("Stone Filler");
	stone_filler_config._removal_disabled = true;
	stone_filler_config._reposition_disabled = true;
	var layers = VALUE_SETTING.instantiate();
	layers._setting_name = "Layers";
	layers._default_value = 8;
	layers._minimum = 1.0;
	layers._step = 1.0;
	layers._maximum = 20.0;
	stone_filler_config.add_setting(layers);
	var noise_mult = VALUE_SETTING.instantiate();
	noise_mult._setting_name = "Noise Multiplier";
	noise_mult._default_value = 0.25;
	stone_filler_config.add_setting(noise_mult);
	return stone_filler_config;


static func get_component_from_config(component_config, _random_generator):
	var stone_filler := StoneFiller.new();
	stone_filler._noise_multiplier = component_config["Noise Multiplier"];
	stone_filler._padding = stone_filler._noise_multiplier * 0.8;
	stone_filler._noise.set_seed(_random_generator.randi());
	return stone_filler;
