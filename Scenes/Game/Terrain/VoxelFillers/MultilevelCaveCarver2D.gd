extends VoxelFiller

class_name MultilevelCaveCarver2D


var _carver_id = 0;
var _cave_factor = 0.1;
var _cave_height = 6;
var _floor_y := 1;
var _levels := 6;
var _level_cave_adjustment := 0.0;
var _level_height_adjustment := 10;
var _noises := [];


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var cur_cave_factor = _cave_factor;
	var floor_y = _floor_y;
	for level in range(_levels):
		var cave_noise = _noises[level].get_noise_2d(gx, gz);
		if cave_noise > -cur_cave_factor and cave_noise < cur_cave_factor:
			voxel_tool.set_value(_carver_id);
			voxel_tool.do_box(Vector3i(gx, floor_y, gz), Vector3i(gx, floor_y + _cave_height, gz));
		cur_cave_factor += _level_cave_adjustment;
		floor_y += _level_height_adjustment;


static func get_component_config():
	var cave_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	cave_config.set_component_name("Multilevel Cave Carver 2D")
	var cave_factor = VALUE_SETTING.instantiate();
	cave_factor._setting_name = "Cave Factor";
	cave_factor._default_value = 0.1;
	cave_config.add_setting(cave_factor);
	var floor_height = VALUE_SETTING.instantiate();
	floor_height._setting_name = "Floor Height";
	floor_height._minimum = -32.0;
	floor_height._maximum = 63.0;
	floor_height._default_value = 1;
	cave_config.add_setting(floor_height);
	var cave_height = VALUE_SETTING.instantiate();
	cave_height._setting_name = "Cave Height";
	cave_height._maximum = 100.0;
	cave_height._default_value = 6.0;
	cave_config.add_setting(cave_height);
	var levels = VALUE_SETTING.instantiate();
	levels._setting_name = "Levels";
	levels._maximum = 20.0;
	levels._step = 1.0;
	levels._default_value = 6;
	cave_config.add_setting(levels);
	var level_cave_adjustment = VALUE_SETTING.instantiate();
	level_cave_adjustment._setting_name = "Level Cave Adjustment";
	level_cave_adjustment._minimum = -1.0;
	level_cave_adjustment._maximum = 1.0;
	level_cave_adjustment._default_value = 0.00;
	cave_config.add_setting(level_cave_adjustment);
	var level_height_adjustment = VALUE_SETTING.instantiate();
	level_height_adjustment._setting_name = "Level Height Adjustment";
	level_height_adjustment._maximum = 63.0;
	level_height_adjustment._step = 1.0;
	level_height_adjustment._default_value = 10;
	cave_config.add_setting(level_height_adjustment);
	return cave_config;


static func get_component_from_config(component_config, _random_generator):
	var cave_carver := MultilevelCaveCarver2D.new();
	cave_carver._cave_factor = component_config["Cave Factor"];
	cave_carver._floor_y = component_config["Floor Height"];
	cave_carver._cave_height = component_config["Cave Height"];
	cave_carver._levels = component_config["Levels"];
	cave_carver._level_cave_adjustment = component_config["Level Cave Adjustment"];
	cave_carver._level_height_adjustment = component_config["Level Height Adjustment"];
	for level in range(cave_carver._levels):
		cave_carver._noises.append(FastNoiseLite.new());
		cave_carver._noises[level].set_seed(_random_generator.randi());
	return cave_carver;
