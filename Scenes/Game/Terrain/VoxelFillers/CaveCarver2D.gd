extends VoxelFiller

class_name CaveCarver2D


var _carver_id = 0;
var _cave_factor = 0.1;
var _cave_height = 6;
var _floor_y := 1;
var _noise := FastNoiseLite.new();


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var cave_noise = _noise.get_noise_2d(gx, gz);
	if cave_noise > -_cave_factor and cave_noise < _cave_factor:
		voxel_tool.set_value(_carver_id);
		voxel_tool.do_box(Vector3i(gx, _floor_y, gz), Vector3i(gx, _floor_y + _cave_height, gz));


static func get_component_config():
	var cave_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	cave_config.set_component_name("Cave Carver 2D")
	var cave_factor = VALUE_SETTING.instantiate();
	cave_factor._setting_name = "Cave Factor";
	cave_factor._default_value = 0.1;
	cave_config.add_setting(cave_factor);
	var floor_height = VALUE_SETTING.instantiate();
	floor_height._setting_name = "Floor Height";
	floor_height._minimum = -32.0;
	floor_height._maximum = 63.0;
	floor_height._default_value = 1;
	cave_config.add_setting(floor_height);
	var cave_height = VALUE_SETTING.instantiate();
	cave_height._setting_name = "Cave Height";
	cave_height._maximum = 100.0;
	cave_height._default_value = 6.0;
	cave_config.add_setting(cave_height);
	return cave_config;


static func get_component_from_config(component_config, _random_generator):
	var cave_carver := CaveCarver2D.new();
	cave_carver._cave_factor = component_config["Cave Factor"];
	cave_carver._floor_y = component_config["Floor Height"];
	cave_carver._cave_height = component_config["Cave Height"];
	cave_carver._noise.set_seed(_random_generator.randi());
	return cave_carver;
