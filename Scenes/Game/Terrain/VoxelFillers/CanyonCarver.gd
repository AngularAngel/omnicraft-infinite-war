extends VoxelFiller

class_name CanyonCarver


var _bridge_factor = 0.0;
var _canyon_factor = -0.1;
var _noise := FastNoiseLite.new();
var _carver_id := 0;
var _floor_y := 1;


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var max_pos := voxel_tool.get_main_area_max()
	voxel_tool.set_value(_carver_id);
	#Canyons!
	var terrain_noise = _noise.get_noise_2d(gx, gz);
	if terrain_noise <= _canyon_factor:
		terrain_noise -= _canyon_factor;
		var y_top = max_pos.y - 1;
		if terrain_noise > -_bridge_factor:
			y_top = max(_floor_y, max_pos.y - max_pos.y * (_bridge_factor + terrain_noise) - 1);
		voxel_tool.do_box(Vector3i(gx, _floor_y, gz), Vector3i(gx, y_top, gz));


static func get_component_config():
	var canyon_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	canyon_config.set_component_name("Canyon Carver")
	var canyon_factor = VALUE_SETTING.instantiate();
	canyon_factor._setting_name = "Canyon Factor";
	canyon_factor._minimum = -1.0;
	canyon_factor._default_value = -0.1;
	canyon_config.add_setting(canyon_factor);
	var floor_height = VALUE_SETTING.instantiate();
	floor_height._setting_name = "Floor Height";
	floor_height._minimum = -32.0;
	floor_height._maximum = 63.0;
	floor_height._default_value = 1;
	canyon_config.add_setting(floor_height);
	return canyon_config;


static func get_component_from_config(component_config, _random_generator):
	var canyon_carver := CanyonCarver.new();
	canyon_carver._canyon_factor = component_config["Canyon Factor"];
	canyon_carver._floor_y = component_config["Floor Height"];
	canyon_carver._noise.set_seed(_random_generator.randi());
	return canyon_carver;
