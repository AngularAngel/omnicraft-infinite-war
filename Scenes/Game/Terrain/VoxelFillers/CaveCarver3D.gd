extends VoxelFiller

class_name CaveCarver3D


var _carver_id = 0;
var _cave_factor = 0.1;
var _noise := FastNoiseLite.new();


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var floor = voxel_tool.get_main_area_min().y;
	var ceiling = voxel_tool.get_main_area_max().y;
	for gy in range(floor, ceiling):
		var cave_noise = _noise.get_noise_3d(gx, gy, gz);
		if cave_noise > -_cave_factor and cave_noise < _cave_factor:
			voxel_tool.set_voxel(Vector3i(gx, gy, gz), _carver_id);


static func get_component_config():
	var cave_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	cave_config.set_component_name("Cave Carver 3D")
	var cave_factor = VALUE_SETTING.instantiate();
	cave_factor._setting_name = "Cave Factor";
	cave_factor._default_value = 0.1;
	cave_config.add_setting(cave_factor);
	return cave_config;


static func get_component_from_config(component_config, _random_generator):
	var cave_carver := CaveCarver3D.new();
	cave_carver._cave_factor = component_config["Cave Factor"];
	cave_carver._noise.set_seed(_random_generator.randi());
	return cave_carver;
