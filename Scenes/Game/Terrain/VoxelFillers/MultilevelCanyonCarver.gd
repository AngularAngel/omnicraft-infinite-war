extends VoxelFiller

class_name MultilevelCanyonCarver


var _bridge_factor = 0.0;
var _canyon_factor = -0.3;
var _noise := FastNoiseLite.new();
var _carver_id := 0;
var _floor_y := 1;
var _levels := 6;
var _level_canyon_adjustment := 0.1;
var _level_height_adjustment := 10;


func fill_voxel_column(voxel_tool: VoxelToolMultipassGenerator, gx: int, gz: int):
	var max_pos := voxel_tool.get_main_area_max()
	voxel_tool.set_value(_carver_id);
	#Canyons!
	var terrain_noise = _noise.get_noise_2d(gx, gz);
	var cur_canyon_factor = _canyon_factor;
	var floor_y = _floor_y;
	for level in range(_levels):
		if terrain_noise <= cur_canyon_factor:
			var bridge_noise = terrain_noise - cur_canyon_factor;
			var height = _level_height_adjustment;
			#print("%s, %s, %s, %s" % [terrain_noise, level, cur_canyon_factor, floor_y])
			if bridge_noise > -_bridge_factor:
				height -= (height * (_bridge_factor + bridge_noise) - 1);
			elif level == _levels - 1:
				height = max_pos.y - 1 - floor_y;
			voxel_tool.do_box(Vector3i(gx, floor_y, gz), Vector3i(gx, floor_y + height, gz));
		cur_canyon_factor += _level_canyon_adjustment;
		floor_y += _level_height_adjustment;
			


static func get_component_config():
	var canyon_config = COMPONENT_CONFIG.instantiate() as ComponentConfig;
	canyon_config.set_component_name("Multilevel Canyon Carver")
	var canyon_factor = VALUE_SETTING.instantiate();
	canyon_factor._setting_name = "Canyon Factor";
	canyon_factor._minimum = -1.0;
	canyon_factor._default_value = -0.3;
	canyon_config.add_setting(canyon_factor);
	var floor_height = VALUE_SETTING.instantiate();
	floor_height._setting_name = "Floor Height";
	floor_height._minimum = -32.0;
	floor_height._maximum = 63.0;
	floor_height._default_value = 1;
	canyon_config.add_setting(floor_height);
	var levels = VALUE_SETTING.instantiate();
	levels._setting_name = "Levels";
	levels._maximum = 20.0;
	levels._step = 1.0;
	levels._default_value = 6;
	canyon_config.add_setting(levels);
	var level_canyon_adjustment = VALUE_SETTING.instantiate();
	level_canyon_adjustment._setting_name = "Level Canyon Adjustment";
	level_canyon_adjustment._minimum = -1.0;
	level_canyon_adjustment._maximum = 1.0;
	level_canyon_adjustment._default_value = 0.1;
	canyon_config.add_setting(level_canyon_adjustment);
	var level_height_adjustment = VALUE_SETTING.instantiate();
	level_height_adjustment._setting_name = "Level Height Adjustment";
	level_height_adjustment._maximum = 63.0;
	level_height_adjustment._step = 1.0;
	level_height_adjustment._default_value = 10;
	canyon_config.add_setting(level_height_adjustment);
	return canyon_config;


static func get_component_from_config(component_config, _random_generator):
	var canyon_carver := MultilevelCanyonCarver.new();
	canyon_carver._canyon_factor = component_config["Canyon Factor"];
	canyon_carver._floor_y = component_config["Floor Height"];
	canyon_carver._levels = component_config["Levels"];
	canyon_carver._level_canyon_adjustment = component_config["Level Canyon Adjustment"];
	canyon_carver._level_height_adjustment = component_config["Level Height Adjustment"];
	canyon_carver._noise.set_seed(_random_generator.randi());
	return canyon_carver;
