extends Node

class_name Main

const DEFAULT_SAVE_FILE_PATH = "user://saves/"
const GameScene = preload("res://Scenes/Game/Game.tscn")
const LoadingScreenScene = preload("res://Scenes/GUI/Screens/Loading/LoadingScreen.tscn")


var _game: Game;
var _saves_dir = DEFAULT_SAVE_FILE_PATH;
var _mouse_sensitivity := 1.0;
var _show_fps := true;
var _show_position := true;


@onready var _settings_menu: SettingsMenu = $Settings


func _on_title_screen_enter_game_lobby():
	hide_menus()
	$GameLobbyScreen.show()


func _on_game_lobby_screen_started_new_game(settings: Dictionary):
	hide_menus()
	
	_game = GameScene.instantiate()
	
	_game._save_name = settings["Rules"][0]["Save Name"];
	_game._save_dir = _saves_dir + _game._save_name;
	DirAccess.make_dir_recursive_absolute(_game._save_dir);
	
	var seed = settings["Terrain"][0]["Seed"];
	
	var terrain = _game.get_node("Terrain") as VoxelTerrain;
	_game.get_node("Voxels")._stone_layers = settings["Terrain"][1]["Layers"];
	
	_game._random.seed = seed;
	
	add_child(_game);
	terrain.generator._seed = seed;
	
	for terrain_component in settings["Terrain"]:
		terrain.generator.add_component(terrain_component);
	
	for rule in settings["Rules"]:
		_game.add_component(rule);
	
	_game.save();
	
	var placeholder = VoxelViewer.new();
	
	_game.add_child(placeholder);
	
	var loading_screen:LoadingScreen = LoadingScreenScene.instantiate();
	
	var _task_max := [0];
	loading_screen._progress_callback = func(this):
		var tasks = VoxelEngine.get_stats().thread_pools.general.tasks + \
					_game._navigation_generator._async_tasks.size();
		_task_max[0] = max(tasks, _task_max[0]);
		if _task_max[0] > 0:
			if tasks == 0:
				return true;
			else:
				this._progress_bar.value = _task_max[0] - tasks;
				this._progress_bar.max_value = _task_max[0];
		return false;
	
	loading_screen._completion_callback = func():
		_game._navigation_generator._max_tasks = 300;
		_game.instantiate_player(settings["Loadout"]);
		placeholder.queue_free();
		
	add_child(loading_screen);


func _on_load_game(directory: String) -> void:
	hide_menus()
	
	_game = GameScene.instantiate();
	
	_game._save_name = directory;
	_game._save_dir = _saves_dir + _game._save_name;
	_game._load = true;
	add_child(_game);
	
	var placeholder = VoxelViewer.new();
	
	_game.add_child(placeholder);
	
	var loading_screen:LoadingScreen = LoadingScreenScene.instantiate();
	
	var _task_max := [0];
	loading_screen._progress_callback = func(this):
		var tasks = VoxelEngine.get_stats().thread_pools.general.tasks + \
					_game._navigation_generator._async_tasks.size();
		_task_max[0] = max(tasks, _task_max[0]);
		if _task_max[0] > 0:
			if tasks == 0:
				return true;
			else:
				this._progress_bar.value = _task_max[0] - tasks;
				this._progress_bar.max_value = _task_max[0];
		return false;
	
	loading_screen._completion_callback = func():
		_game._navigation_generator._max_tasks = 300;
		_game.load_player();
		placeholder.queue_free();
		
		
	add_child(loading_screen);


func exit_to_main_menu():
	if _game != null:
		_game.exit_player();
		var loading_screen:LoadingScreen = LoadingScreenScene.instantiate();
		
		var _task_max := [0];
		loading_screen._progress_callback = func(this):
			var tasks = VoxelEngine.get_stats().thread_pools.general.tasks + \
						_game._navigation_generator._async_tasks.size();
			_task_max[0] = max(tasks, _task_max[0]);
			if _task_max[0] > 0:
				if tasks == 0:
					return true;
				else:
					this._progress_bar.value = _task_max[0] - tasks;
					this._progress_bar.max_value = _task_max[0];
			return false;
		
		loading_screen._completion_callback = func():
			_game.queue_free();
			_game = null;
			hide_menus();
			$TitleScreen.show();
		
		add_child(loading_screen);
	else:
		hide_menus();
		$TitleScreen.show();


func _on_enter_load_saved_game_screen() -> void:
	hide_menus();
	$LoadSavedGameScreen.create_save_list();
	$LoadSavedGameScreen.show();


func hide_menus():
	$TitleScreen.hide();
	$GameLobbyScreen.hide();
	$LoadSavedGameScreen.hide();
	if _settings_menu.visible:
		hide_settings();
	#$MultiplayerScreen.hide()


func _process(delta: float) -> void:
	if _game == null and Input.is_action_just_pressed("prevMenu"):
		exit_to_main_menu();


func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		get_tree().quit();


func show_settings(screen_panel):
	screen_panel.hide();
	remove_child(_settings_menu);
	screen_panel.get_parent().add_child(_settings_menu);
	_settings_menu._screen_panel = screen_panel;
	_settings_menu.show();


func hide_settings():
	_settings_menu._exit();
