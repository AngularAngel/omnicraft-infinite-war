extends Control

class_name GameLobbyScreen


signal started_new_game(Dictionary);


const VALUE_SETTING = preload("res://Scenes/GUI/Common/Settings/Value/ValueSetting.tscn");
const COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/ComponentConfig/ComponentConfig.tscn")
const BUDGETED_COMPONENT_CONFIG = preload("res://Scenes/GUI/Common/BudgetedComponentConfig/BudgetedComponentConfig.tscn")


@onready var _loadout_setup: SetupMenu = $LoadoutSetup
@onready var _terrain_setup: SetupMenu = $TerrainSetup
@onready var _rule_setup: SetupMenu = $RuleSetup


func _ready() -> void:
	_loadout_setup.add_component_option("Chassis", func():
		_loadout_setup.set_component_disabled("Chassis");
		return PlayerComponentDescription.get_player_chassis_config();
	, true);
	
	_loadout_setup.add_component_option("Battery", func():
		_loadout_setup.set_component_disabled("Battery");
		return StorageComponentDescription.get_battery_config();
	, true);
	
	_loadout_setup.add_component_option("Mass Storage", func():
		_loadout_setup.set_component_disabled("Mass Storage");
		var mass_storage_config =  StorageComponentDescription.get_mass_storage_config();
		mass_storage_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Mass STorage", false);
		);
		return mass_storage_config;
	, true);
	
	_loadout_setup.add_component_option("Energy Generator", func():
		_loadout_setup.set_component_disabled("Energy Generator");
		var generator_config =  GeneratorComponentDescription.get_energy_generator_config();
		generator_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Energy Generator", false);
		);
		return generator_config;
	, true);
	
	_loadout_setup.add_component_option("Shield Generator", func():
		_loadout_setup.set_component_disabled("Shield Generator");
		var generator_config =  GeneratorComponentDescription.get_shield_generator_config();
		generator_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Shield Generator", false);
		);
		return generator_config;
	, true);
	
	_loadout_setup.add_component_option("Mass Generator", func():
		_loadout_setup.set_component_disabled("Mass Generator");
		var generator_config =  GeneratorComponentDescription.get_mass_generator_config();
		generator_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Mass Generator", false);
		);
		return generator_config;
	, false);
	
	_loadout_setup.add_component_option("Energy Conduit", func():
		var conduit_config = ConduitComponentDescription.get_energy_conduit_config();
		_loadout_setup.set_component_disabled("Energy Conduit");
		conduit_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Energy Conduit", false);
		);
		return conduit_config;
	, true);
	
	_loadout_setup.add_component_option("Mass Conduit", func():
		var conduit_config = ConduitComponentDescription.get_mass_conduit_config();
		_loadout_setup.set_component_disabled("Mass Conduit");
		conduit_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Mass Conduit", false);
		);
		return conduit_config;
	, true);
	
	_loadout_setup.add_component_option("Motivator", func():
		_loadout_setup.set_component_disabled("Motivator");
		var motivator_config = MotivatorComponentDescription.get_component_config();
		motivator_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Motivator", false);
		);
		return motivator_config;
	, true);
	
	_loadout_setup.add_component_option("Mass Collector", func():
		_loadout_setup.set_component_disabled("Mass Collector");
		var mass_collector_config = MassCollectorComponentDescription.get_component_config();
		mass_collector_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Mass Collector", false);
		);
		return mass_collector_config;
	, true);
	
	_loadout_setup.add_component_option("Construction Beam", func():
		_loadout_setup.set_component_disabled("Construction Beam");
		var construction_beam_config = ConstructionBeamComponentDescription.get_component_config();
		construction_beam_config.tree_exited.connect(
			func():
				_loadout_setup.set_component_disabled("Construction Beam", false);
		);
		return construction_beam_config;
	, true);
	
	var weapons = ["Rifle", "Laser", "Shield Disruptor", "Grenade Launcher", "Shotgun"];
	
	_loadout_setup.add_component_option("Laser", func():
		for weapon in weapons:
			_loadout_setup.set_component_disabled(weapon);
		var laser_config = LaserComponentDescription.get_component_config();
		laser_config.tree_exited.connect(
			func():
				for weapon in weapons:
					_loadout_setup.set_component_disabled(weapon, false);
		);
		return laser_config;
	, false);
	
	_loadout_setup.add_component_option("Shield Disruptor", func():
		for weapon in weapons:
			_loadout_setup.set_component_disabled(weapon);
		var shield_disruptor_config = ShieldDisruptorComponentDescription.get_component_config();
		shield_disruptor_config.tree_exited.connect(
			func():
				for weapon in weapons:
					_loadout_setup.set_component_disabled(weapon, false);
		);
		return shield_disruptor_config;
	, false);
	
	_loadout_setup.add_component_option("Grenade Launcher", func():
		for weapon in weapons:
			_loadout_setup.set_component_disabled(weapon);
		var grenade_launcher_config = GrenadeLauncherComponentDescription.get_component_config();
		grenade_launcher_config.tree_exited.connect(
			func():
				for weapon in weapons:
					_loadout_setup.set_component_disabled(weapon, false);
		);
		return grenade_launcher_config;
	, false);
	
	_loadout_setup.add_component_option("Shotgun", func():
		for weapon in weapons:
			_loadout_setup.set_component_disabled(weapon);
		var shotgun_config = ShotgunComponentDescription.get_component_config();
		shotgun_config.tree_exited.connect(
			func():
				for weapon in weapons:
					_loadout_setup.set_component_disabled(weapon, false);
		);
		return shotgun_config;
	, false);
	
	_loadout_setup.add_component_option("Rifle", func():
		for weapon in weapons:
			_loadout_setup.set_component_disabled(weapon);
		var rifle_config = RifleComponentDescription.get_component_config();
		rifle_config.tree_exited.connect(
			func():
				for weapon in weapons:
					_loadout_setup.set_component_disabled(weapon, false);
		);
		return rifle_config;
	, true);
	
	_terrain_setup.add_component_option("Seed", func():
		_terrain_setup.set_component_disabled("Seed");
		return OmniTerrain.get_seed_config();
	, true);
	
	_terrain_setup.add_component_option("Stone Filler", func():
		_terrain_setup.set_component_disabled("Stone Filler");
		return StoneFiller.get_component_config();
	, true);
	
	_terrain_setup.add_component_option("Canyon Carver", func():
		return CanyonCarver.get_component_config();
	, false);
	
	_terrain_setup.add_component_option("Multilevel Canyon Carver", func():
		return MultilevelCanyonCarver.get_component_config();
	, true);
		
	_terrain_setup.add_component_option("Cave Carver 2D", func():
		return CaveCarver2D.get_component_config();
	, false);
	
	_terrain_setup.add_component_option("Multilevel Cave Carver 2D", func():
		return MultilevelCaveCarver2D.get_component_config();
	, true);
		
	_terrain_setup.add_component_option("Cave Carver 3D", func():
		return CaveCarver3D.get_component_config();
	, false);
	
	_terrain_setup.add_component_option("Hard Crystal Sphere", func():
		return MultipassScript.get_hard_crystal_sphere_config();
	, true);
	
	_terrain_setup.add_component_option("Large Pit Placer", func():
		return MultipassScript.get_large_pit_config();
	, true);
	
	_terrain_setup.add_component_option("Small Pit Placer", func():
		return MultipassScript.get_small_pit_config();
	, true);
	
	_terrain_setup.add_component_option("Large Stone Placer", func():
		return MultipassScript.get_large_stone_config();
	, true);
	
	_terrain_setup.add_component_option("Small Stone Placer", func():
		return MultipassScript.get_small_stone_config();
	, true);
	
	_terrain_setup.add_component_option("Crystal Placer", func():
		return MultipassScript.get_crystal_config();
	, true);
	
	_rule_setup.add_component_option("Save Name", func():
		_rule_setup.set_component_disabled("Save Name");
		return Game.get_save_name_config();
	, true);
	
	_rule_setup.add_component_option("Endless Horde", func():
		return EndlessHorde.get_component_config();
	, true);


func _on_start_game_button_pressed():
	var settings = {
		"Loadout": _loadout_setup.get_setup(),
		"Terrain": _terrain_setup.get_setup(),
		"Rules": _rule_setup.get_setup(),
	};
	emit_signal("started_new_game", settings);
