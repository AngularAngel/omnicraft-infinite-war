extends Control

class_name LoadingScreen


@onready var _progress_bar: ProgressBar = $Panel/VBoxContainer/ProgressBar


var _task_max := 0;
var _progress_callback: Callable;
var _completion_callback: Callable;


func _process(delta: float) -> void:
	if _progress_callback.call(self):
		_completion_callback.call();
		queue_free();
