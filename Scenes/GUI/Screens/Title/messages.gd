extends Node

const _messages = [
	"Still In Alpha!",
	"But can it run Doom?",
	"Pew Pew Pew!",
	"Kablooey!",
	"Open Source!",
	"GPL Forever!",
	"Stay Tuned!",
	"Dun Dun Dun...!",
	"Procedurally Generated!",
	"Go Go Godot!",
	"Distribute Freely!",
	"Supremely Commanding!",
	"GNU Terry Pratchett",
]

const _political_messages = [
	"Slava Ukraine!",
	"This Machine Kills Fascists!"
]
