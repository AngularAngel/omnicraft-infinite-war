extends Control

class_name TitleScreen


signal enter_game_lobby;
signal enter_multiplayer_screen;
signal enter_load_saved_game_screen;


func _on_new_game_button_pressed():
	emit_signal("enter_game_lobby");


func _on_load_game_button_pressed():
	enter_load_saved_game_screen.emit();


func _on_multiplayer_button_pressed():
	emit_signal("enter_multiplayer_screen");


func _on_exit_button_pressed():
	get_tree().get_root().propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST);


func _on_settings_button_pressed() -> void:
	$"/root/Main".show_settings($Panel);
