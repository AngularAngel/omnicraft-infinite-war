extends Control

class_name SettingsMenu


const DEFAULT_SETTINGS_FILE_PATH = "user://settings.ini"
const INPUT_BINDING_SCENE = preload("res://Scenes/GUI/Common/Settings/InputBinding/InputBinding.tscn")
const INPUT_ACTIONS = {
	"moveLeft": "Move Left",
	"moveRight": "Move Right",
	"moveForward": "Move Forward",
	"moveBackward": "Move Backward",
	"moveUp": "Move Up",
	"moveDown": "Move Down",
	"jump": "Jump",
	"toggleFlight": "Toggle Flight",
	"leftAction": "Left Action",
	"rightAction": "Right Action",
	"accessHUD": "Access HUD",
	"pause": "Pause",
}


@onready var _video_settings: VBoxContainer = $Panel/VBoxContainer/VideoSettings
@onready var _audio_settings: VBoxContainer = $Panel/VBoxContainer/AudioSettings
@onready var _interface_settings: VBoxContainer = $Panel/VBoxContainer/InterfaceSettings
@onready var _input_bindings: VBoxContainer = $Panel/VBoxContainer/InputBindings
@onready var _setting_pages := [_video_settings, _audio_settings, _interface_settings, _input_bindings]


@onready var _window_mode: DropdownSetting = $"Panel/VBoxContainer/VideoSettings/Window Mode";


@onready var _mouse_sensitivity: SliderSetting = $"Panel/VBoxContainer/InterfaceSettings/Mouse Sensitivity"
@onready var _show_fps: CheckboxSetting = $"Panel/VBoxContainer/InterfaceSettings/Show FPS"
@onready var _show_position: CheckboxSetting = $"Panel/VBoxContainer/InterfaceSettings/Show Position"


var _config := ConfigFile.new();
var _is_remapping := false;
var _action_to_remap = null;
var _button_to_remap: InputBinding = null;
var _screen_panel = null; #Whatever we hid in order to show this.


func _ready() -> void:
	create_action_list();
	if !FileAccess.file_exists(DEFAULT_SETTINGS_FILE_PATH):
		for setting: BaseSetting in _video_settings.get_children():
			_config.set_value("video settings", setting.get_setting_name(), setting.get_value());
			
		for setting: BaseSetting in _audio_settings.get_children():
			_config.set_value("audio settings", setting.get_setting_name(), setting.get_value());
			
		for setting: BaseSetting in _interface_settings.get_children():
			_config.set_value("interface settings", setting.get_setting_name(), setting.get_value());
		
		for input_binding: InputBinding in _input_bindings.get_child(0).get_child(0).get_children():
			_config.set_value("input bindings", input_binding.get_setting_name(), input_binding.get_value());
			
		_config.save(DEFAULT_SETTINGS_FILE_PATH);
	else:
		_config.load(DEFAULT_SETTINGS_FILE_PATH);
		load_settings("video settings");
		load_settings("audio settings");
		load_settings("interface settings");
		load_settings("input bindings");
	
	for setting: BaseSetting in _video_settings.get_children():
		setting.setting_changed.connect(save_setting.bind("video settings", setting));
			
	for setting: BaseSetting in _audio_settings.get_children():
		setting.setting_changed.connect(save_setting.bind("audio settings", setting));
			
	for setting: BaseSetting in _interface_settings.get_children():
		setting.setting_changed.connect(save_setting.bind("interface settings", setting));
		
	for input_binding: InputBinding in _input_bindings.get_child(0).get_child(0).get_children():
		input_binding.setting_changed.connect(save_setting.bind("input bindings", input_binding));


func save_setting(section, setting):
	_config.set_value(section, setting.get_setting_name(), setting.get_value());
	_config.save(DEFAULT_SETTINGS_FILE_PATH);


func load_settings(section):
	match(section):
		"video settings":
			for setting: BaseSetting in _video_settings.get_children():
				setting.set_value(_config.get_value(section, setting.get_setting_name()));
		"audio settings":
			for setting: BaseSetting in _audio_settings.get_children():
				setting.set_value(_config.get_value(section, setting.get_setting_name()));
		"interface settings":
			for setting: BaseSetting in _interface_settings.get_children():
				setting.set_value(_config.get_value(section, setting.get_setting_name()));
		"input bindings":
			for input_binding: InputBinding in _input_bindings.get_child(0).get_child(0).get_children():
				input_binding.set_value(_config.get_value(section, input_binding.get_setting_name()));


func _settings_tab_changed(tab_id: int):
	for setting_page in _setting_pages:
		setting_page.hide();
	_setting_pages[tab_id].show();


func _on_window_mode_changed():
	if is_instance_valid(_window_mode):
		match(_window_mode.get_value()):
			"Windowed":
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED);
				DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, false);
				return;
			"Fullscreen":
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN);
				DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, false);
				return;
			"Borderless Window":
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED);
				DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, true);
				return;
			"Borderless Fullscreen":
				DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN);
				DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, true);
				return;


func _on_mouse_sensitivity_changed():
	if is_instance_valid(_mouse_sensitivity):
		$"/root/Main"._mouse_sensitivity = _mouse_sensitivity.get_value();


func _on_show_fps_changed():
	if is_instance_valid(_show_fps):
		$"/root/Main"._show_fps = _show_fps.get_value();


func _on_show_position_changed():
	if is_instance_valid(_show_position):
		$"/root/Main"._show_position = _show_position.get_value();


func create_action_list():
	InputMap.load_from_project_settings();
	for child in _input_bindings.get_child(0).get_child(0).get_children():
		child.queue_free();
	
	for action in INPUT_ACTIONS:
		var input_binding := INPUT_BINDING_SCENE.instantiate() as InputBinding;
		input_binding._setting_name = INPUT_ACTIONS[action];
		
		_input_bindings.get_child(0).get_child(0).add_child(input_binding);
		var events = InputMap.action_get_events(action);
		if events.size() > 0:
			input_binding.set_value(get_event_text(events[0]));
		
		input_binding.button_pressed.connect(_on_input_binding_pressed.bind(input_binding, action));
		input_binding.setting_changed.connect(_update_input_binding.bind(input_binding, action));


func _on_input_binding_pressed(button, action):
	if not _is_remapping:
		_is_remapping = true;
		button.set_text("Press key to bind...");
		_action_to_remap = action;
		_button_to_remap = button;


func _update_input_binding(input_binding: InputBinding, action):
	InputMap.action_erase_events(action);
	
	var event: InputEvent;
	
	var event_string: String = input_binding.get_value();
	
	if event_string.begins_with("mouse_"):
		event = InputEventMouseButton.new();
		event.button_index = int(event_string.split("_")[1]);
	else:
		event = InputEventKey.new();
		event.keycode = OS.find_keycode_from_string(event_string);
	
	InputMap.action_add_event(action, event);
	input_binding.set_text(get_event_text(event));


func _input(event):
	if _is_remapping:
		if (
			event is InputEventKey or
			(event is InputEventMouseButton and event.pressed)
		):
			if event is InputEventMouseButton and event.double_click:
				event.double_click = false;
			
			var event_string;
			if event is InputEventKey:
				event_string = OS.get_keycode_string(event.physical_keycode);
			elif event is InputEventMouseButton:
				event_string = "mouse_" + str(event.button_index);
			
			_button_to_remap.set_value(event_string)
			_is_remapping = false;
			_action_to_remap = null;
			_button_to_remap = null;
			
			accept_event();


func get_event_text(event: InputEvent):
	return event.as_text().trim_suffix(" (Physical)");


func _exit():
	_screen_panel.show();
	hide();
	var main = $"/root/Main";
	get_parent().remove_child(self);
	_screen_panel = null;
	main.add_child(self);
