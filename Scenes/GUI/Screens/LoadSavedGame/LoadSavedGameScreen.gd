extends Control

class_name LoadSavedGameScreen


signal load_game(directory: String);


const SAVE_LOADER_CONTAINER = preload("res://Scenes/GUI/Screens/LoadSavedGame/SaveLoaderContainer.tscn")


@onready var save_container: VBoxContainer = $Panel/ScrollContainer/SaveContainer


func create_save_list() -> void:
	for child in save_container.get_children():
		child.queue_free();
	var saves_dir = get_parent()._saves_dir;
	var directories := DirAccess.get_directories_at(get_parent()._saves_dir);
	for directory in directories:
		var save_loader: SaveLoaderContainer = SAVE_LOADER_CONTAINER.instantiate();
		save_container.add_child(save_loader);
		save_loader.set_save_name(directory)
		save_loader.loaded.connect(
			func():
				load_game.emit(directory);
		);
		save_loader.deleted.connect(
			func():
				OS.move_to_trash(ProjectSettings.globalize_path(saves_dir + "/" + directory));
				create_save_list();
		);
