extends HBoxContainer

class_name SaveLoaderContainer


signal loaded;
signal deleted;


@onready var _save_name: Label = $SaveName
@onready var _delete_button: Button = $DeleteButton
@onready var _confirm_button: Button = $ConfirmButton


var _confirming_deletion := false;


func set_save_name(save_name: String):
	_save_name.text = save_name;


func _on_load_button_pressed() -> void:
	loaded.emit();


func _on_delete_button_pressed() -> void:
	if not _confirming_deletion:
		_delete_button.text = "Cancel Deletion";
		_confirming_deletion = true;
		_confirm_button.show();
	else:
		_delete_button.text = "Delete";
		_confirming_deletion = false;
		_confirm_button.hide();


func _on_confirm_button_pressed() -> void:
	deleted.emit();
