extends Control

class_name PauseMenu


var _unpause_buffer = false;


@onready var _panel: Panel = $Panel


func _ready():
	visibility_changed.connect(_on_visibility_changed);


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("pause"):
		_resume();


#This is to prevent the same input event that pauses the game from immediately unpausing it.
#Probably a better way, but this will do for now.
func _on_visibility_changed():
	if visible:
		_unpause_buffer = true;


func _resume():
	if not _unpause_buffer:
		if not _panel.visible:
			_hide_settings_menu();
		else:
			get_parent().toggle_pause();
	else:
		_unpause_buffer = false;


func _show_settings():
	$"/root/Main".show_settings(_panel);


func _hide_settings_menu():
	$"/root/Main".hide_settings();


func _exit():
	_resume();
	get_parent().get_parent().release_mouse();
	get_node("/root/Main").exit_to_main_menu();
