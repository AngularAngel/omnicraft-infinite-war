extends Label3D

class_name CommodityDisplay


@export var _resource_name: String

var _parent;

func _ready():
	_parent = get_parent();
	while not _parent.has_method("get_commodity_amount"):
		_parent = _parent.get_parent();


func _process(_delta):
	var current : float = _parent.get_commodity_amount(_resource_name);
	var maximum : float = _parent.get_commodity_capacity(_resource_name);
	set_text("%s: %d/%d" % [_resource_name, round(current), maximum])
