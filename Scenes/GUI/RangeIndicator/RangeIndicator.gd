extends MeshInstance3D

class_name RangeIndicator


func set_range(range: float, do_scale := true):
	if do_scale:
		scale = Vector3(1, 1, 1) * range;
	$Intersection.set_instance_shader_parameter("sphere_radius", range);


func set_color(color: Color):
	set_instance_shader_parameter("outline_color", color);
	$Intersection.set_instance_shader_parameter("intersection_color", color);
