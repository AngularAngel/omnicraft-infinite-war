extends Control


const SpriteButtonScene = preload("res://Scenes/GUI/Common/SpriteButton/SpriteButton.tscn")


@onready var _crosshair_centerer: CenterContainer = $CrosshairCenterer
@onready var _pause_menu: Control = $PauseMenu


# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().mouse_captured.connect(_on_mouse_captured);
	get_parent().mouse_released.connect(_on_mouse_released);
	create_building_buttons();


func create_building_buttons():
	for child in $BuildPanel/BuildContainer.get_children():
		child.queue_free();
	for building_option: BuildingOption in get_parent().get_faction()._building_options:
		var button: SpriteButton = SpriteButtonScene.instantiate();
		button.selected.connect(_select_building_option.bind(building_option));
		$BuildPanel/BuildContainer.add_child(button);
		button.tooltip_text = building_option.get_name();
		if building_option._icon != null:
			button.set_sprite(ImageTexture.create_from_image(building_option._icon));


func _on_mouse_captured():
	_crosshair_centerer.show();


func _on_mouse_released():
	_crosshair_centerer.hide();


func _select_building_option(building_option: BuildingOption):
	get_parent().discard_construction_site();
	get_parent()._construction_site = building_option.get_construction_site();
	if building_option._preview_scene != null:
		get_parent()._building_preview = building_option._preview_scene.instantiate();
		get_parent()._terrain.add_child(get_parent()._building_preview);
		get_parent()._building_preview.show_range_groups_for_faction(get_parent().get_faction());
	get_parent().capture_mouse();


func toggle_pause():
	var tree := get_tree();
	if not tree.paused:
		_pause_menu.show();
		get_parent().release_mouse();
		tree.set_pause(true);
	else:
		_pause_menu.hide();
		get_parent().capture_mouse();
		tree.set_pause(false);
