extends Label



func _process(_delta: float) -> void:
	if not $"/root/Main"._show_position:
		hide();
	else:
		show();
		set_text("Position: %s" % get_parent().get_parent().get_global_position())
