extends HBoxContainer

class_name ResourceBox


enum ResourceType {RESOURCECONTAINER, COMMODITY, DEFENSE};


@export var _resource_name: String
@export var _resource_type := ResourceType.RESOURCECONTAINER;


@onready var _player = get_parent().get_parent().get_parent().get_parent();


var _resource_set := false;


func _ready():
	await _player.ready
	
	if not _resource_set and _resource_type == ResourceType.RESOURCECONTAINER:
		set_resource(_player._chassis._stats.get_stat(_resource_name))
		
	if not _resource_set and _resource_type == ResourceType.DEFENSE:
		set_resource(_player._chassis.get_node(_resource_name)._stats.get_stat("Health"))
			

func set_resource(resource: ResourceContainer):
	var on_resource_changed = func on_resource_changed():
		var current : float = resource._current_resource.get_current_score();
		var maximum : float = resource._maximum_resource.get_current_score();
		$ResourceBar.set_max(maximum);
		$ResourceBar.set_value(current);
		$ResourceScore.set_text("%d/%d" % [current, maximum]);
	resource._current_resource.changed.connect(on_resource_changed, CONNECT_DEFERRED);
	resource._maximum_resource.changed.connect(on_resource_changed, CONNECT_DEFERRED);
	on_resource_changed.call();
	_resource_set = true;
	

func set_commodity(commodity: String, chassis: BaseChassisComponent):
	var current : float = chassis.get_commodity_amount(commodity);
	var maximum : float = chassis.get_commodity_capacity(commodity);
	$ResourceBar.set_max(maximum);
	$ResourceBar.set_value(current);
	$ResourceScore.set_text("%d/%d" % [current, maximum]);
	_resource_set = true;


func _process(_delta):
	if _resource_type == ResourceType.COMMODITY:
		set_commodity(_resource_name, _player._chassis);
