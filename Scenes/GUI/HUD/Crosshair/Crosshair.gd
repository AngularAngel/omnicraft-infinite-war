extends Control

class_name Crosshair


@onready var _lines = [$Bottom, $LowerLeft, $UpperLeft, $Top, $UpperRight, $LowerRight]


func set_line_scale(line_scale: float) -> void:
	for child in get_children():
		child.scale = Vector2(line_scale, line_scale);
