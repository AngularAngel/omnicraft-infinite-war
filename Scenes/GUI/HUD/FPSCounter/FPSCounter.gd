extends Label

class_name FPSCounter


func _process(_delta: float) -> void:
	if not $"/root/Main"._show_fps:
		hide();
	else:
		show();
		set_text("FPS: %d" % Engine.get_frames_per_second())
