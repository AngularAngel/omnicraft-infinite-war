extends VBoxContainer

class_name SetupMenu


@onready var _label: Label = $Label
@onready var _addition: DropdownSetting = $AdditionDropdown
@onready var _component_list: VBoxContainer = $ScrollContainer/ComponentList


@export var _addition_name: String


var _menu_components: Dictionary = {};


func _ready():
	_label.text = get_name().capitalize();
	_addition._setting_name = _addition_name;
	
	_addition.setting_changed.connect(
		func():
			_on_addition_button_pressed();
	)


func add_component_option(option: String, callable: Callable, starting_component := false):
	_menu_components[option] = callable;
	_addition.add_option(option);
	if starting_component:
		add_component(option);
	

func _on_addition_button_pressed() -> void:
	if not _addition.is_option_disabled(_addition.get_value()):
		add_component(_addition.get_value());


func add_component(component):
	var menu_component = _menu_components[component].call();
	_component_list.add_child(menu_component);
	return menu_component;


func set_component_disabled(component, disabled := true):
	_addition.set_option_disabled(component, disabled);


func get_setup():
	var setup = [];
	for child in _component_list.get_children():
		if child.has_method("get_config"):
			setup.append(child.get_config());
	return setup;
