extends PanelContainer

class_name ComponentConfig;


@onready var _remove_button: Button = $VBoxContainer/HBoxContainer/RemoveButton
@onready var _component_list: VBoxContainer = $VBoxContainer/ComponentList


var _removal_disabled := false;
var _reposition_disabled := false;


func _ready() -> void:
	set_removal_disabled(_removal_disabled);
	set_reposition_disabled(_reposition_disabled);


func set_component_name(component_name):
	$VBoxContainer/HBoxContainer/ComponentName.text = component_name;


func add_setting(setting: BaseSetting):
	$VBoxContainer/ComponentList.add_child(setting);


func get_setting(setting_name):
	return $VBoxContainer/ComponentList.get_node(setting_name);


func set_removal_disabled(disabled := true):
	_remove_button.disabled = disabled;
	
	
func set_reposition_disabled(disabled := true):
	_reposition_disabled = disabled;
	$VBoxContainer/HBoxContainer/UpButton.disabled = disabled;
	$VBoxContainer/HBoxContainer/DownButton.disabled = disabled;
	var index = get_index();
	if index > 0:
		var above = get_parent().get_child(index - 1);
		$VBoxContainer/HBoxContainer/UpButton.disabled = disabled or above._reposition_disabled;
		if is_instance_valid(above):
			above.get_node("VBoxContainer/HBoxContainer/DownButton").disabled = disabled or above._reposition_disabled;
	
	if index < get_parent().get_child_count() - 1:
		var below = get_parent().get_child(index + 1);
		$VBoxContainer/HBoxContainer/DownButton.disabled = disabled or below._reposition_disabled;
		if is_instance_valid(below):
			below.get_node("VBoxContainer/HBoxContainer/UpButton").disabled = disabled or below._reposition_disabled;
			


func _on_up_button_pressed() -> void:
	var index = get_index();
	if index > 0:
		var above = get_parent().get_child(index - 1);
		get_parent().move_child(self, get_index() - 1);
		above.set_reposition_disabled(above._reposition_disabled);
		set_reposition_disabled(_reposition_disabled);


func _on_down_button_pressed() -> void:
	var index = get_index();
	if index < get_parent().get_child_count() - 1:
		var below = get_parent().get_child(index + 1);
		get_parent().move_child(self, get_index() + 1);
		below.set_reposition_disabled(below._reposition_disabled);
		set_reposition_disabled(_reposition_disabled);


func _on_remove_button_pressed() -> void:
	queue_free();


func get_config() -> Dictionary:
	var config := {"Config Name": $VBoxContainer/HBoxContainer/ComponentName.text};
	for child in _component_list.get_children():
		if child.has_method("get_setting_name"):
			config[child.get_setting_name()] = child.get_value();
	return config;
