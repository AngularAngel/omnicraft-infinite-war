extends PanelContainer

class_name SpriteButton


signal selected


@onready var _sprite = $Sprite as TextureRect


func _gui_input(event):
	if event is InputEventMouseButton:
		if not event.pressed:
			emit_signal("selected")


func set_sprite(sprite):
	_sprite.texture = sprite;
	_sprite.reset_size();
