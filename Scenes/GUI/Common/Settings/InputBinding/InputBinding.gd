extends ButtonSetting

class_name InputBinding;


@onready var _setting_button: Button = $SettingButton


func set_value(value):
	super.set_value(value);


func set_text(text):
	_setting_button.text = text;
