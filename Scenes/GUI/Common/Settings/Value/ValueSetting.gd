extends BaseSetting

class_name ValueSetting


@export var _minimum := 0.0;
@export var _maximum := 1.0;
@export var _default_value := 0.5;
@export var _step := 0.01;


@onready var _setting_input: SpinBox = $SettingInput

@export var format_string: String = "%s"


# Connect the signal handlers required to make this work seamlessly.
func _ready() -> void:
	super._ready();
	_setting_input.step = _step;
	_setting_input.min_value = _minimum;
	_setting_input.max_value = _maximum;
	_setting_input.value = _default_value;
	setting_changed.connect(format_value, CONNECT_DEFERRED);
	_setting_input.get_line_edit().focus_entered.connect(format_value, CONNECT_DEFERRED);
	_setting_input.get_line_edit().focus_exited.connect(format_value, CONNECT_DEFERRED);

	# Also, set this up initially
	format_value();


# This handler ensures the format is retained when exiting the field.
func format_value() -> void:
	await get_tree().create_timer(0.01).timeout;
	var caret_column = _setting_input.get_line_edit().caret_column;
	_setting_input.get_line_edit().text = (_setting_input.prefix + format_string + _setting_input.suffix) % _setting_input.value;
	_setting_input.get_line_edit().caret_column = caret_column;


func get_value():
	return _setting_input.value;


func set_value(value: float):
	_setting_input.value = value;
	super.set_value(value);


func _on_setting_changed(_value):
	setting_changed.emit();
