extends BaseSetting

class_name ButtonSetting


signal button_pressed;


var _value;


func get_value():
	return _value;


func set_value(value):
	_value = value;
	super.set_value(value);


func _on_setting_button_pressed():
	button_pressed.emit();
