extends SliderSetting

class_name AudioSliderSetting


var _bus_index: int;


func _ready():
	super._ready();
	_bus_index = AudioServer.get_bus_index(_setting_name);
	
	set_value(db_to_linear(AudioServer.get_bus_volume_db(_bus_index)));
	
	setting_changed.connect(_on_volume_changed);


func _on_volume_changed():
	AudioServer.set_bus_volume_db(_bus_index, linear_to_db(get_value()))
