extends BaseSetting

class_name TextSetting


@export var _default_value := "";


@onready var _text_input: TextEdit = $TextInput


var _forbidden_characters := ['\n'];


func _ready() -> void:
	super._ready();
	_text_input.text = _default_value;


func get_value():
	return _text_input.text;


func set_value(value: String):
	clean_text();
	_text_input.text = value;
	super.set_value(value);


func _on_text_changed():
	var caret_pos = Vector2i(_text_input.get_caret_column(),
							 _text_input.get_caret_line());
	clean_text();
	_text_input.set_caret_column(caret_pos.x);
	if caret_pos.y < _text_input.get_line_count():
		_text_input.set_caret_line(caret_pos.y);
	else:
		_text_input.set_caret_column(_text_input.get_line(caret_pos.y - 1).length());


func clean_text():
	for character in _forbidden_characters:
		_text_input.text = _text_input.text.replace(character, "");


func _on_setting_changed():
	setting_changed.emit();
