extends ValueSetting

class_name SliderSetting


@onready var _setting_slider: HSlider = $SettingSlider


func _ready() -> void:
	super._ready();
	_setting_slider.step = _step;
	_setting_slider.min_value = _minimum;
	_setting_slider.max_value = _maximum;
	_setting_slider.value = _default_value;


func _on_slider_value_changed(value: float):
	_setting_input.value = value;


func _on_input_value_changed(value: float):
	_setting_slider.value = value;
