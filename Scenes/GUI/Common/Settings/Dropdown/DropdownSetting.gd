extends BaseSetting

class_name DropdownSetting


@export var _options := [];


@onready var _setting_input: OptionButton = $SettingInput


func _ready():
	super._ready();
	
	for option in _options:
		_setting_input.add_item(option);
	_setting_input.item_selected.connect(
		func(_item):
			setting_changed.emit();
	);


func add_option(option):
	_options.append(option);
	_setting_input.add_item(option);


func set_option_disabled(option, disabled := true):
	_setting_input.set_item_disabled(_options.find(option), disabled);


func is_option_disabled(option) -> bool:
	return _setting_input.is_item_disabled(_options.find(option));


func get_value():
	return _setting_input.get_item_text(_setting_input.get_selected_id());


func set_value(value):
	_setting_input.selected = _options.find(value);
	super.set_value(value);
