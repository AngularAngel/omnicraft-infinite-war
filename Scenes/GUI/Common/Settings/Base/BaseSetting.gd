extends HBoxContainer

class_name BaseSetting


signal setting_changed;


@export var _setting_name: String: get = get_setting_name, set = set_setting_name;


func _ready() -> void:
	if not _setting_name.is_empty():
		name = _setting_name;
	$SettingLabel.text = _setting_name + ":";


func set_setting_name(setting_name):
	_setting_name = setting_name;
	$SettingLabel.text = _setting_name + ":";


func get_setting_name():
	return _setting_name;


func get_value():
	return null;


func set_value(value):
	setting_changed.emit();
