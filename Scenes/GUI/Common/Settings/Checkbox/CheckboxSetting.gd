extends BaseSetting

class_name CheckboxSetting


@onready var _setting_checkbox: CheckButton = $SettingCheckbox;


@export var _default_value := true;


func _ready() -> void:
	super._ready();
	set_value(_default_value);


func get_value():
	return _setting_checkbox.button_pressed;


func set_value(value: bool):
	_setting_checkbox.button_pressed = value;
	super.set_value(value);


func on_checkbox_toggled(_value):
	setting_changed.emit();
