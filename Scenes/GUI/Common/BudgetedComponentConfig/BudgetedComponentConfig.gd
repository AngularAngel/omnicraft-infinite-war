extends ComponentConfig

class_name BudgetedComponentConfig


signal cost_changed;


@onready var _cost_label: Label = $VBoxContainer/CostLabel


var _cost_function := func(setting):
	return setting.get_value() * setting.get_meta("costMultiplier", 1.0);


func add_setting(setting: BaseSetting):
	super.add_setting(setting);
	setting.setting_changed.connect(cost_changed.emit);


func recalculate_cost():
	var cost = get_meta("baseCost", 0);
	for child in _component_list.get_children():
		cost += _cost_function.call(child);
	cost = max(get_meta("minCost", 0), cost)
	_cost_label.text = "Cost: %s" % cost;
	return cost;
