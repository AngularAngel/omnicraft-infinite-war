extends SetupMenu

class_name BudgetedSetupMenu


@onready var _cost_label: Label = $CostLabel


func _ready() -> void:
	super._ready();
	if not get_parent().is_node_ready():
		await get_parent().ready;
	recalculate_cost();


func add_component(component):
	var component_config := super.add_component(component) as BudgetedComponentConfig;
	component_config.cost_changed.connect(recalculate_cost);
	component_config.tree_exited.connect(recalculate_cost);
	recalculate_cost();


func recalculate_cost():
	var cost = 0;
	for child: BudgetedComponentConfig in _component_list.get_children():
		cost += child.recalculate_cost();
	_cost_label.text = "Total Cost: %s" % cost;
