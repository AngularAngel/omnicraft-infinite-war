extends Node3D

class_name MassFracture


@onready var _random: RandomNumberGenerator = $"../../"._random;
@onready var _terrain: OmniTerrain = $"../../Terrain";
@onready var _voxel_tool: VoxelTool = _terrain._tool;
@onready var _voxels: Voxels = $"../../Voxels";


@export var _attempts := 10;


var _aabb := AABB(Vector3(-0.5, -0.5, -0.5), Vector3(1.0, 1.0, 1.0))


func _ready() -> void:
	$EnergyGlow.set_color(0.65);
	$Timer.timeout.connect(grow_crystals);


func grow_crystals():
	for i in range(_attempts):
		var voxel_pos := Vector3i(get_global_position());
		var dir := Vector2(_random.randf_range(-1, 1), _random.randf_range(-1, 1)).normalized() * _random.randf() * 10;
		voxel_pos += Vector3i(int(dir.x), -2, int(dir.y));
		var voxel_id = _voxel_tool.get_voxel(voxel_pos);
		
		if voxel_id == 0 and _voxel_tool.get_voxel(voxel_pos + Vector3i(0, -1, 0)) in _voxels._solid_voxel_ids:
			_voxel_tool.set_voxel(voxel_pos, _voxels._small_crystal_full_id + 1);
			return;
		
		
		var voxel_data = _voxels._voxel_library.get_type_name_and_attributes_from_model_index(voxel_id);
		var voxel_type := _voxels._voxel_library.get_type_from_name(voxel_data[0]);
		if voxel_id > _voxels._voxel_library.get_model_index_default(voxel_type.unique_name):
			_voxel_tool.set_voxel(voxel_pos, voxel_id - 1);
			return;
		elif voxel_id == _voxels._small_crystal_full_id and should_merge_crystals(voxel_pos):
			for x in range(2):
				for y in range(2):
					for z in range(2):
						var id = _voxels._large_crystal_full_id + 5;
						if x > 0:
							id = _voxels._multiblock_x_id;
						elif z > 0:
							id = _voxels._multiblock_z_id;
						elif y > 0:
							id = _voxels._multiblock_y_id;
						
						_voxel_tool.set_voxel(voxel_pos + Vector3i(x, y, z), id);
			return;


func should_merge_crystals(voxel_pos: Vector3i):
	return _voxel_tool.get_voxel(voxel_pos + Vector3i(1, 0, 0)) == _voxels._small_crystal_full_id and \
		   _voxel_tool.get_voxel(voxel_pos + Vector3i(0, 0, 1)) == _voxels._small_crystal_full_id and \
		   _voxel_tool.get_voxel(voxel_pos + Vector3i(1, 0, 1)) == _voxels._small_crystal_full_id;
