extends CharacterBody3D

class_name BaseMobileUnit


signal destroyed;


@export var _faction: Faction;


@onready var _chassis = $Chassis as BaseChassisComponent;
@onready var _motivator = $Chassis/Motivator as BaseMotivatorComponent;
@onready var _controller = $Controller as BaseController
@onready var _stair_checker: RayCast3D = $StairChecker
@onready var _stair_stepper_forward: CollisionShape3D = $StairStepperForward
@onready var _stair_stepper_left: CollisionShape3D = $StairStepperLeft
@onready var _stair_stepper_right: CollisionShape3D = $StairStepperRight
@onready var _initial_stair_stepper_dist := 1.0;


var _terrain: VoxelTerrain = null;
var _terrain_tool: VoxelTool = null;
var _was_grounded_last_frame := false;
var _stair_snapped_last_frame := false;
var _aabb := AABB(Vector3(-0.8, -1.4, -0.8), Vector3(1.6, 2.8, 1.6))

# Get the gravity from the project settings to be synced with RigidBody nodes.
var _gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


func _ready():
	var this = self;
	_chassis.component_destroyed.connect(
		func():
			if is_instance_valid(this):
				this.destroyed.emit();
				this.queue_free();
	);
	if _motivator != null:
		_motivator.reference();
	
	var game = get_parent().get_parent();
	
	_terrain = game.get_node("Terrain");
	
	if not game.is_node_ready():
		await game.ready;
	
	_terrain_tool = _terrain.get_voxel_tool();


func add_gravity(delta):
	if _motivator._movement_mode != null && _motivator._movement_mode.is_affected_by_gravity():
		velocity.y -= _gravity * delta;


func get_unit():
	return self;


func get_faction():
	return _faction;


func get_speed():
	if _motivator == null:
		return 0;
	else:
		return _motivator.get_speed();


func _physics_process(delta):
	_stair_stepper_forward.disabled = not is_on_floor();
	_stair_stepper_left.disabled = _stair_stepper_forward.disabled;
	_stair_stepper_right.disabled = _stair_stepper_forward.disabled;
	
	if _terrain.is_area_meshed(AABB(_aabb.position + position, _aabb.size)):
		var direction := _controller.get_direction();
		
		if _motivator != null:
			if _motivator.is_destroyed():
				_motivator.unreference();
				_motivator = null;
				velocity.x = 0;
				velocity.z = 0;
			else:
				_motivator.move(self, direction, delta);
		
		add_gravity(delta);
		rotate_stair_stepper();
		move_and_slide();
		snap_down_to_stair_check();
	
	#var mp := get_tree().get_multiplayer()
	#if mp.has_multiplayer_peer():
		## Broadcast our position to other peers.
		## Note, for other peers, this is a different script (remote_character.gd).
		## Each peer is authoritative of its own position for now.
		## TODO Make sure this RPC is not sent when we are not connected
		#rpc(&"receive_position", position)


func rotate_stair_stepper():
	var stair_stepper_offset = (velocity * Vector3(1, 0, 1)).normalized() * _initial_stair_stepper_dist
	_stair_stepper_forward.global_position = global_position + stair_stepper_offset;
	_stair_stepper_left.global_position = global_position + stair_stepper_offset.rotated(Vector3i(0, 1.0, 0), deg_to_rad(-50));
	_stair_stepper_right.global_position = global_position + stair_stepper_offset.rotated(Vector3i(0, 1.0, 0), deg_to_rad(50));


func snap_down_to_stair_check():
	var stair_snapped := false;
	if _motivator._movement_mode != null && _motivator._movement_mode.is_affected_by_gravity() \
		and _stair_checker.is_colliding() and not is_on_floor() and velocity.y <= 0 and \
		(_was_grounded_last_frame or _stair_snapped_last_frame):
		
		var body_test_result = PhysicsTestMotionResult3D.new();
		var body_test_params = PhysicsTestMotionParameters3D.new();
		body_test_params.from = global_transform;
		body_test_params.motion = Vector3(0, -1.0, 0);
		if PhysicsServer3D.body_test_motion(get_rid(), body_test_params, body_test_result):
			var step_distance = body_test_result.get_travel().y;
			position.y += step_distance;
			apply_floor_snap();
			stair_snapped = true;
	
	_was_grounded_last_frame = is_on_floor();
	_stair_snapped_last_frame = stair_snapped;
	
