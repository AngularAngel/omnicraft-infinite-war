extends BaseExternalComponent

class_name BaseWeaponComponent


signal firing_begun;
signal before_firing;
signal fired(DamageInstance);
signal after_firing;
signal firing_finished;


@onready var _range := $Stats/Range as PrimaryStat;
@onready var _damage_stats: DamageStats = $Stats/DamageStats


var _fire_conditions = [];
var _is_firing = false;
var _controller: BaseController = null;
var _raycast: RayCast3D = null;
var _use_hitscan := true;


func _ready():
	super._ready()
	get_parent().get_parent().add_collision_exception_with(self);
	
	var unit = get_unit();
	
	_controller = unit.get_node("Controller");
	
	if not _controller.is_node_ready():
		await _controller.ready;
	_raycast = _controller._raycast;


func _physics_process(_delta: float) -> void:
	if _is_firing:
		if allow_firing():
			fire();


func allow_firing():
	for fire_condition in _fire_conditions:
		if not fire_condition.call():
			return false;
	return true;


func fire():
	_raycast.collision_mask = 12; #layers 3+4
		
	before_firing.emit();
	
		
	var damage := _damage_stats.get_damage(_raycast);
	
	fired.emit(damage);
	
	if  _use_hitscan and _raycast != null and _raycast.is_colliding():
		var collision_object = _raycast.get_collider() as Node;
		
		if is_instance_valid(collision_object) and collision_object.has_method("take_damage"):
			collision_object.take_damage(damage);
	
		
	after_firing.emit();


func begin_firing():
	_is_firing = true;
	firing_begun.emit();


func finish_firing():
	_is_firing = false;
	firing_finished.emit();
