extends BaseExternalComponent

class_name BaseConduitComponent


@export var _resource_name: String
@export var _resource_type := ResourceType.COMMODITY;


@onready var _target_area = $TargetArea as TargetAreaComponentProperty;
@onready var _range_indicator: RangeIndicator = $TargetArea/Area/RangeIndicator
@onready var _energy_beam = $EnergyBeam as EnergyBeam;
@onready var _timer = $Timer as Timer;
@onready var _beam_ray = $EnergyBeam/BeamRay as RayCast3D;
@onready var _transfer_rate = $"Stats/Transfer Rate" as PrimaryStat
@onready var _transfer_speed = $"Stats/Transfer Speed" as PrimaryStat
@onready var _range = $TargetArea/Stats/Range as PrimaryStat
@onready var _priority = $Stats/Priority as PrimaryStat
@onready var _current_reception = $"Stats/Resources/Reception/Current Reception" as PrimaryStat


var _recent_targets = {};


func _ready():
	super._ready();
	_target_area._target_condition = _target_condition;
	_target_area._area.collision_mask = 2;
	_energy_beam._color = get_faction()._energy_color;
	_timer.wait_time = 1.0 / _transfer_speed.get_current_score();
	
	_beam_ray.target_position.z = -_range.get_current_score();
	_range.changed.connect(
		func():
			_beam_ray.target_position.z = -_range.get_current_score();
	);
	_transfer_speed.changed.connect(
		func():
			_timer.wait_time = 1.0 / _transfer_speed.get_current_score();
	);
	_timer.timeout.connect(
		func():
			send_resource(_timer.wait_time);
	);
	_timer.start();


func show_range_for_faction(faction: Faction):
	if get_faction() == faction:
		_range_indicator.set_visible(true);


func hide_range():
	_range_indicator.set_visible(false);


func send_resource(multiplier):
	var own_chassis = get_chassis() as BaseChassisComponent;
	var capacity = own_chassis.get_commodity_capacity(_resource_name);
	var free_capacity = own_chassis.get_commodity_free_capacity(_resource_name);
	var priority_mod := 0;
	if free_capacity == 0:
		priority_mod = -5;
	elif free_capacity < capacity * 0.1:
		priority_mod -= 1;
		if free_capacity < capacity * 0.05:
			priority_mod -= 1;
			if free_capacity < capacity * 0.01:
				priority_mod -= 1;
	elif free_capacity > capacity * 0.95:
		priority_mod += 1;
	_priority.set_current_score(_priority._base_score + priority_mod);
	
	var amount = min(
		own_chassis.get_commodity_amount(_resource_name),
		_transfer_rate.get_current_score() * multiplier
	);
	
	if amount <= 0.0:
		return amount;
	
	var chosen = self;
	var chosen_priority = _priority.get_current_score();
	
	for target in _target_area._targets:
		var target_chassis := target.get_chassis() as BaseChassisComponent;
		if target.get_free_capacity() <= 0.0:
			continue;
		if _recent_targets.has(target.get_unit()):
			if Time.get_unix_time_from_system() - _recent_targets[target.get_unit()] < 1.0:
				continue;
			else:
				_recent_targets.erase(chosen.get_unit())
		var target_priority = target._priority.get_current_score();
		if target_priority > chosen_priority or (target_priority == chosen_priority and \
				target_chassis.get_commodity_amount(_resource_name) < chosen.get_chassis().get_commodity_amount(_resource_name)):
			_beam_ray.look_at(target.get_global_position());
			_beam_ray.force_raycast_update();
			if _beam_ray.get_collider() == target:
				chosen = target;
				chosen_priority = target_priority;
		
	if chosen != self:
		amount = min(
			amount,
			chosen.get_free_capacity(),
		);
		chosen.receive_commodity(amount, self);
		get_chassis().subtract_commodity(_resource_name, amount);
		
		_energy_beam.set_position(get_global_position());
		_energy_beam._target = chosen
		_energy_beam.fade_in_out(_timer.wait_time);
		
	return amount;


func get_free_capacity():
	return min(get_chassis().get_commodity_free_capacity(_resource_name), _current_reception.get_current_score());


func receive_commodity(amount, source):
	get_chassis().receive_commodity(_resource_name, amount);
	_current_reception.modify_base_score(-amount);
		
	_recent_targets[source.get_unit()] = Time.get_unix_time_from_system();


func _target_condition(target):
	return target._resource_name == _resource_name and target.get_faction() == get_faction();
