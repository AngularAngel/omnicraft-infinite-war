extends AnimatableBody3D

class_name BaseExternalComponent


signal component_destroyed;


enum ResourceType {RESOURCE, COMMODITY};


@onready var _stats = $Stats as StatContainer
@onready var _surface_area: PrimaryStat = $Stats/SurfaceArea


var _defense_layers := [];
var _ref_count = RefCounted.new();
var subsidiary = null;


func _ready():
	_ref_count.init_ref();
	get_parent().add_collision_exception_with(self);
	set_meta("isComponent", true);
	set_meta("isRepairable", true);


func add_defense_layer(defense_layer):
	var i := 0;
	while i < _defense_layers.size() and _defense_layers[i]._priority >= defense_layer._priority:
		i += 1;
	_defense_layers.insert(i, defense_layer);


func is_destroyed() -> bool:
	var destroyed = true;
	for defense_layer: DefensiveLayerComponentProperty in _defense_layers:
		if defense_layer._defense_name == "Integrity" and defense_layer.get_health() > 0.0:
			destroyed = false;
	return destroyed;


func is_subsidiary():
	if subsidiary == null:
		subsidiary = get_parent().get_meta("isComponent", false);
	
	return subsidiary;


func take_damage(damage: DamageInstance) -> bool:
	if damage._source_type == damage.DAMAGE_SOURCE.AREA:
		damage._raw_quantity *= _surface_area.get_current_score();
	
	if is_subsidiary() and is_instance_valid(get_parent()):
		damage = get_parent().absorb_damage(damage);
	
	if damage._raw_quantity <= 0:
		return false;
	
	for defense_layer: DefensiveLayerComponentProperty in _defense_layers:
		defense_layer.take_damage(damage);
	
	if is_destroyed():
		destroy()
	return true;


func destroy():
	component_destroyed.emit()
	var parent = get_parent();
	if parent != null:
		parent.remove_child(self)


func reference():
	_ref_count.reference();


func unreference():
	_ref_count.unreference()
	if _ref_count.get_reference_count() <= 1 and is_destroyed():
		queue_free();


func get_unit():
	return get_parent().get_unit();


func get_chassis():
	return get_parent().get_chassis();


func get_faction():
	return get_unit().get_faction();
