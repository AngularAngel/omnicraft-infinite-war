extends BaseExternalComponent

class_name BaseChassisComponent


const PARTICLES = preload("res://Scenes/Detritus/Particles/BaseParticles.tscn")


var _shield: DefensiveLayerComponentProperty;
var _storage_components := {};


func _physics_process(_delta: float) -> void:
	if has_node("Shield Mesh"):
		$"Shield Mesh".set_instance_shader_parameter("shield_color", 
			Color(get_faction()._shield_color, _shield.get_health() / _shield._stats.get_stat("MaximumHealth").get_current_score()));


func add_defense_layer(defense_layer):
	super.add_defense_layer(defense_layer);
	if defense_layer._defense_name == "Shield":
		_shield = defense_layer;
		defense_layer.before_damaged.connect(before_shield_damage.bind(defense_layer));


func absorb_damage(damage: DamageInstance):
	for defense_layer: DefensiveLayerComponentProperty in _defense_layers:
		if defense_layer._priority > 0:
			defense_layer.take_damage(damage);
	return damage;


func before_shield_damage(damage_quantity, damage_instance, shield):
	if damage_quantity > 0:
		var cur_shield = shield.get_health();
		if cur_shield > 0 and damage_instance._source_type == DamageInstance.DAMAGE_SOURCE.RAYCAST:
			var raycast := damage_instance._source as RayCast3D;
			var normal = raycast.get_collision_normal();
			var shield_particles := PARTICLES.instantiate() as GPUParticles3D;
			var color = Color(get_faction()._shield_color,
							(cur_shield / _shield._stats.get_stat("MaximumHealth").get_current_score()) * 0.5)
			shield_particles.set_instance_shader_parameter("albedo", color);
			shield_particles.set_instance_shader_parameter("emission", get_faction()._shield_color);
			shield_particles.set_instance_shader_parameter("emission_energy", color.a);
			shield_particles.transform.origin = raycast.get_collision_point();
			get_node("/root/Main/Game/Particles").add_child(shield_particles);
			if normal != Vector3.BACK:
				shield_particles.look_at((raycast.get_collision_point() + raycast.get_collision_normal()), Vector3.BACK)
			


func take_damage(damage: DamageInstance) -> bool:
	return super.take_damage(absorb_damage(damage));


func get_storage_components(commodity: String) -> Array:
	if not _storage_components.has(commodity):
		_storage_components[commodity] = [];
	
	return _storage_components[commodity];


func add_storage_component(component: BaseStorageComponent):
	get_storage_components(component._resource_name).append(component)


func get_commodity_amount(commodity: String):
	var amount := 0.0
	for component: BaseStorageComponent in get_storage_components(commodity):
		amount += component.get_current();
	
	return amount;


func get_commodity_capacity(commodity: String):
	var amount := 0.0
	for component: BaseStorageComponent in get_storage_components(commodity):
		amount += component.get_maximum();
	
	return amount;


func get_commodity_free_capacity(commodity: String):
	var amount := 0.0
	for component: BaseStorageComponent in get_storage_components(commodity):
		amount += component.get_free_capacity();
	
	return amount;


func receive_commodity(commodity: String, amount: float) -> float:
	for component: BaseStorageComponent in get_storage_components(commodity):
		amount = component.receive(amount);
		if amount == 0:
			break;
	
	return amount;


func subtract_commodity(commodity: String, amount: float) -> float:
	for component: BaseStorageComponent in get_storage_components(commodity):
		amount = component.subtract(amount);
		if amount == 0:
			break;
	
	return amount;


func get_chassis():
	return self;
