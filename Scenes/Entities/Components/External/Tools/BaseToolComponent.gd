extends BaseExternalComponent

class_name BaseToolComponent


func _ready():
	super._ready()
	get_parent().get_parent().add_collision_exception_with(self);
