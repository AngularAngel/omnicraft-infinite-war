extends BaseToolComponent

class_name ConstructionBeam


@onready var _range := $Stats/Range as PrimaryStat;
@onready var _energy_expenditure = $"Stats/Energy Expenditure" as PrimaryStat
@onready var _mass_expenditure = $"Stats/Mass Expenditure" as PrimaryStat
@onready var _build_speed = $"Stats/Build Speed" as PrimaryStat


var _controller: BaseController = null;
var _units: Units = null;
var _entity_voxel = 4;
var _game: Game;
var _is_constructing := false;


func _ready():
	super._ready();
	
	var entity = get_unit();
	
	await entity.ready;
	
	_game = entity.get_parent().get_parent()
	
	_units = _game.get_node("Units");
	
	_controller = entity.get_node("Controller");
	$EnergyBeam._color = get_faction()._energy_color;
	$EnergyBeam._target = _controller._target_point;


func construct(delta) -> bool:
	#_controller._raycast.target_position.z = -_range.get_current_score();
	_controller._raycast.collision_mask = 12;
	_controller._raycast.force_raycast_update();
	
	if not _controller._raycast.is_colliding():
		return false;
	
	var collision_object = _controller._raycast.get_collider() as Node;
	
	if collision_object != null:
		if collision_object.has_method("get_unit") and collision_object.get_unit().get_meta("isConstructionSite", false):
			
			var construction_site = collision_object.get_unit() as ConstructionSite;
			var progress = _build_speed.get_current_score() * delta;
			var percent_expenditure = progress / construction_site._progress._maximum_resource.get_current_score()
			var energy_expenditure = _energy_expenditure.get_current_score() * percent_expenditure * construction_site._energy_cost.get_current_score();
			if get_parent().get_commodity_amount("Energy") >= energy_expenditure:
				var mass_expenditure = _mass_expenditure.get_current_score() * percent_expenditure * construction_site._mass_cost.get_current_score();
				if get_parent().get_commodity_amount("Mass") >= mass_expenditure:
					get_parent().subtract_commodity("Energy", energy_expenditure);
					get_parent().subtract_commodity("Mass", mass_expenditure);
					if not _is_constructing:
						$EnergyBeam.fade_in(0.25)
						_is_constructing = true;
					construction_site.add_progress(progress);
					return true;
		elif collision_object.get_meta("isRepairable", false):
			for defensive_layer: DefensiveLayerComponentProperty in collision_object._defense_layers:
				if defensive_layer._health.get_current_percent() < 1.0:
					var repair = _build_speed.get_current_score() * delta * defensive_layer._repair_speed_multiplier.get_current_score();
					#var percent_expenditure = repair / defensive_layer._health._maximum_resource.get_current_score();
					var energy_expenditure = _energy_expenditure.get_current_score() * repair * defensive_layer._repair_energy_cost.get_current_score();
					if get_parent().get_commodity_amount("Energy") >= energy_expenditure:
						var mass_expenditure = _mass_expenditure.get_current_score() * repair * defensive_layer._repair_mass_cost.get_current_score();
						if get_parent().get_commodity_amount("Mass") >= mass_expenditure:
							get_parent().subtract_commodity("Energy", energy_expenditure);
							get_parent().subtract_commodity("Mass", mass_expenditure);
							if not _is_constructing:
								$EnergyBeam.fade_in(0.25)
								_is_constructing = true;
							defensive_layer._health._current_resource.modify_base_score_with_min(repair);
							return true;
	
	return false;


func stop_constructing():
	_is_constructing = false;
	$EnergyBeam.fade_out(0.5, false, false)


func place_construction_site(construction_site, voxel_position) -> bool:
	if not can_place_construction_site(construction_site, voxel_position):
		return false;
	construction_site.transform.origin = Vector3(voxel_position);
	construction_site._faction = get_parent().get_parent()._faction;
	construction_site._terrain_tool = _controller._terrain_tool;
	construction_site._units = _units;
	
	_units.add_voxel_entity(construction_site);
	return true;


func can_place_construction_site(construction_site, voxel_position) -> bool:
	for i in range(construction_site._dimensions.x):
		for k in range(construction_site._dimensions.z):
			if _controller._terrain_tool.get_voxel(voxel_position + Vector3i(i, -1, k)) not in _game._voxels._solid_voxel_ids:
				return false;
			for j in range(construction_site._dimensions.y):
				if _controller._terrain_tool.get_voxel(voxel_position + Vector3i(i, j, k)) not in _game._voxels._empty_voxel_ids:
					return false;
	return true;
