extends BaseToolComponent

class_name MassCollector


@onready var _range := $Stats/Range as PrimaryStat;
@onready var _progress = $Stats/Resources/Progress as ResourceContainer;
@onready var _energy_expenditure = $"Stats/Energy Expenditure" as PrimaryStat
@onready var _mass_efficiency: PrimaryStat = $"Stats/Mass Efficiency"
@onready var _energy_beam: EnergyBeam = $EnergyBeam


var _is_collecting = false;
var _controller: BaseController = null;
var _target := Vector3i();

var _game: Game = null;
var _voxel_library : VoxelBlockyTypeLibrary = null;
var _hide_energy_beam := true;

func _ready():
	super._ready();
	
	var entity = get_parent().get_parent();
	
	await entity.ready
	
	_controller = entity.get_node("Controller");
	
	_game = entity.get_parent().get_parent();
	
	if not _game.is_node_ready():
		await _game.ready;
	
	var _terrain = _game._terrain
	
	_voxel_library = _game._voxels._voxel_library;
	_energy_beam.reverse();
	_energy_beam._color = get_faction()._energy_color;
	_energy_beam._target = _controller._target_point;



func _physics_process(delta):
	var hit := _controller._get_pointed_voxel(_range.get_current_score());
	
	if hit == null:
		stop_collecting();
		return;
	
	var hit_voxel_data = _controller._get_hit_voxel_data(hit.position)
	var hit_voxel: VoxelBlockyType = hit_voxel_data[1];
	var voxel_position = hit_voxel_data[2] 
	
	if _is_collecting:
		if voxel_position != _target:
			start_collecting();
		else:
			var progress = _progress._regen_resource.get_current_score() * delta * hit_voxel.get_meta("collectionSpeed", 1)
			var energy_expenditure = _energy_expenditure.get_current_score() * progress * hit_voxel.get_meta("energyCost", 1);
			if get_parent().get_commodity_amount("Energy") >= energy_expenditure:
				get_parent().subtract_commodity("Energy", energy_expenditure);
				_progress._current_resource.modify_base_score(progress)
				if _progress._current_resource.get_current_score() >= _progress._maximum_resource.get_current_score():
					get_parent().receive_commodity("Mass", _mass_efficiency.get_current_score() * hit_voxel.get_meta("mass", 1));
					
					var size = Vector3i(hit_voxel.get_meta("selectionSize", Vector3(1, 1, 1)).ceil())
					
					var cur_voxel_id = _controller._terrain_tool.get_voxel(voxel_position);
					if cur_voxel_id - _voxel_library.get_model_index_default(hit_voxel.unique_name) < \
							hit_voxel.get_meta("growthStages", 1) - 1:
						_controller._terrain_tool.set_voxel(voxel_position, cur_voxel_id + 1);
					else:
						for x in range(size.x):
							for y in range(size.y):
								for z in range(size.z):
									var set_pos = Vector3i(voxel_position)
									set_pos.x += x;
									set_pos.y += y;
									set_pos.z += z;
									_controller._terrain_tool.set_voxel(set_pos, 0);
					_progress._current_resource.set_base_score(0);


func start_collecting():
	var hit := _controller._get_pointed_voxel(_range.get_current_score());
	
	if hit != null:
		var hit_voxel_data = _controller._get_hit_voxel_data(hit.position)
		var hit_voxel = hit_voxel_data[1];
		var voxel_position = hit_voxel_data[2] 
		if hit_voxel.get_meta("isUncollectable", false):
			return;
		_is_collecting = true;
		_target = voxel_position;
		_progress._current_resource.set_base_score(0);
		if _hide_energy_beam:
			_energy_beam.fade_in(0.25)


func stop_collecting():
	if _is_collecting:
		_is_collecting = false;
		if _hide_energy_beam:
			_energy_beam.fade_out(0.5, false, false)
