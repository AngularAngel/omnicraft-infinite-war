extends BaseExternalComponent

class_name BaseMotivatorComponent


var _movement_mode : MovementMode = WalkingMovement.new();

@onready var _speed = $Stats/Speed as PrimaryStat
@onready var jump_velocity: PrimaryStat = $"Stats/Jump Velocity"
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat


func _ready():
	super._ready()
	get_parent().get_parent().add_collision_exception_with(self);


func get_speed() -> float:
	return _speed.get_current_score();


func set_movement_mode(movement_mode):
	_movement_mode = movement_mode;


func add_gravity(entity, delta):
	if _movement_mode.is_affected_by_gravity():
		entity.add_gravity(delta)


func move(entity, direction: Vector3, delta):
	if  not direction.is_zero_approx():
		var expenditure = _energy_cost.get_current_score() * delta;
		if get_parent().get_commodity_amount("Energy") >= expenditure:
			get_parent().subtract_commodity("Energy", expenditure);
		else:
			direction = Vector3(0, 0, 0);
	
	_movement_mode.move(entity, direction);


func get_movement(entity, direction: Vector3, delta):
	if  not direction.is_zero_approx():
		var expenditure = _energy_cost.get_current_score() * delta;
		if get_parent().get_commodity_amount("Energy") >= expenditure:
			get_parent().subtract_commodity("Energy", expenditure);
		else:
			direction = Vector3(0, 0, 0);
	
	return _movement_mode.get_movement(entity, direction);


func jump(entity):
	_movement_mode.jump(entity);
