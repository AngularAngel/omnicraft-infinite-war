extends BaseInternalComponent

class_name BaseStorageComponent


@export var _resource_name: String;
@export var _amount := 500.0;

@onready var _storage = $Stats/Resources/Storage as ResourceContainer


func _ready():
	super._ready();
	var parent = get_parent();
	_storage._maximum_resource.set_base_score(_amount);
	if parent.has_method("add_storage_component"):
		parent.add_storage_component(self);


func get_current() -> float:
	return _storage._current_resource.get_current_score();
	
	
func get_maximum() -> float:
	return _storage._maximum_resource.get_current_score();
	
	
func get_free_capacity() -> float:
	return get_maximum() - get_current();


func receive(quantity: float) -> float:
	var capacity = get_free_capacity();
	
	if quantity > capacity:
		_storage._current_resource.set_base_score(get_maximum());
		return quantity - capacity;
	else:
		_storage._current_resource.modify_base_score(quantity)
		return 0;


func subtract(quantity: float) -> float:
	var current = get_current();
	
	if quantity > current:
		_storage._current_resource.set_base_score(0);
		return quantity - current;
	else:
		_storage._current_resource.modify_base_score(-quantity)
		return 0;
