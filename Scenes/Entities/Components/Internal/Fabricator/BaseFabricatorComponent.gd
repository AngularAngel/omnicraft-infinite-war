extends BaseInternalComponent

class_name BaseFabricatorComponent


@onready var _progress = $Stats/Resources/Progress as ResourceContainer;
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat
@onready var _mass_cost = $"Stats/Mass Cost" as PrimaryStat


var _requirement_callback;
var _fabrication_callback;


# Called when the node enters the scene tree for the first time.
func _ready():
	_progress.full.connect(fabricate)


func _physics_process(delta):
	if _requirement_callback.call():
		var progress = min(_progress._maximum_resource.get_current_score() - _progress._current_resource.get_current_score(), 
					   _progress._regen_resource.get_current_score() * delta);
		if progress > 0:
			var energy_expenditure = _energy_cost.get_current_score() * progress;
			if get_parent().get_parent().get_commodity_amount("Energy") >= energy_expenditure:
				var mass_expediture = _mass_cost.get_current_score() * progress;
				if get_parent().get_parent().get_commodity_amount("Mass") >= mass_expediture:
					get_parent().get_parent().subtract_commodity("Energy", energy_expenditure);
					get_parent().get_parent().subtract_commodity("Mass", mass_expediture);
					_progress._current_resource.modify_base_score(progress)


func fabricate():
	_fabrication_callback.call();
	_progress._current_resource.modify_base_score(-1.0);
