extends Node

class_name BaseInternalComponent


enum ResourceType {RESOURCE, COMMODITY, DEFENSE};


signal component_destroyed;


@onready var _stats = $Stats as StatContainer
@onready var _integrity = _stats.get_stat("Current Integrity") as PrimaryStat


var _ref_count = RefCounted.new();


func _ready():
	_ref_count.init_ref();
	set_meta("isComponent", true)


func is_destroyed() -> bool:
	return _integrity.get_current_score() <= 0.0;


func take_damage(damage : float) -> bool:
	if damage <= 0:
		return false;
	
	_integrity.modify_base_score(damage);
	if is_destroyed():
		destroy()
	return true;


func destroy():
	component_destroyed.emit()
	var parent = get_parent();
	if parent != null:
		parent.remove_child(self)


func reference():
	_ref_count.reference();


func unreference():
	_ref_count.unreference()
	if _ref_count.get_reference_count() <= 1 and is_destroyed():
		queue_free();


func get_unit():
	return get_parent().get_unit();


func get_faction():
	return get_unit().get_faction();
