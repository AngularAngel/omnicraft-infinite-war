extends BaseInternalComponent

class_name BaseGeneratorComponent


@export var _resource_name: String
@export var _resource_type := ResourceType.COMMODITY;


@onready var _generation_amount: PrimaryStat = $"Stats/Generation Amount"
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat

# Called when the node enters the scene tree for the first time.
func _ready():
	super._ready()


func _physics_process(delta):
	match _resource_type:
		ResourceType.RESOURCE:
			var resource = get_parent()._stats.get_stat(_resource_name);
			if resource._current_resource.get_current_score() >= resource._maximum_resource.get_current_score():
				return;
		
		ResourceType.DEFENSE:
			var resource = get_parent().get_node(_resource_name)._stats.get_stat("Health");
			if resource._current_resource.get_current_score() >= resource._maximum_resource.get_current_score():
				return;
		
		ResourceType.COMMODITY:
			if get_parent().get_commodity_free_capacity(_resource_name) <= 0:
				return;
	var amount = _generation_amount.get_current_score() * delta;
	var expenditure = _energy_cost.get_current_score() * amount;
	if get_parent().get_commodity_amount("Energy") >= expenditure:
		get_parent().subtract_commodity("Energy", expenditure);
		
		match _resource_type:
			ResourceType.RESOURCE:
				get_parent()._stats.get_stat(_resource_name)._current_resource.modify_base_score(amount);
				
			ResourceType.DEFENSE:
				get_parent().get_node(_resource_name)._stats.get_stat("CurrentHealth").modify_base_score(amount);
			
			ResourceType.COMMODITY:
				get_parent().receive_commodity(_resource_name, amount);
