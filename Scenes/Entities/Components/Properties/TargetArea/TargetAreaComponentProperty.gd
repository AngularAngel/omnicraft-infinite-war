extends BaseComponentProperty

class_name TargetAreaComponentProperty;


signal added_target(target);
signal lost_target(target);


@onready var _area = $Area as Area3D;
@onready var _sphere = $Area/Sphere as CollisionShape3D;
@onready var _range = $Stats/Range as PrimaryStat
@onready var _range_indicator: MeshInstance3D = $Area/RangeIndicator
@onready var _intersection: MeshInstance3D = $Area/RangeIndicator/Intersection


var _targets := [];
var _target_condition: Callable = func(_target): return true;


func _ready() -> void:
	update_range();
	_range.changed.connect(
		func():
			update_range();
	);
	
	_area.body_entered.connect(target_entered);
	_area.body_exited.connect(target_exited);


func update_range():
	_area.scale = Vector3(1, 1, 1) * _range.get_current_score();
	_intersection.set_instance_shader_parameter("sphere_radius", _range.get_current_score());


func _physics_process(_delta: float) -> void:
	_area.set_position(get_global_position());


func target_entered(target):
	if target != self and _target_condition.call(target) and not _targets.has(target):
		_targets.append(target);
		added_target.emit(target);


func target_exited(target):
	if _targets.has(target):
		_targets.erase(target);
		lost_target.emit(target);
