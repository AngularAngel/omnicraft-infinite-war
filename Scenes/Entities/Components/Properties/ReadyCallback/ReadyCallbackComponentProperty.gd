extends BaseComponentProperty

class_name ReadyCallbackComponentProperty


@export var _callback: Callable;


func _ready():
	_callback.call(self);
