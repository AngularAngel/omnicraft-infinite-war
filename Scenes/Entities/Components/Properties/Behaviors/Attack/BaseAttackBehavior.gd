extends BaseBehavior

class_name BaseAttackBehavior


@onready var _weapon = get_parent().get_parent().get_node("Chassis/Weapon") as BaseWeaponComponent;


var _target : set = set_target;


func _ready():
	super._ready();
	
	await _controller.ready;
	
	_controller._raycast.add_exception(_weapon);
	_weapon.reference();
	_controller._raycast.target_position.z = -_weapon._range.get_current_score();


func _physics_process(_delta):
	super._physics_process(_delta);
	
	if is_instance_valid(_target):
		var entity = get_parent().get_parent();
		if not is_instance_valid(entity):
			return;
		
		var target_pos = _target.get_global_position();
		
		var target_direction = entity.get_global_position().direction_to(target_pos);
		if not target_direction.is_zero_approx():
			if _controller._raycast.global_position == target_pos:
				print("Target_Pos: %s" % target_pos);
			else:
				_controller._raycast.look_at(target_pos, Vector3.UP);
		entity.rotation.y = _controller._raycast.global_rotation.y;
		_controller._raycast.collision_mask = 12;
		_controller._raycast.force_raycast_update();
		var collision_object = _controller._raycast.get_collider();
		if is_instance_valid(collision_object) and is_instance_valid(_weapon) and collision_object.has_method("take_damage") and \
			collision_object.has_method("get_faction") and collision_object.get_faction() != entity.get_faction():
			if not _weapon._is_firing:
				_weapon.begin_firing();
		elif _weapon._is_firing:
				_weapon.finish_firing();
	else:
		_target = null;


func set_target(target):
	_target = target;
