extends BaseComponentProperty

class_name BaseBehavior


@onready var _controller: BaseController = get_parent();


func _ready() -> void:
	pass;


func _physics_process(_delta: float) -> void:
	pass;
