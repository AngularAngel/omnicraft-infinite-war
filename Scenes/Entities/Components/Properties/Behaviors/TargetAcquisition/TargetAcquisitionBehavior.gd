extends BaseBehavior

class_name TargetAcquisitionBehavior


signal target_acquired(target)


@onready var _target_area = $TargetArea as TargetAreaComponentProperty;
@onready var _range_indicator: RangeIndicator = $TargetArea/Area/RangeIndicator
@onready var _timer = $Timer as Timer;
@onready var _acquisition_speed = $Stats/AcquisitionSpeed as PrimaryStat


var _target_priority_function: Callable = func closest(this: Node3D, target: Node3D):
	return _target_area._range.get_current_score() - this.get_global_position().distance_to(target.get_global_position());


func _ready():
	_timer.wait_time = 1.0 / _acquisition_speed.get_current_score();
	_acquisition_speed.changed.connect(
		func():
			_timer.wait_time = 1.0 / _acquisition_speed.get_current_score();
	);
	
	_timer.timeout.connect(acquire_target);
	_timer.start();


func show_range_for_faction(faction: Faction):
	if get_parent().get_faction() == faction:
		_range_indicator.set_visible(true);


func hide_range():
	_range_indicator.set_visible(false);


func set_range(range):
	_target_area._range.set_base_score(range);


func acquire_target():
	if _target_area._targets.size() > 0:
		var current_priority = _target_priority_function.call(self, _target_area._targets[0])
		var current_target = _target_area._targets[0];
		for target in _target_area._targets:
			if target != current_target:
				var target_priority = _target_priority_function.call(self, target);
				if target_priority > current_priority:
					current_target = target;
					current_priority = target_priority;
		if current_priority >= 0.0:
			target_acquired.emit(current_target);
		else:
			target_acquired.emit(null);
