extends BaseBehavior

class_name BaseEngineeringBehavior


@onready var _construction_beam = get_parent().get_parent().get_node("Chassis/ConstructionBeam") as ConstructionBeam;


var _target : set = set_target;


func _ready():
	super._ready();
	
	await _controller.ready;
	
	_controller._raycast.add_exception(_construction_beam);
	_construction_beam.reference();
	_controller._raycast.target_position.z = -_construction_beam._range.get_current_score();


func _physics_process(_delta):
	super._physics_process(_delta);
	
	if is_instance_valid(_target):
		var entity = get_parent().get_parent();
		if not is_instance_valid(entity):
			return;
		
		var target_pos = _target.get_global_position();
		
		var target_direction = entity.get_global_position().direction_to(target_pos);
		if not target_direction.is_zero_approx():
			if _controller._raycast.global_position == target_pos:
				print("Target_Pos: %s" % target_pos);
			else:
				_controller._raycast.look_at(target_pos, Vector3.UP);
		_controller._raycast.collision_mask = 12;
		_controller._raycast.force_raycast_update();
		var collision_object = _controller._raycast.get_collider();
		if is_instance_valid(collision_object) and is_instance_valid(_construction_beam) and \
			collision_object.has_method("get_faction") and collision_object.get_faction() == entity.get_faction():
			_construction_beam.construct(_delta);
	else:
		if _construction_beam._is_constructing:
			_construction_beam.stop_constructing();
		_target = null;


func set_target(target):
	_target = target;
