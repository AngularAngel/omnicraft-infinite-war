extends BaseBehavior

class_name BaseMiningBehavior


@onready var _collector = get_parent().get_parent().get_node("Chassis/MassCollector") as MassCollector;


func _ready():
	super._ready();
	
	await _controller.ready;
	
	_controller._raycast.collision_mask = 9;
	_controller.rotation_degrees.x = 330;
	_controller._raycast.add_exception(_collector);
	_collector.reference();
	_controller._raycast.target_position.z = -_collector._range.get_current_score();
	_collector._energy_beam.fade_in(0.05);
	_collector._hide_energy_beam = false;


func _physics_process(_delta):
	super._physics_process(_delta);
	var collision_object = _controller._raycast.get_collider();
	if is_instance_valid(collision_object) and collision_object.get_meta("isTerrain", false):
		var hit := _controller._get_pointed_voxel(_collector._range.get_current_score(), 2.5);
	
		if hit != null:
			var hit_voxel_data = _controller._get_hit_voxel_data(hit.position)
			var hit_voxel = hit_voxel_data[1];
			var voxel_mass = hit_voxel.get_meta("mass", 1.0);
			if voxel_mass > 1.0 and hit_voxel.get_meta("energyCost", 1.0) < 10.0:
				if not _collector._is_collecting and _collector.get_parent().get_commodity_free_capacity("Mass") >= voxel_mass:
						_collector.start_collecting();
				return;
		
		_collector.stop_collecting();
	_controller.rotation_degrees.y += 60 * _delta;
	if _controller.rotation_degrees.y >= 360:
		_controller.rotation_degrees.y -= 360;
		_controller.rotation_degrees.x += 5;
		if _controller.rotation_degrees.x >= 400:
			_controller.rotation_degrees.x -= 70;
