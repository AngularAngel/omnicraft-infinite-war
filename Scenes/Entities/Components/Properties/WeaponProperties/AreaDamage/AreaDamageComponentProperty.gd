extends BaseComponentProperty

class_name AreaDamageComponentProperty


@onready var _damage_stats: DamageStats = $Stats/DamageStats


func _ready() -> void:
	get_parent().hit.connect(on_hit);


func on_hit(_area_effect, target):
	var damage = _damage_stats.get_damage(self, DamageInstance.DAMAGE_SOURCE.AREA);
	damage._raw_quantity *= get_physics_process_delta_time();
	if target.has_method("take_damage"):
			target.take_damage(damage);
