extends BaseComponentProperty

class_name ChargedShotComponentProperty


@onready var _charge := $"Stats/Charge" as ResourceContainer;
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat
@onready var _upkeep_cost = $"Stats/Upkeep Cost" as PrimaryStat
@onready var _base_damage_multiplier = $"Stats/Base Damage Multiplier" as PrimaryStat
@onready var _charge_damage_multiplier = $"Stats/Charge Damage Multiplier" as PrimaryStat


var _is_charging := false;


func _ready() -> void:
	await get_parent().ready;
	get_parent()._damage_stats._damage_modifiers.append(modify_damage);
	get_parent().firing_begun.connect(begin_firing);
	get_parent().firing_finished.connect(finish_firing);
	get_parent()._fire_conditions.append(allow_firing);


func modify_damage(damage: DamageInstance):
	damage._raw_quantity *= get_damage_multiplier();


func get_damage_multiplier():
	return _base_damage_multiplier.get_current_score() + \
							_charge.get_current_percent() * \
							_charge_damage_multiplier.get_current_score();


func _physics_process(delta: float) -> void:
	if _is_charging:
		var cur_charge = _charge._current_resource.get_current_score();
		var charge_increase = min(
			_charge._maximum_resource.get_current_score() - cur_charge, 
			_charge._regen_resource.get_current_score() * delta
		);
		
		if charge_increase > 0 or (_upkeep_cost.get_current_score() > 0 and cur_charge > 0):
			var energy_expenditure = _energy_cost.get_current_score() * charge_increase + \
									 _upkeep_cost.get_current_score() * cur_charge * delta;
			if get_parent().get_chassis().get_commodity_capacity("Energy") > energy_expenditure:
				get_parent().get_chassis().subtract_commodity("Energy", energy_expenditure);
				_charge._current_resource.modify_base_score(charge_increase);


func begin_firing():
	_is_charging = true;


func finish_firing():
	if _is_charging:
		_is_charging = false;
		get_parent().fire();
		_charge._current_resource.set_base_score(0);


func allow_firing() -> bool:
	return not _is_charging;
