extends BaseComponentProperty


@onready var _audio := $Audio as AudioStreamPlayer3D;


func _ready() -> void:
	get_parent().before_firing.connect(before_firing);


func before_firing():
	_audio.play();
