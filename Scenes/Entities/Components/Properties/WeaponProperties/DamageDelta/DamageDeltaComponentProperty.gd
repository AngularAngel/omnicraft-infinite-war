extends BaseComponentProperty

class_name DamageDeltaComponentProperty


func _ready():
	await get_parent().ready;
	get_parent()._damage_stats._damage_modifiers.append(modify_damage)


func modify_damage(damage: DamageInstance):
	damage._raw_quantity *= get_physics_process_delta_time();
