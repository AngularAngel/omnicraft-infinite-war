extends BaseComponentProperty

class_name ProjectileDetonationComponentProperty


@export var _detonation_scene = null;


@onready var _lifetime_base: PrimaryStat = $Stats/LifetimeBase
@onready var _lifetime_mult: PrimaryStat = $Stats/LifetimeMult


func _ready() -> void:
	get_parent().fired.connect(
		func(_bullet_property, bullet: Bullet):
			bullet.hit.connect(self.on_hit);
	)


func on_hit(projectile, _target):
	var detonation = _detonation_scene.instantiate();
	detonation.get_node("TargetArea/Area").collision_mask = 4;
	var timer = Timer.new()
	detonation.add_child(timer);
	var charge_percent = projectile.get_meta("chargePercent", 0.0)
	detonation.set_meta("chargePercent", charge_percent);
	var duration = _lifetime_base.get_current_score() + _lifetime_mult.get_current_score() * charge_percent;
	detonation.set_meta("duration", duration);
	timer.wait_time = duration;
	var explosion_effect = detonation.get_node("ExplosionEffect");
	if explosion_effect != null:
		explosion_effect.ready.connect(
			func():
				explosion_effect.multiply_lifetimes(duration);
		);
	timer.autostart = true;
	timer.timeout.connect(
		func():
			detonation.queue_free()
	);
	
	detonation.set_position(projectile.get_global_position());
	
	get_node("/root/Main/Game/Attacks").add_child(detonation);
	var multiplier = projectile.get_meta("damageMultiplier", 1.0);
	detonation.get_node("AreaDamageComponentProperty")._damage_stats._damage_modifiers.append(
		func(damage):
			damage._raw_quantity *= multiplier;
	)
	
	projectile.queue_free();
	
