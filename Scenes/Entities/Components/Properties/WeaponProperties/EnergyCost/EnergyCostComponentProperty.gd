extends BaseComponentProperty

class_name EnergyCostComponentProperty


@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat


func _ready() -> void:
	get_parent().before_firing.connect(before_firing);
	get_parent()._fire_conditions.append(allow_firing)


func allow_firing():
	var energy_expenditure = _energy_cost.get_current_score() * get_physics_process_delta_time();
	return get_parent().get_chassis().get_commodity_amount("Energy") >= energy_expenditure;


func before_firing():
	var energy_expenditure = _energy_cost.get_current_score() * get_physics_process_delta_time();
	get_parent().get_chassis().subtract_commodity("Energy", energy_expenditure);
