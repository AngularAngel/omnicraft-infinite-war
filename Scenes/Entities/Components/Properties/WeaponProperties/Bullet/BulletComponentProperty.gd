extends BaseComponentProperty

class_name BulletComponentProperty


signal fired(this, bullet);


const BULLET_SCENE = preload("res://Scenes/Entities/Ephemeral/Bullet/Bullet.tscn");
const ENERGY_GLOW_SCENE = preload("res://Scenes/Detritus/Effects/EnergyGlow/EnergyGlow.tscn");

@export var _projectile_scene = null;


@onready var _gravity_mult: PrimaryStat = $Stats/GravityMult
@onready var _speed_base: PrimaryStat = $Stats/SpeedBase
@onready var _speed_mult: PrimaryStat = $Stats/SpeedMult
@onready var _lifetime_base: PrimaryStat = $Stats/LifetimeBase
@onready var _lifetime_mult: PrimaryStat = $Stats/LifetimeMult


var _color := Color(0.95, 0.5, 0.5)
var _destroy_projectile := true;


func _ready() -> void:
	get_parent()._use_hitscan = false;
	get_parent().fired.connect(on_fired);


func on_fired(damage: DamageInstance):
	var projectile := _projectile_scene.instantiate() as Bullet;
	
	projectile._gravity_mult = _gravity_mult.get_current_score();
	projectile.set_position(get_global_position());
	get_node("/root/Main/Game/Attacks").add_child(projectile);
	projectile.look_at(get_parent()._controller._target_point.get_global_position());
		
	var timer = Timer.new()
	timer.wait_time = _lifetime_base.get_current_score() + _lifetime_mult.get_current_score() * projectile.get_meta("chargePercent", 0.0);
	timer.autostart = true;
	timer.timeout.connect(
		func():
			print("???")
			projectile.queue_free()
	);
	projectile.add_child(timer);
	
	if not projectile.is_node_ready():
		await projectile.ready;
		
	projectile._velocity.z = -(_speed_base.get_current_score() + _speed_mult.get_current_score() * projectile.get_meta("chargePercent", 0.0));
	damage._source = projectile._raycast;
	
	projectile._raycast.add_exception(get_parent());
	projectile._raycast.add_exception(get_parent().get_parent());
	
	fired.emit(self, projectile);
	
	projectile.hit.connect(
		func(projectile: Bullet, collision_object):
			if collision_object.has_method("take_damage"):
				collision_object.take_damage(damage);
			if _destroy_projectile:
				projectile.queue_free();
	);
