extends BaseComponentProperty

class_name ChargeBarComponentProperty


const RESOURCE_BOX = preload("res://Scenes/GUI/HUD/ResourceBox/ResourceBox.tscn");


func _ready() -> void:
	var charge_bar = RESOURCE_BOX.instantiate() as ResourceBox;
	charge_bar._resource_name = "Charge";
	
	charge_bar.get_node("ResourceScore").hide();
	
	charge_bar.anchor_left = 0.5;
	charge_bar.anchor_right = 0.5;
	charge_bar.anchor_top = 0.9;
	charge_bar.anchor_bottom = 0.9;
	
	charge_bar.offset_left = -70;
	charge_bar.offset_right = 70;
	charge_bar.offset_top = -20;
	charge_bar.offset_bottom = 0;
	
	charge_bar.grow_horizontal = Control.GROW_DIRECTION_BOTH;
	charge_bar.grow_vertical = Control.GROW_DIRECTION_BEGIN;
	
	var parent = get_parent();
	
	if not parent.is_node_ready():
		await parent.ready;
	
	charge_bar.set_resource(parent._charge);
	
	var weapon = parent.get_parent();
	
	if not weapon.is_node_ready():
		await weapon.ready;
	
	weapon._controller.get_node("HUD").add_child(charge_bar);
