extends BaseComponentProperty

class_name BulletHoleComponentProperty


const BULLET_HOLE = preload("res://Scenes/Detritus/BulletHole.tscn")


func _ready():
	await get_parent().ready;
	get_parent()._damage_stats._damage_modifiers.append(modify_damage)


func modify_damage(damage: DamageInstance):
	damage.damaged_defense.connect(on_dealing_damage.bind(damage));
	damage.damaged_terrain.connect(leave_bullet_hole.bind(damage));


func on_dealing_damage(defense_name, _amount, damage: DamageInstance):
	if defense_name != "Shield":
		leave_bullet_hole(damage);


func leave_bullet_hole(damage: DamageInstance):
	if damage.source_type != DamageInstance.DAMAGE_SOURCE.RAYCAST:
		return;
	var raycast := damage.source as RayCast3D;
	var collision_object = raycast.get_collider() as Node;
	
	if collision_object.is_inside_tree():
		var collision_point = raycast.get_collision_point();
		var collision_normal = raycast.get_collision_normal();
		
		var bulletInst = BULLET_HOLE.instantiate() as Node3D
		collision_object.add_child(bulletInst)
		
		bulletInst.transform.origin = collision_point - collision_object.global_transform.origin as Vector3
		
		if collision_normal != Vector3.BACK:
			bulletInst.look_at((collision_point + collision_normal), Vector3.BACK)
	
