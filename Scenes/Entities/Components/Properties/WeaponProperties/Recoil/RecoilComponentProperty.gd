extends BaseComponentProperty

class_name RecoilComponentProperty


@onready var _inaccuracy := $Stats/Inaccuracy as PrimaryStat;
@onready var _recoil := $"Stats/Recoil" as ResourceContainer;
@onready var _fire_recoil := $"Stats/Recoil/Fire Recoil" as PrimaryStat;
@onready var _camera_recoil := $"Stats/Recoil/Camera Recoil Multiplier" as PrimaryStat;


func _ready() -> void:
	get_parent().before_firing.connect(before_firing);
	get_parent().after_firing.connect(after_firing);


func _physics_process(delta):
	if get_parent()._controller.get_meta("isPlayerController", false) :
		get_parent()._controller.scale_crosshair(_recoil._current_resource.get_current_score());
	
	_recoil._current_resource.set_base_score(lerp(_recoil._current_resource.get_current_score(),
												  1.0,
												  _recoil._regen_resource.get_current_score() * delta));


func before_firing():
	var raycast = get_parent()._controller._raycast;
	
	var spread = _inaccuracy.get_current_score() * _recoil._current_resource.get_current_score();
		
	_recoil._current_resource.set_base_score(lerp(_recoil._current_resource.get_current_score(),
												  _recoil._maximum_resource.get_current_score(),
												  _fire_recoil.get_current_score()));
	
	var random_spread = Vector3(randf_range(-1, 1), randf_range(-1, 1), 0).normalized() * spread * randf();
	
	raycast.rotation_degrees = random_spread;
	raycast.force_raycast_update();


func after_firing():
	get_parent()._controller._raycast.rotation_degrees = Vector3();
	if get_parent()._controller.get_meta("isPlayerController", false) :
		get_parent()._controller._camera.apply_recoil(get_camera_recoil());


func get_camera_recoil():
	return _camera_recoil.get_current_score() * _recoil._current_resource.get_current_score();
