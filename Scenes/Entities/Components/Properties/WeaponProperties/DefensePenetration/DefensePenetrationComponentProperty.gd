extends BaseComponentProperty

class_name DefensePenetrationComponentProperty


@export var _defense_name := "Integrity";


@onready var _damage_multiplier: PrimaryStat = $Stats/DamageMultiplier;
@onready var _flat_pen: PrimaryStat = $Stats/FlatPen;
@onready var _pen_multiplier: PrimaryStat = $Stats/PenMultiplier;


func _ready() -> void:
	await get_parent().ready;
	get_parent()._damage_stats._damage_modifiers.append(modify_damage);


func modify_damage(damage: DamageInstance):
	damage._defense_penetrations[_defense_name] = get_defense_penetration();


func get_defense_penetration():
	var defense_penetration = DefensePenetration.new();
	
	defense_penetration._damage_multiplier = _damage_multiplier.get_current_score();
	defense_penetration._flat_pen = _flat_pen.get_current_score();
	defense_penetration._pen_multiplier = _pen_multiplier.get_current_score();
	
	return defense_penetration;
