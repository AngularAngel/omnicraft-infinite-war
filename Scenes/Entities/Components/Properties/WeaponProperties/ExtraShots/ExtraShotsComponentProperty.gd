extends BaseComponentProperty

class_name MultipleShotComponentProperty


@onready var _extra_shots: PrimaryStat = $Stats/ExtraShots


var _fire_extra := false;


func _ready() -> void:
	get_parent().firing_begun.connect(on_firing_begun);
	get_parent().after_firing.connect(fire_extra_shots);


func on_firing_begun():
	_fire_extra = true;


func fire_extra_shots():
	if _fire_extra:
		_fire_extra = false;
		var shots = _extra_shots.get_current_score();
		for i in range(shots):
			get_parent().fire();
