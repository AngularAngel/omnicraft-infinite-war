extends BaseComponentProperty

class_name ChargeGlowComponentProperty


@onready var _light_intensity = $"Stats/Light Intensity" as PrimaryStat
@onready var _charge_glow = $ChargeGlow as EnergyGlow;


var _color := Color(0.95, 0.5, 0.5)


func _ready() -> void:
	_charge_glow._light_intensity = _light_intensity.get_current_score();
	_charge_glow._color = _color;
	_light_intensity.changed.connect(
		func():
			_charge_glow._light_intensity = _light_intensity.get_current_score();
	);


func _physics_process(_delta: float) -> void:
	var parent = get_parent();
	if parent._is_charging:
		visible = true;
		_charge_glow.set_color(parent._charge.get_current_percent());
	else:
		visible = false;
