extends BaseComponentProperty

class_name EnergyBeamComponentProperty;


@onready var _energy_beam = $EnergyBeam as EnergyBeam;

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var parent = get_parent();
	if not parent.is_node_ready():
		await parent.ready;
	
	if not parent._controller.is_node_ready():
		await parent._controller.ready;
	
	parent._raycast = $EnergyBeam/BeamRay;
	
	parent._raycast.add_exception(parent.get_parent());
	_energy_beam._color = parent.get_faction()._energy_color;
	
	_energy_beam._target = parent._controller._target_point;
	_energy_beam._beam_ray.target_position.z = -parent._range.get_current_score();
	parent.firing_begun.connect(begin_firing);
	parent.firing_finished.connect(finish_firing);


func begin_firing():
	_energy_beam.fade_in(0.25)


func finish_firing():
	_energy_beam.fade_out(0.5, false, false)
