extends BaseComponentProperty


@onready var _ammo := $"Stats/Ammo" as ResourceContainer;


func _ready() -> void:
	get_parent().after_firing.connect(after_firing);
	get_parent()._fire_conditions.append(allow_firing)
	var fabricator = get_parent().get_node("Ammo Fabricator");
	if fabricator != null:
		fabricator._requirement_callback = requires_fabrication;
		fabricator._fabrication_callback = fabricate;


func allow_firing() -> bool:
	return _ammo._current_resource.get_current_score() >= 1.0;


func after_firing():
	_ammo._current_resource.modify_base_score(-1);


func requires_fabrication():
	return _ammo._current_resource.get_current_score() < _ammo._maximum_resource.get_current_score();


func fabricate():
	_ammo._current_resource.modify_base_score(1)
