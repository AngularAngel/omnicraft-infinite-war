extends BaseComponentProperty

class_name ReloadingComponentProperty


@onready var _progress := $"Stats/Progress" as ResourceContainer;
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat


func _ready() -> void:
	get_parent().after_firing.connect(after_firing);
	get_parent()._fire_conditions.append(allow_firing)


func _physics_process(delta):
	var progress = min(_progress._maximum_resource.get_current_score() - _progress._current_resource.get_current_score(), 
					   _progress._regen_resource.get_current_score() * delta);
	
	if progress > 0:
		var energy_expenditure = _energy_cost.get_current_score() * progress;
		if get_parent().get_chassis().get_commodity_capacity("Energy") > energy_expenditure:
			get_parent().get_chassis().subtract_commodity("Energy", energy_expenditure);
			_progress._current_resource.modify_base_score(progress);


func allow_firing() -> bool:
	return _progress._current_resource.get_current_score() >= 1.0;


func after_firing():
	_progress._current_resource.modify_base_score(-1);
