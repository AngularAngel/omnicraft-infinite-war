extends StatContainer

class_name DamageStats


var _damage_modifiers := [];


func get_damage(source, source_type := DamageInstance.DAMAGE_SOURCE.RAYCAST) -> DamageInstance:
	var damage := DamageInstance.new()
	
	damage.source = source;
	damage.source_type = source_type;
	damage._raw_quantity = $Damage.get_current_score();
	
	modify_damage(damage);
	
	return damage;


func modify_damage(damage):
	for damage_modifier in _damage_modifiers:
		damage_modifier.call(damage);
