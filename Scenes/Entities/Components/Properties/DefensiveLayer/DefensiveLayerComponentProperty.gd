extends BaseComponentProperty

class_name DefensiveLayerComponentProperty


signal before_damaged(amount, damage_instance)
signal after_damaged(amount, damage_instance)


@export var _defense_name := "Integrity";


@onready var _health = $Stats/Resources/Health as ResourceContainer;

@onready var _flat_damage_reduction: PrimaryStat = $Stats/FlatDamageReduction
@onready var _damage_divisor: PrimaryStat = $Stats/DamageDivisor

@onready var _flat_pen_reduction: PrimaryStat = $Stats/FlatPenReduction
@onready var _pen_reduction_multiplier: PrimaryStat = $Stats/PenReductionMultiplier
@onready var _repair_speed_multiplier: PrimaryStat = $Stats/RepairSpeedMultiplier
@onready var _repair_mass_cost: PrimaryStat = $Stats/RepairMassCost
@onready var _repair_energy_cost: PrimaryStat = $Stats/RepairEnergyCost


@export var _priority := -1;



func _ready():
	get_parent().add_defense_layer(self);


func take_damage(damage: DamageInstance) -> bool:
	var damage_quantity = damage.get_damage_against(_defense_name);
	damage_quantity -= _flat_damage_reduction.get_current_score();
	damage_quantity /= _damage_divisor.get_current_score();
	damage_quantity = min(damage_quantity, get_health());
	
	
	if damage_quantity > 0:
		var damage_reduction = get_health();
		damage_reduction *= _pen_reduction_multiplier.get_current_score();
		damage_reduction += min(_flat_pen_reduction.get_current_score(), get_health());
	
		#print("%s: %s, %s, %s" % [_defense_name, _health._current_resource.get_current_score(), damage_quantity, damage_reduction])
		before_damaged.emit(damage_quantity, damage);
		_health._current_resource.modify_base_score_with_min(-damage_quantity);
		after_damaged.emit(damage_quantity, damage);
		
		damage.damaged_defense.emit(_defense_name, damage_quantity);
		damage.reduce_damage_from(_defense_name, damage_reduction);
	
	return true;


func get_health():
	return _health._current_resource.get_current_score();
