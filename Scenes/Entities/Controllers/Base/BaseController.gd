extends Node3D

class_name BaseController


@onready var _target_point = $TargetPoint as Node3D;


var _direction : Vector3;

var _raycast: RayCast3D = null;
var _terrain: VoxelTerrain = null;
var _terrain_tool: VoxelTool = null;
var _voxel_library: VoxelBlockyTypeLibrary = null;


func _ready():
	var entity = get_parent();
	
	var game = entity.get_parent().get_parent();
	
	_terrain = game.get_node("Terrain");
	
	if not game.is_node_ready():
		await game.ready;
	
	_voxel_library = game._voxels._voxel_library;
	
	_terrain_tool = _terrain.get_voxel_tool();
	_terrain_tool.channel = VoxelBuffer.CHANNEL_TYPE;


func _physics_process(_delta: float) -> void:
	if _raycast != null:
		_target_point.set_position(_raycast.get_collision_point());


func get_direction() -> Vector3:
	return _direction;


func _get_hit_voxel_data(voxel_position: Vector3i):
	var voxel_id := _terrain_tool.get_voxel(voxel_position)
	while voxel_id > 0 and voxel_id <= 3:
		match (voxel_id):
			1:
				voxel_position.x -= 1;
			2:
				voxel_position.y -= 1;
			3:
				voxel_position.z -= 1;
		
		voxel_id = _terrain_tool.get_voxel(voxel_position);
	var voxel_data = _voxel_library.get_type_name_and_attributes_from_model_index(voxel_id);
	var voxel_type := _voxel_library.get_type_from_name(voxel_data[0])
	return [voxel_id, voxel_type, voxel_position]


func _get_pointed_voxel(ray_range, offset := 0.0) -> VoxelRaycastResult:
	var origin = _raycast.get_global_transform().origin;
	var forward = -_raycast.get_global_transform().basis.z.normalized();
	origin += forward * offset;
	var hit := _terrain_tool.raycast(origin, forward, ray_range, 1);
	return hit;


func get_faction():
	return get_parent().get_faction();
