extends BaseController

class_name PlayerController


signal mouse_captured
signal mouse_released


@onready var _chassis = get_parent().get_node("Chassis") as BaseChassisComponent;
@onready var _weapon = _chassis.get_node("Weapon") as BaseWeaponComponent;
@onready var _construction_beam = _chassis.get_node("ConstructionBeam") as ConstructionBeam;
@onready var _collector = _chassis.get_node("MassCollector") as MassCollector;
@onready var _motivator = _chassis.get_node("Motivator") as BaseMotivatorComponent;
@onready var _camera = $Camera as Camera3D
@onready var _building_preview: BuildingPreview;


var _units: Units = null;
var _entity_voxel = 4;
var _mouseSensibility = 600
var _construction_site = null;
var _target = null;
var _block_outline : MeshInstance3D = null;
var _red_material = StandardMaterial3D.new();


func _ready():
	super._ready();
	
	_units = get_parent().get_parent().get_parent().get_node("Units");
	_raycast = $Camera/RayCast;
	capture_mouse()
	_raycast.add_exception(get_parent())
	_raycast.add_exception(_chassis)
	_raycast.add_exception(_weapon)
	_raycast.add_exception(_motivator)
	_weapon.reference();
	_motivator.reference();
	set_meta("isPlayerController", true);
	#_chassis.set_visible(false);
	
	var mesh := Util.create_wirecube_mesh(Color(0,0,0));
	_block_outline = MeshInstance3D.new();
	_block_outline.mesh = mesh;
	_block_outline.set_scale(Vector3(1,1,1) * 1.01);
	_terrain.add_child(_block_outline);
	_red_material.albedo_color = Color(1, 0, 0);


func capture_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED;
	mouse_captured.emit();


func release_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE;
	mouse_released.emit();


func _process(delta: float) -> void:
	if not DisplayServer.window_is_focused():
		$HUD.toggle_pause();


func _physics_process(_delta):
	super._physics_process(_delta);
	
	if _terrain_tool == null:
		return;
		
	var hit := _get_pointed_voxel(10);
	
	if hit == null:
		_block_outline.hide();
		if is_instance_valid(_building_preview):
			_building_preview.hide();
	else:
		var hit_voxel_data = _get_hit_voxel_data(hit.position);
		var hit_voxel_id = hit_voxel_data[0];
		var hit_voxel = hit_voxel_data[1];
		var voxel_position = hit_voxel_data[2] 
		
		
		_block_outline.set_surface_override_material(0, null);
		_block_outline.show()
		if is_instance_valid(_building_preview):
			_building_preview.show();
		if _construction_site != null:
			var offset = (hit.previous_position - hit.position);
			if offset == Vector3i(0, 1, 0):
				voxel_position = hit.previous_position;
			else:
				voxel_position = hit.position + offset * _construction_site._dimensions;
			if global_rotation_degrees.y > -90 and global_rotation_degrees.y < 90:
				voxel_position.z -= _construction_site._dimensions.z - 1;
			if global_rotation_degrees.y > 0:
				voxel_position.x -= _construction_site._dimensions.x - 1;
			_block_outline.set_position(voxel_position);
			if is_instance_valid(_building_preview):
				_building_preview.set_position(voxel_position);
			_block_outline.set_scale(_construction_site._dimensions);
			if not _construction_beam.can_place_construction_site(_construction_site, voxel_position):
				_block_outline.set_surface_override_material(0, _red_material);
		else:
			_block_outline.set_position(voxel_position)
			if hit_voxel_id == _entity_voxel:
				var _building = _units.get_voxel_entity(voxel_position);
				_block_outline.set_scale(_building._dimensions);
			else:
				_block_outline.set_scale(hit_voxel.get_meta("selectionSize", Vector3i(1, 1, 1)))
		
		if Input.is_action_pressed("rightAction"):
			if _construction_site != null and _construction_beam.place_construction_site(_construction_site, voxel_position):
				discard_construction_site();
			elif not _construction_beam.construct(_delta) and _collector != null and not _collector._is_collecting:
				_collector.start_collecting();
		
	if Input.is_action_just_pressed("accessHUD"):
		if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
			release_mouse();
		else:
			capture_mouse();
		
	if Input.is_action_just_pressed("pause"):
		if _construction_site != null:
			discard_construction_site();
		elif Input.mouse_mode != Input.MOUSE_MODE_CAPTURED:
			capture_mouse();
		else:
			$HUD.toggle_pause();
	
	
	var entity: BaseMobileUnit = get_parent();
	
	if _motivator != null:
		if _motivator.is_destroyed():
			_motivator.unreference();
			_motivator = null;
	
	if _weapon != null:
		if _weapon.is_destroyed():
			_weapon.unreference();
			_weapon = null;
	
	if is_instance_valid(_target):
		entity.look_at(_target.get_global_position(), Vector3.UP);
		entity.rotation.x = 0;
		entity.rotation.z = 0;
	
	_direction = Vector3();
	var vertical := 0.0
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		if Input.is_action_just_pressed("toggleFlight") and _motivator != null:
			if _motivator._movement_mode.is_affected_by_gravity():
				_motivator.set_movement_mode(FlyingMovement.new())
			else:
				_motivator.set_movement_mode(WalkingMovement.new())
		
		# Handle Jump.
		if Input.is_action_pressed("jump") and _motivator != null:
			_motivator.jump(entity);
		
		# Get the input direction and handle the movement/deceleration.
		var input_dir = Input.get_vector("moveLeft", "moveRight", "moveForward", "moveBackward")
		if Input.is_action_pressed("moveUp"):
			vertical += 1.0;
		if Input.is_action_pressed("moveDown"):
			vertical += -1.0;
		_direction = (entity.transform.basis * Vector3(input_dir.x, vertical, input_dir.y)).normalized()
		
		if Input.is_action_just_pressed("leftAction") and _weapon != null:
			_weapon.begin_firing();
			
		if Input.is_action_just_released("leftAction") and _weapon != null:
			_weapon.finish_firing();
			
		if Input.is_action_just_released("rightAction"):
			if _construction_beam != null:
				_construction_beam.stop_constructing();
			if _collector != null:
				_collector.stop_collecting();


func discard_construction_site():
	_construction_site = null;
	if is_instance_valid(_building_preview):
		_building_preview.hide_range_groups();
		_building_preview.queue_free();
		_building_preview = null;


func _input(event):
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED and event is InputEventMouseMotion:
		var mouse_sensitivity = $"/root/Main"._mouse_sensitivity;
		if not is_instance_valid(_target):
			get_parent().rotation.y -= mouse_sensitivity * event.relative.x / _mouseSensibility
		rotation.x -= mouse_sensitivity * event.relative.y / _mouseSensibility
		rotation.x = clamp(rotation.x, deg_to_rad(-90), deg_to_rad(90) )


func scale_crosshair(crosshair_scale: float):
	$HUD/CrosshairCenterer/Crosshair.set_line_scale(crosshair_scale);


func get_camera_pos():
	return _camera.get_global_transform().origin;
