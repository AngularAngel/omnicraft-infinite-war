extends Camera3D


var _final_position: Vector3;
var _current_position: Vector3;

var _recoil_speed = 1.5;
var _return_speed = 4;


func _physics_process(delta: float) -> void:
	_final_position = lerp(_final_position, Vector3.ZERO, _return_speed * delta);
	_current_position = lerp(_current_position, _final_position, _recoil_speed * delta);
	rotation_degrees = _current_position;
	


func apply_recoil(recoil):
	_final_position.x += recoil;
