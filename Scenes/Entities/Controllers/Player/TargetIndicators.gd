extends TargetAreaComponentProperty

class_name TargetIndicators


const CROSSHAIR_SCENE = preload("res://Scenes/GUI/HUD/Crosshair/Crosshair.tscn")


@onready var _controller = get_parent().get_parent();
@onready var _camera: Camera3D = get_parent();
var _indicators = {}
var _viewport_center;
var _border_offset = Vector2(32, 32);
var _max_indicator_position;
var _ellipse_size;
var _primary_target;


func _ready():
	super._ready();
	_area.collision_mask = 1;
	_target_condition = func(target):
		return target.has_method("get_faction") and target.get_faction() != _controller.get_faction(); 
	added_target.connect(_on_added_target);
	lost_target.connect(_on_lost_target);
	update_viewport_center();
	get_viewport().size_changed.connect(update_viewport_center);


func update_viewport_center():
	_viewport_center = Vector2(get_viewport().size) / 2.0;
	_max_indicator_position = _viewport_center - _border_offset;
	_ellipse_size = _max_indicator_position * 0.8;


func _process(_delta: float) -> void:
	for target in _indicators:
		var target_indicator = _indicators[target];
		var target_pos = target.get_global_position();
		
		var indicator_position = _camera.unproject_position(target_pos);
		if ((indicator_position - _viewport_center) / _ellipse_size).length() > 1.0:
			if _camera.is_position_behind(target_pos):
				indicator_position = _viewport_center - ((indicator_position - _viewport_center) / _ellipse_size).normalized() * _ellipse_size;
			else:
				indicator_position = _viewport_center + ((indicator_position - _viewport_center) / _ellipse_size).normalized() * _ellipse_size;
			target_indicator.rotation = Vector2.UP.angle_to(indicator_position)
		else:
			target_indicator.rotation = 0;
		target_indicator.set_global_position(indicator_position);
		
		if not is_instance_valid(_primary_target):
			_primary_target = target;
			modify_indicator(target_indicator, true);
		elif (_controller._target == null and \
			(_indicators[_primary_target].get_global_position() - _viewport_center).length() > \
			(indicator_position - _viewport_center).length()
		):
			modify_indicator(_indicators[_primary_target]);
			_primary_target = target;
			modify_indicator(target_indicator, true);
	
	if Input.is_action_just_pressed("lockTarget"):
		if not is_instance_valid(_controller._target):
			if is_instance_valid(_primary_target):
				_controller._target = _primary_target;
		else:
			_controller._target = null;


func _on_added_target(target):
	_indicators[target] = CROSSHAIR_SCENE.instantiate();
	add_child(_indicators[target]);
	modify_indicator(_indicators[target]);


func modify_indicator(indicator, primary:= false):
	if primary:
		indicator.modulate = Color.ORANGE;
		for line in indicator._lines:
			line.show();
		indicator.set_line_scale(0.9);
	else:
		indicator.modulate = Color.RED;
		var hide_line = true;
		for line in indicator._lines:
			if hide_line:
				line.hide();
			hide_line = not hide_line;
		indicator.set_line_scale(0.75);


func _on_lost_target(target):
	_indicators[target].queue_free();
	_indicators.erase(target);
	if _primary_target == target:
		_primary_target = null;
	if _controller._target == target:
		_controller._target = null;
