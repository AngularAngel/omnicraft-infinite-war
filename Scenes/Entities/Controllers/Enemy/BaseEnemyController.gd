extends BaseController

class_name BaseEnemyController


const NAV_LAYER_WALK = 1
const NAV_LAYER_CLIMB1 = 2


@onready var _chassis = get_parent().get_node("Chassis") as BaseChassisComponent;
@onready var _weapon = _chassis.get_node("Weapon") as BaseWeaponComponent;
@onready var _motivator = _chassis.get_node("Motivator") as BaseMotivatorComponent;


var _target;
var _path: PackedVector3Array;
var _path_index = 0;

func _ready():
	super._ready();
	_raycast = $RayCast as RayCast3D
	_raycast.add_exception(get_parent());
	_raycast.add_exception(_chassis);
	_raycast.add_exception(_weapon);
	_raycast.add_exception(_motivator);
	_weapon.reference();
	_motivator.reference();
	_raycast.target_position.z = -_weapon._range.get_current_score();

func _physics_process(_delta):
	super._physics_process(_delta);
	var entity: BaseMobileUnit = get_parent();
	if _terrain_tool == null or not NavigationServer3D.map_is_active(entity.get_world_3d().navigation_map):
		return;
		
	if entity.get_global_position().distance_to(_target.get_global_position()) > 100:
		entity.queue_free();
		return;
	
	if _motivator != null:
		if _motivator.is_destroyed():
			_motivator.unreference();
			_motivator = null;
		
	if _path_index >= _path.size() - 1 or _path[-1].distance_to(_target.get_global_position()) > 5:
		find_path_to(entity);
	else:
		if entity.get_global_position().distance_to(_path[_path_index]) < 1.2:
			_path_index += 1;
		_direction = entity.get_global_position().direction_to(_path[_path_index]);
		if _direction.y > 0 and _motivator != null:
			_motivator.jump(entity);
	var target_direction = entity.get_global_position().direction_to(_target.get_global_position());
	if not target_direction.is_zero_approx():
		var target_pos = _raycast.global_position + target_direction;
		if _raycast.global_position == target_pos:
			print("Target_Pos: %s" % target_pos);
		else:
			_raycast.look_at(target_pos, Vector3.UP);
	entity.rotation.y = _raycast.global_rotation.y;
	
	var collision_object = _raycast.get_collider();
	
	_raycast.collision_mask = 12;
	
	if collision_object != null and _weapon != null and collision_object.is_inside_tree() and collision_object.has_method("take_damage") and \
		collision_object.has_method("get_faction") and collision_object.get_faction() != entity.get_faction():
		if not _weapon._is_firing:
			_weapon.begin_firing();
	elif _weapon._is_firing:
			_weapon.finish_firing();

func find_path_to(entity):
	var map = entity.get_world_3d().navigation_map
	
	var target_point = NavigationServer3D.map_get_closest_point_to_segment(map, _target.get_global_position(), _target.get_global_position() + Vector3.DOWN * 100)
	_path = NavigationServer3D.map_get_path(map, entity.get_global_position(), target_point, true)
	#print(_path.size())
	_path_index = 0;
	#_direction = entity.get_global_position().direction_to(_target.get_global_position());
