extends BaseController

class_name BaseUnitController


@onready var _chassis = get_parent().get_node("Chassis") as BaseChassisComponent;


func _ready():
	super._ready();
	_raycast = $RayCast as RayCast3D;
	_raycast.add_exception(get_parent());
	_raycast.add_exception(_chassis);


func _physics_process(delta: float) -> void:
	super._physics_process(delta);
