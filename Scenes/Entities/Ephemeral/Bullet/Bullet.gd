extends Node3D

class_name Bullet


signal hit(this, target);


@onready var _raycast: RayCast3D = $Raycast


var _velocity := Vector3(0, 0, 0);
var _gravity_mult := 1.0;

# Get the gravity from the project settings to be synced with RigidBody nodes.
var _gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
	
func add_gravity(delta):
	_velocity.y -= _gravity * delta * _gravity_mult;
	


func _physics_process(delta: float) -> void:
	add_gravity(delta);
	var prev_pos = get_global_position();
	move(delta);
	var pos = get_global_position();
	var offset = pos - prev_pos;
	look_at(pos + offset);
	_velocity.y = 0;


func move(delta):
	if _raycast.is_colliding():
		hit.emit(self, _raycast.get_collider());
	translate(_velocity * delta);
	_raycast.target_position.z = _velocity.z * delta;
	
