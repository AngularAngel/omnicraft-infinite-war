extends Node3D

class_name AreaEffect


signal hit(this, target);


@onready var _target_area: TargetAreaComponentProperty = $TargetArea


func _physics_process(_delta: float):
	for target in _target_area._targets:
		hit.emit(self, target);
