extends MovementMode

class_name FlyingMovement


func _init():
	_speed = 2.5;


func is_affected_by_gravity():
	return false;


func get_movement(entity, direction):
	if direction:
		direction.x = direction.x * entity.get_speed() * get_speed();
		direction.y = direction.y * entity.get_speed() * get_speed();
		direction.z = direction.z * entity.get_speed() * get_speed();
	else:
		direction.x = move_toward(entity.velocity.x, 0, entity.get_speed() * get_speed())
		direction.y = move_toward(entity.velocity.y, 0, entity.get_speed() * get_speed())
		direction.z = move_toward(entity.velocity.z, 0, entity.get_speed() * get_speed())
	return direction;
	

func move(entity, direction):
	if direction:
		entity.velocity.x = direction.x * entity.get_speed() * get_speed();
		entity.velocity.y = direction.y * entity.get_speed() * get_speed();
		entity.velocity.z = direction.z * entity.get_speed() * get_speed();
	else:
		entity.velocity.x = move_toward(entity.velocity.x, 0, entity.get_speed() * get_speed())
		entity.velocity.y = move_toward(entity.velocity.y, 0, entity.get_speed() * get_speed())
		entity.velocity.z = move_toward(entity.velocity.z, 0, entity.get_speed() * get_speed())
