extends Resource


class_name MovementMode


var _speed := 0.0;


func get_speed():
	return _speed;


func is_affected_by_gravity():
	return true;


func get_movement(_entity, _direction):
	pass;


func move(_entity, _direction):
	pass;


func jump(_entity):
	pass;
