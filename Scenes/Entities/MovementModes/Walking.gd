extends MovementMode

class_name WalkingMovement


func _init():
	_speed = 1;


func get_movement(entity, direction):
	if direction:
		direction.x = direction.x * entity.get_speed() * get_speed();
		direction.z = direction.z * entity.get_speed() * get_speed();
	else:
		direction.x = move_toward(entity.velocity.x, 0, entity.get_speed() * get_speed());
		direction.z = move_toward(entity.velocity.z, 0, entity.get_speed() * get_speed());
	return direction;


func move(entity, direction):
	if direction:
		entity.velocity.x = direction.x * entity.get_speed() * get_speed()
		entity.velocity.z = direction.z * entity.get_speed() * get_speed()
	else:
		entity.velocity.x = move_toward(entity.velocity.x, 0, entity.get_speed() * get_speed())
		entity.velocity.z = move_toward(entity.velocity.z, 0, entity.get_speed() * get_speed())


func jump(entity):
	if entity.is_on_floor():
		entity.velocity.y = entity._motivator.jump_velocity.get_current_score();
