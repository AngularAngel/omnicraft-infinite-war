extends BaseBuildingUnit

class_name ConstructionSite


@onready var _progress = $Stats/Resources/Progress as ResourceContainer;
@onready var _energy_cost = $"Stats/Energy Cost" as PrimaryStat
@onready var _mass_cost = $"Stats/Mass Cost" as PrimaryStat


var _terrain_tool : VoxelTool = null;
var _voxel_id : int;
var _units : Node;
var _building: BaseBuildingUnit = null;

var _multiblock_x := 1;
var _multiblock_y := 2;
var _multiblock_z := 3;
var _entity_voxel := 4;


func _ready():
	set_meta("isConstructionSite", true);
	super._ready();
	set_scale(Vector3(_dimensions));
	$Chassis/Mesh.set_instance_shader_parameter("texel_scale", Vector3(_dimensions));


func set_dimensions(dimensions: Vector3i):
	super.set_dimensions(dimensions);
	set_scale(Vector3(_dimensions));


func get_building_offset():
	return Vector3(0.5, 0.5, 0.5);


func set_building(building: BaseBuildingUnit):
	_building = building;
	_dimensions = building._dimensions;
	_voxel_id = _entity_voxel;


func add_progress(amount: float):
	_progress._current_resource.modify_base_score(amount)
	if _progress._current_resource.get_current_score() >= _progress._maximum_resource.get_current_score():
		build();


func build():
	if _building != null:
		_building._faction = _faction;
		_building.transform.origin = transform.origin;
		
		_units.add_voxel_entity(_building);
	
	else:
		var voxel_position := Vector3i(global_position);
		for x in range(_dimensions.x):
			for y in range(_dimensions.y):
				for z in range(_dimensions.z):
					var id = _voxel_id;
					if x > 0:
						id = _multiblock_x;
					elif z > 0:
						id = _multiblock_z;
					elif y > 0:
						id = _multiblock_y;
						
					_units.get_parent()._terrain.set_voxel(voxel_position + Vector3i(x, y, z), id);
	
	queue_free()
