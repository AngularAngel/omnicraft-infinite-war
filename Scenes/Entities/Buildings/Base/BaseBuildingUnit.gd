extends StaticBody3D

class_name BaseBuildingUnit


signal destroyed;


@export var _faction : Faction;
@export var _dimensions := Vector3i(1, 1, 1) : set = set_dimensions;


@onready var _chassis = $Chassis as BaseChassisComponent;


func _ready():
	var this = self;
	_chassis.component_destroyed.connect(func():
		this.destroyed.emit();
		this.queue_free();
	)


func _enter_tree():
	var offset = get_building_offset();
	for child in get_children():
		if child.has_method("translate"):
			child.translate(offset);


func set_dimensions(dimensions: Vector3i):
	_dimensions = dimensions


func get_building_offset():
	return Vector3(0.5, 0.5, 0.5) * Vector3(_dimensions);


func get_unit():
	return self;
	

func get_faction():
	return _faction;
